﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Filtering;
using Xunit;

namespace ShikashiBot.UnitTests.Filtering
{
    public class WordFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext, 
            [Frozen] Mock<IPersistenceProvider> persistenceProvider, Model.Channel channel)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var dbContext = TestDatabaseProvider.CreateInMemoryDb();

            persistenceProvider.Setup(c => c.GetPersistenceContext()).Returns(dbContext);

            dbContext.Channel.Add(channel);
            dbContext.SaveChanges();
            var model = new Model.Filters.WordFilter()
            {
                Channel = channel
            };
            var handler = new WordFilterHandler(model, persistenceProvider.Object);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext, [Frozen] Mock<IPersistenceProvider> persistenceProvider, Model.Channel channel)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            dbContext.Channel.Add(channel);
            dbContext.SaveChanges();
            var model = new Model.Filters.WordFilter()
            {
                Channel = channel
            };

            persistenceProvider.Setup(c => c.GetPersistenceContext()).Returns(dbContext);

            var handler = new WordFilterHandler(model, persistenceProvider.Object);
            var bannedWord = new Model.BannedWord()
            {
                Channel = channel,
                Word = "testMessage"
            };
            
            dbContext.BannedWord.Add(bannedWord);
            dbContext.SaveChanges();
            handler.ReloadBannedWords();

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.True(result);
        }
    }
}
