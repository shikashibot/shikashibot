﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Filtering;
using Xunit;

namespace ShikashiBot.UnitTests.Filtering
{
    public class URLFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext, Model.Channel channel)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            dbContext.Channel.Add(channel);
            dbContext.SaveChanges();
            var model = new Model.Filters.DomainFilter()
            {
                Channel = channel
            };

            var handler = new URLFilterHandler(model, dbContext);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext, Model.Channel channel)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage.com";

            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            dbContext.Channel.Add(channel);
            dbContext.SaveChanges();

            var model = new Model.Filters.DomainFilter()
            {
                Channel = channel
            };
            
            var whitelistedDomain = new Model.WhitelistedDomain();

            whitelistedDomain.Channel = channel;
            whitelistedDomain.Domain = "readit.com";
            dbContext.WhitelistedDomain.Add(whitelistedDomain);
            dbContext.SaveChanges();

            var handler = new URLFilterHandler(model, dbContext);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.True(result);
        }
    }
}
