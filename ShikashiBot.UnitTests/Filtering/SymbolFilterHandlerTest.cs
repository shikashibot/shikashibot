﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filtering
{
    public class SymbolFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var model = new Model.Filters.SymbolFilter()
            {
                MaxCount = 2
            };

            var handler = new SymbolFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "( ͡° ͜ʖ ͡°)";

            var model = new Model.Filters.SymbolFilter()
            {
                MaxCount = 2
            };

            var handler = new SymbolFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.True(result);
        }
    }
}
