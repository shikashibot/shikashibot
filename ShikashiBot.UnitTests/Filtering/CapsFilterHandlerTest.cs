﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Filtering;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filtering
{
    public class CapsFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var model = new Model.Filters.CapsFilter()
            {
                CapsLimit = 2
            };
            var handler = new CapsFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "TESTMESSAGE";

            var model = new Model.Filters.CapsFilter()
            {
                CapsLimit = 2
            };
            var handler = new CapsFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.True(result);
        }
    }
}
