﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Messaging.Filtering;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filtering
{
    public class EmoticonsFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext, [Frozen] Mock<IEmoticonProvider> emoticonProvider)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "testMessage";

            var model = new Model.Filters.EmoteFilter()
            {
                EmoteLimit = 2
            };
            var handler = new EmoticonsFilterHandler(model, emoticonProvider.Object);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext, [Frozen] Mock<IEmoticonProvider> emoticonProvider)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "Kappa Kappa Kappa";

            var model = new Model.Filters.EmoteFilter()
            {
                EmoteLimit = 2
            };
            var handler = new EmoticonsFilterHandler(model, emoticonProvider.Object);
            emoticonProvider.Setup(c => c.IsEmoticon(It.Is<string>(t => t == "Kappa"))).Returns(true);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert
            Assert.True(result);
        }
    }
}
