﻿using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Messaging.Filtering;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filtering
{
    public class LengthFilterHandlerTest
    {
        [Theory, AutoMoqData]
        public void MessageDoesNotViolateTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "t";

            var model = new Model.Filters.LengthFilter()
            {
                MaxLength = 2
            };
            var handler = new LengthFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert 
            Assert.False(result);
        }

        [Theory, AutoMoqData]
        public void MessageViolatesTheFilter([Frozen] Mock<IMessageContext> messageContext)
        {
            // Arrange
            string channelName = "testChannelName";
            string senderUsername = "testUsername";
            string message = "textMessage";

            var model = new Model.Filters.LengthFilter()
            {
                MaxLength = 2
            };
            var handler = new LengthFilterHandler(model);

            // Act
            var result = handler.HandleMessage(channelName, senderUsername, message, messageContext.Object);

            // Assert 
            Assert.True(result);
        }
    }
}
