﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Moq;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Security.Claims;

namespace ShikashiBot.UnitTests
{
    public class AuthorizationServiceProvider
    {
        public static void Setup(Mock<IAuthorizationService> service, Model.Channel channelModel, OperationAuthorizationRequirement role)
        {
            service.Setup(c => c.AuthorizeAsync(
                It.Is<ClaimsPrincipal>(t => t == null),
                It.Is<ChannelViewModel>(t => t.ChannelName == channelModel.ChannelName),
                It.Is<IAuthorizationRequirement[]>(t => t.Length == 1 && t.Contains(role))))
                .ReturnsAsync(true);
        }
    }
}
