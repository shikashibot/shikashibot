﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using ShikashiBot.IRC;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Services;
using ShikashiBot.TwitchAPI;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests
{
    public class ShikashiBotManagerTest
    {
        [Theory]
        [InlineData("TestChannel")]
        public async void ExistingChannelIsSetToActive(string channelName)
        {
            var builder = new DbContextOptionsBuilder<PersistenceContext>();
            builder.UseInMemoryDatabase();
            var options = builder.Options;

            var model = new Model.Channel { ChannelName = channelName, Visiting = false };

            using (PersistenceContext context = new PersistenceContext(options))
            {
                context.Channel.Add(model);
                context.SaveChanges();
            }

            var persistenceProvider = new Mock<IPersistenceProvider>();
            var loggerFactory = new Mock<ILoggerFactory>();
            var ircRelay = new Mock<IIrcRelay>();
            var config = new Mock<IConfigurationRoot>();
            var usageStatistics = new Mock<IUsageStatisticsService>();
            var streamProvider = new Mock<ITwitchStreamProvider>();
            var emoteProvider = new Mock<IEmoticonProvider>();

            persistenceProvider.Setup(c => c.GetPersistenceContext()).Returns(new PersistenceContext(options));

            var logger = new Mock<ILogger>();
            loggerFactory.Setup(c => c.CreateLogger(It.IsAny<string>())).Returns(logger.Object);


            var botManager = new ShikashiBotManager(persistenceProvider.Object, loggerFactory.Object, ircRelay.Object, config.Object, usageStatistics.Object, streamProvider.Object, emoteProvider.Object);

            using (PersistenceContext context = new PersistenceContext(options))
            {
                var persistentChannel = await context.Channel.Where(p => p.ChannelName == channelName).SingleOrDefaultAsync();
                botManager.ModerateChannel(persistentChannel, true);
            }

            using (PersistenceContext context = new PersistenceContext(options))
            {
                var channel = (from p in context.Channel
                               where p.ChannelName == channelName
                               select p).SingleOrDefault();

                Assert.NotNull(channel);
                Assert.True(channel.Visiting);
            }
        }
    }
}
