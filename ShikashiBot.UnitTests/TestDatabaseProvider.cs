﻿using Microsoft.EntityFrameworkCore;

namespace ShikashiBot.UnitTests
{
    public static class TestDatabaseProvider
    {
        public static PersistenceContext CreateInMemoryDb()
        {
            var builder = new DbContextOptionsBuilder<PersistenceContext>();
            builder.UseInMemoryDatabase();
            var options = builder.Options;

            return new PersistenceContext(options);
        }

        public static void DeleteAll<T>(this DbContext context) where T : class
        {
            foreach (var p in context.Set<T>())
            {
                context.Entry(p).State = EntityState.Deleted;
            }
        }
    }
}
