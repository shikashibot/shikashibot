﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class AnnouncementControllerTest
    {
        [Theory, AutoMoqData]
        public async void AnnouncementIsRemovedWhenStopped([Frozen] Mock<IAuthorizationService> authorizationService, 
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Announcement announcementModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new AnnouncementController(botManager.Object, authorizationService.Object, dbContext);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == announcementModel.Channel.ChannelName)));
            
            dbContext.Announcement.Add(announcementModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, announcementModel.Channel, Operations.Delete);

            // Act
            var result = await controller.StopAnnouncement(announcementModel.Channel.ChannelName, announcementModel.Id);

            // Assert
            Assert.True(result is OkResult);
            Assert.False(dbContext.Announcement.Where(t => t.ChannelName == announcementModel.ChannelName && t.Message == announcementModel.Message).Any());
        }

        [Theory, AutoMoqData]
        public async void AddedAnnouncementIsPersistent([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Announcement announcementModel, string message, int secondsInterval)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new AnnouncementController(botManager.Object, authorizationService.Object, dbContext);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == announcementModel.Channel.ChannelName)));

            var announcementViewModel = new AnnouncementViewModel { IsActive = true, Message = message, SecondsInterval = secondsInterval };

            dbContext.Channel.Add(announcementModel.Channel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, announcementModel.Channel, Operations.Create);

            // Act
            var result = await controller.AddAnnouncement(announcementViewModel, announcementModel.Channel.ChannelName);

            //Assert
            Assert.True(result is OkResult);
            Assert.True(dbContext.Announcement.Where(t => t.ChannelName == announcementModel.Channel.ChannelName && t.Message == message && t.SecondsInterval == secondsInterval).Any());
        }

        [Theory, AutoMoqData]
        public async void PersistentAnnouncementIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Announcement announcementModel, string message, int secondsInterval)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new AnnouncementController(botManager.Object, authorizationService.Object, dbContext);
            
            dbContext.Channel.Add(announcementModel.Channel);
            dbContext.Announcement.Add(announcementModel);
            dbContext.SaveChanges();
            AuthorizationServiceProvider.Setup(authorizationService, announcementModel.Channel, Operations.Read);

            //Act
            var result = await controller.GetAnnouncement(announcementModel.Id, announcementModel.Channel.ChannelName) as ObjectResult;

            //Assert
            Assert.True(result is OkObjectResult);
            var announcement = result.Value as AnnouncementViewModel;
            Assert.NotNull(announcement);
            Assert.Equal(announcement.Id, announcementModel.Id);
        }

        [Theory, AutoMoqData]
        public async void PersistentAnnouncementsAreReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Channel channelModel, string message, int count)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new AnnouncementController(botManager.Object, authorizationService.Object, dbContext);

            dbContext.Channel.Add(channelModel);

            for (int i = 0; i < count; i++)
            {
                var announcementModel = new Model.Announcement { Channel = channelModel, IsActive = true, Message = message, SecondsInterval = 30000 };
                dbContext.Announcement.Add(announcementModel);
            }

            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Read);

            //Act
            var result = await controller.GetAllAnnouncements(channelModel.ChannelName) as ObjectResult;

            //Assert
            Assert.True(result is OkObjectResult);
            var announcements = result.Value as List<AnnouncementViewModel>;
            Assert.NotNull(announcements);
            Assert.Equal(count, announcements.Count);
        }

        [Theory, AutoMoqData]
        public async void AnnouncementUpdatesArePersistent([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Announcement announcementModel, AnnouncementViewModel announcementViewModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new AnnouncementController(botManager.Object, authorizationService.Object, dbContext);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == announcementModel.Channel.ChannelName)));

            dbContext.Announcement.Add(announcementModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, announcementModel.Channel, Operations.Update);

            //Act
            var result = await controller.UpdateAnnouncement(announcementModel.Id, announcementModel.Channel.ChannelName, announcementViewModel) as ObjectResult;

            //Assert
            Assert.True(result is OkObjectResult);
            var announcement = result.Value as AnnouncementViewModel;
            Assert.NotNull(announcement);

            var storedAnnouncement = dbContext.Announcement.Where(t => t.Id == announcementModel.Id).FirstOrDefault();
            Assert.NotNull(storedAnnouncement);

            Assert.Equal(announcementModel.Message, storedAnnouncement.Message);
            Assert.Equal(announcementModel.SecondsInterval, storedAnnouncement.SecondsInterval);
        }
    }
}
