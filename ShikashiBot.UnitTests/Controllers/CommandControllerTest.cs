﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Model.Commands;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class CommandControllerTest
    {
        [Theory, AutoMoqData]
        public async void ReturnsDefaultCommandConfigurations([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authService, DefaultCommandConfiguration defaultConfigModel, Model.Channel channelModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CommandController(botManager.Object, dbContext, authService.Object);

            dbContext.DeleteAll<DefaultCommandConfiguration>();
            dbContext.DefaultCommandConfiguration.Add(defaultConfigModel);
            dbContext.Channel.Add(channelModel);
            await dbContext.SaveChangesAsync();

            AuthorizationServiceProvider.Setup(authService, channelModel, Operations.Read);

            // Act
            var result = await controller.GetAllCommands(channelModel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;
            var viewModels = (List<CommandConfigurationViewModel>)objectResult.Value;

            Assert.Equal(1, viewModels.Count);
            Assert.Equal(defaultConfigModel.Description, viewModels[0].Description);
        }

        [Theory, AutoMoqData]
        public async void DefaultCommandConfigurationIsOverwrittenByCustomCommandConfiguration([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authService, DefaultCommandConfiguration defaultConfigModel, Model.Channel channelModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CommandController(botManager.Object, dbContext, authService.Object);

            var channelCommandConfig = new ChannelCommandConfiguration
            {
                Channel = channelModel,
                CommandId = defaultConfigModel.CommandId,
                Enabled = true,
                MinRank = defaultConfigModel.MinRank,
                Prefix = defaultConfigModel.Prefix
            };

            dbContext.DeleteAll<DefaultCommandConfiguration>();
            dbContext.DefaultCommandConfiguration.Add(defaultConfigModel);
            dbContext.ChannelCommandConfiguration.Add(channelCommandConfig);
            dbContext.Channel.Add(channelModel);
            await dbContext.SaveChangesAsync();

            AuthorizationServiceProvider.Setup(authService, channelModel, Operations.Read);

            // Act
            var result = await controller.GetAllCommands(channelModel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;
            var viewModels = (List<CommandConfigurationViewModel>)objectResult.Value;

            Assert.Equal(1, viewModels.Count);
            Assert.Equal(defaultConfigModel.Description, viewModels[0].Description);
            Assert.True(viewModels[0].Enabled);
        }

        [Theory, AutoMoqData]
        public async void ModifyCommandIsStored([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authService, DefaultCommandConfiguration defaultConfigModel, Model.Channel channelModel, string commandPrefix)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CommandController(botManager.Object, dbContext, authService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == channelModel.ChannelName)));

            defaultConfigModel.Prefix = commandPrefix;

            dbContext.DeleteAll<DefaultCommandConfiguration>();
            dbContext.DefaultCommandConfiguration.Add(defaultConfigModel);
            dbContext.Channel.Add(channelModel);
            await dbContext.SaveChangesAsync();

            var commandViewModel = new CommandConfigurationViewModel
            {
                Prefix = commandPrefix,
                MinRank = 3,
                Enabled = true
            };

            AuthorizationServiceProvider.Setup(authService, channelModel, Operations.Update);

            // Act
            var result = await controller.ModifyCommand(channelModel.ChannelName, defaultConfigModel.CommandId, commandViewModel);

            //Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;
            var returnedViewModel = (CommandConfigurationViewModel)objectResult.Value;

            Assert.Equal(commandViewModel.MinRank, returnedViewModel.MinRank);
            Assert.Equal(commandViewModel.Enabled, returnedViewModel.Enabled);
        }
    }
}
