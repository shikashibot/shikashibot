﻿using ShikashiBot.Controllers;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class StatisticsControllerTest
    {
        [Fact]
        public void IndexReturnsMemoryUsage()
        {
            // Arrange
            var controller = new StatisticsController();

            // Act
            var response = controller.Index();

            // Assert
            Assert.NotNull(response);
            Assert.True(controller.ViewData["Memory"] is float);
            Assert.True((float)controller.ViewData["Memory"] > 0);
        }
    }
}
