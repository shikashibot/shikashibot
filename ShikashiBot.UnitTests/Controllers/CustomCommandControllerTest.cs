﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Model.Commands;
using ShikashiBot.Permissions;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class CustomCommandControllerTest
    {
        [Theory, AutoMoqData]
        public async void AddingCommandPersists([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authorizationService, Model.Channel channelModel, CustomCommandViewModel commandViewModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();

            dbContext.Channel.Add(channelModel);
            await dbContext.SaveChangesAsync();

            botManager.Setup(c => c.GetChannel(channelModel.ChannelName));

            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Create);

            var controller = new CustomCommandController(botManager.Object, dbContext, authorizationService.Object);

            // Act
            var result = await controller.AddCommand(commandViewModel, channelModel.ChannelName, commandViewModel.CommandId);

            // Assert
            Assert.True(result is OkResult);
            Assert.True(dbContext.CustomCommand.Where(t => t.ChannelName == channelModel.ChannelName && t.CommandId == commandViewModel.CommandId).Any());
        }

        [Theory, AutoMoqData]
        public async void PersistedCommandIsDeleted([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authorizationService, CustomCommand customCommandModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();

            dbContext.CustomCommand.Add(customCommandModel);
            await dbContext.SaveChangesAsync();

            botManager.Setup(c => c.GetChannel(customCommandModel.Channel.ChannelName));

            AuthorizationServiceProvider.Setup(authorizationService, customCommandModel.Channel, Operations.Delete);

            var controller = new CustomCommandController(botManager.Object, dbContext, authorizationService.Object);

            // Act
            var result = await controller.DeleteCommand(customCommandModel.Channel.ChannelName, customCommandModel.CommandId);

            // Assert
            Assert.True(result is OkResult);
            Assert.False(dbContext.CustomCommand.Where(t => t.ChannelName == customCommandModel.Channel.ChannelName && t.CommandId == customCommandModel.CommandId).Any());
        }

        [Theory, AutoMoqData]
        public async void PersistedCommandIsUpdated([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authorizationService, CustomCommand customCommandModel, string newOutput)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();

            dbContext.CustomCommand.Add(customCommandModel);
            await dbContext.SaveChangesAsync();

            botManager.Setup(c => c.GetChannel(customCommandModel.Channel.ChannelName));

            AuthorizationServiceProvider.Setup(authorizationService, customCommandModel.Channel, Operations.Update);

            var controller = new CustomCommandController(botManager.Object, dbContext, authorizationService.Object);
            var viewModel = new CustomCommandViewModel
            {
                CommandId = customCommandModel.CommandId,
                IsActive = customCommandModel.IsActive,
                MinRank = (UserType)customCommandModel.MinRank,
                Output = newOutput
            };

            // Act
            var result = await controller.UpdateCommand(viewModel, customCommandModel.CommandId, customCommandModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);
            Assert.True(dbContext.CustomCommand.Where(t => t.ChannelName == customCommandModel.Channel.ChannelName && t.CommandId == customCommandModel.CommandId && t.Output == newOutput).Any());
        }

        [Theory, AutoMoqData]
        public async void PersistedCommandIsActivated([Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<IAuthorizationService> authorizationService, CustomCommand customCommandModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();

            customCommandModel.IsActive = false;
            dbContext.CustomCommand.Add(customCommandModel);
            await dbContext.SaveChangesAsync();

            botManager.Setup(c => c.GetChannel(customCommandModel.Channel.ChannelName));

            AuthorizationServiceProvider.Setup(authorizationService, customCommandModel.Channel, Operations.Update);

            var controller = new CustomCommandController(botManager.Object, dbContext, authorizationService.Object);
            var viewModel = new CustomCommandViewModel
            {
                CommandId = customCommandModel.CommandId,
                IsActive = true,
                MinRank = (UserType)customCommandModel.MinRank,
                Output = customCommandModel.Output
            };

            // Act
            var result = await controller.SetIsActive(viewModel, customCommandModel.CommandId, customCommandModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);
            Assert.True(dbContext.CustomCommand.Where(t => t.ChannelName == customCommandModel.Channel.ChannelName && t.CommandId == customCommandModel.CommandId && t.IsActive).Any());
        }
    }
}
