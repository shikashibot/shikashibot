﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    /// <summary>
    /// Tests for BannedWord Controller
    /// </summary>
    public class BannedWordsControllerTest
    {
        [Theory, AutoMoqData]
        public async void BannedWordsAreReturned([Frozen] Mock<IAuthorizationService> authorizationService, 
            [Frozen] Mock<IShikashiBotManager> botManager, Model.BannedWord bannedWordModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new BannedWordsController(botManager.Object, dbContext, authorizationService.Object);

            dbContext.BannedWord.Add(bannedWordModel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, bannedWordModel.Channel, Operations.Read);

            // Act
            var results = await controller.GetAllBannedWords(bannedWordModel.Channel.ChannelName) as ObjectResult;

            // Assert
            Assert.NotNull(results);
            Assert.True(results is OkObjectResult);
            Assert.True(results.Value is List<string>);
            List<string> bannedWords = results.Value as List<string>;

            Assert.Equal(bannedWords.Count, 1);
            Assert.Equal(bannedWords[0], bannedWordModel.Word);
        }

        [Theory, AutoMoqData]
        public async void BannedWordIsAdded([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Channel channelModel, BannedWordViewModel bannedWordViewModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new BannedWordsController(botManager.Object, dbContext, authorizationService.Object);

            dbContext.Channel.Add(channelModel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Create);
            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == channelModel.ChannelName)));

            // Act
            var result = await controller.AddBannedWord(bannedWordViewModel, channelModel.ChannelName) as CreatedResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(dbContext.BannedWord.Where(t => t.ChannelName == channelModel.ChannelName && t.Word == bannedWordViewModel.Word).SingleOrDefault());
        }

        [Theory, AutoMoqData]
        public async void BannedWordIsDeleted([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.BannedWord bannedWordModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new BannedWordsController(botManager.Object, dbContext, authorizationService.Object);
            
            dbContext.BannedWord.Add(bannedWordModel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, bannedWordModel.Channel, Operations.Delete);
            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == bannedWordModel.Channel.ChannelName)));

            // Act
            var result = await controller.DeleteBannedWord(bannedWordModel.Channel.ChannelName, bannedWordModel.Word);

            // Assert
            Assert.True(result is OkResult);
            Assert.Null(dbContext.BannedWord.Where(t => t.ChannelName == bannedWordModel.Channel.ChannelName && t.Word == bannedWordModel.Word).SingleOrDefault());
        }
    }
}
