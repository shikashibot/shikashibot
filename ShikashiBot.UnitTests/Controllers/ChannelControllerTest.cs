﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Policies;
using ShikashiBot.TwitchAPI;
using ShikashiBot.ViewModels;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class ChannelControllerTest
    {
        [Theory, AutoMoqData]
        public async void ModeratingChannelIsReturned([Frozen] Mock<IAuthorizationService> authorizationService, Model.Channel channelModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new ChannelController(null, dbContext, authorizationService.Object);

            channelModel.Visiting = true;

            dbContext.Channel.Add(channelModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Read);

            // Act
            var results = await controller.GetChannel(channelModel.ChannelName) as OkObjectResult;

            // Assert
            Assert.NotNull(results);
            Assert.True(results is OkObjectResult);
            Assert.True(results.Value is ChannelViewModel);
            ChannelViewModel returnedViewModel = results.Value as ChannelViewModel;

            Assert.Equal(returnedViewModel.ChannelName, channelModel.ChannelName);
            Assert.True(returnedViewModel.Visiting);
        }

        [Theory, AutoMoqData]
        public async void ChannelCanBeModerated([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<ITwitchStreamProvider> streamProvider,
            [Frozen] Mock<IPersistenceProvider> persistenceProvider, [Frozen] Mock<ILoggerFactory> loggerFactory,
            Model.Channel channelModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new ChannelController(botManager.Object, dbContext, authorizationService.Object);
            
            dbContext.Channel.Add(channelModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Create);

            persistenceProvider.Setup(c => c.GetPersistenceContext()).Returns(dbContext);
            
            botManager.Setup(c => c.BotUsername).Returns(channelModel.ChannelName);
            botManager.Setup(c => c.ModerateChannel(It.Is<Model.Channel>(t => t.ChannelName == channelModel.ChannelName), true));

            Channel internalChannelInstance = new Channel(channelModel, botManager.Object, persistenceProvider.Object, loggerFactory.Object, true, streamProvider.Object);

            botManager.Setup(c => c.GetChannel(channelModel.ChannelName)).Returns(internalChannelInstance);

            // Act
            var results = await controller.EnterChannel(new ChannelViewModel { ChannelName = channelModel.ChannelName, Visiting = true }) as CreatedAtRouteResult;

            // Assert
            Assert.NotNull(results);
            Assert.True(results.Value is ChannelViewModel);
            ChannelViewModel returnedViewModel = results.Value as ChannelViewModel;

            Assert.Equal(returnedViewModel.ChannelName, channelModel.ChannelName);
            Assert.True(returnedViewModel.Visiting);
            botManager.Verify(c => c.ModerateChannel(It.Is<Model.Channel>(t => t.ChannelName == channelModel.ChannelName), true));
        }
        
        [Theory, AutoMoqData]
        public async void ChannelCanBeStoppedModerated([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, [Frozen] Mock<ITwitchStreamProvider> streamProvider,
            [Frozen] Mock<IPersistenceProvider> persistenceProvider, [Frozen] Mock<ILoggerFactory> loggerFactory,
            Model.Channel channelModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new ChannelController(botManager.Object, dbContext, authorizationService.Object);

            channelModel.Visiting = true;

            dbContext.Channel.Add(channelModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, channelModel, Operations.Delete);

            persistenceProvider.Setup(c => c.GetPersistenceContext()).Returns(dbContext);

            botManager.Setup(c => c.BotUsername).Returns(channelModel.ChannelName);

            Channel internalChannelInstance = new Channel(channelModel, botManager.Object, persistenceProvider.Object, loggerFactory.Object, true, streamProvider.Object);

            botManager.Setup(c => c.GetChannel(channelModel.ChannelName)).Returns(internalChannelInstance);

            // Act
            var result = await controller.LeaveChannel(channelModel.ChannelName);

            // Assert
            Assert.True(result is OkResult);
            botManager.Verify(c => c.StopModeratingChannel(It.Is<string>(t => t == channelModel.ChannelName)));
        }
    }
}
