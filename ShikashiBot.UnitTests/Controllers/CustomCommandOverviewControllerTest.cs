﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Model.Commands;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class CustomCommandOverviewControllerTest
    {

        [Theory, AutoMoqData]
        public async void ReturnsPersistentCommandsConfigurations([Frozen] Mock<IAuthorizationService> authService, CustomCommand customCommandModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CustomCommandOverviewController(dbContext, authService.Object);

            dbContext.CustomCommand.Add(customCommandModel);
            await dbContext.SaveChangesAsync();

            AuthorizationServiceProvider.Setup(authService, customCommandModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetAllCustomCommands(customCommandModel.Channel.ChannelName);

            //Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;
            var viewModels = (List<CustomCommandViewModel>)objectResult.Value;

            Assert.Equal(1, viewModels.Count);
            Assert.Equal(customCommandModel.Output, viewModels[0].Output);
        }
    }
}
