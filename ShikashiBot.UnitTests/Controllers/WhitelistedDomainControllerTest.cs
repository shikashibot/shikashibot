﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class WhitelistedDomainControllerTest
    {
        [Theory, AutoMoqData]
        public async void GetDomainsShouldReturnPersistentDomains([Frozen] Mock<IAuthorizationService> authorizationService, 
            [Frozen] Mock<IShikashiBotManager> botManager, Model.WhitelistedDomain whitelistedDomainModel)
        {
            //Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new WhitelistedDomainsController(botManager.Object, dbContext, authorizationService.Object);
            whitelistedDomainModel.Domain = "example.com";

            dbContext.WhitelistedDomain.Add(whitelistedDomainModel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, whitelistedDomainModel.Channel, Operations.Read);

            //Act
            var response = await controller.GetWhitelistedDomains(whitelistedDomainModel.Channel.ChannelName);

            //Assert
            Assert.True(response is OkObjectResult);

            OkObjectResult objectResult = (OkObjectResult)response;
            Assert.NotNull(objectResult.Value);

            List<string> domains = (List<string>)objectResult.Value;
            Assert.Equal(1, domains.Count);
            Assert.Equal(whitelistedDomainModel.Domain, domains[0]);
        }

        [Theory, AutoMoqData]
        public async void AddingDomainShouldStoreDomain([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.WhitelistedDomain whitelistedDomainModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new WhitelistedDomainsController(botManager.Object, dbContext, authorizationService.Object);
            string domain = "foo.bar.com";

            dbContext.Channel.Add(whitelistedDomainModel.Channel);
            dbContext.SaveChanges();
            
            AuthorizationServiceProvider.Setup(authorizationService, whitelistedDomainModel.Channel, Operations.Update);

            // Act
            var response = await controller.AddWhitelistedDomains(whitelistedDomainModel.Channel.ChannelName, new WhitelistedDomainViewModel { Domain = domain });

            // Assert
            Assert.True(response is CreatedResult);
            Assert.True(dbContext.WhitelistedDomain.Where(p => p.ChannelName == whitelistedDomainModel.Channel.ChannelName && p.Domain == domain).Any());
        }

        [Theory, AutoMoqData]
        public async void DeletingDomainShouldDeleteDomain([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.WhitelistedDomain whitelistedDomainModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new WhitelistedDomainsController(botManager.Object, dbContext, authorizationService.Object);

            dbContext.WhitelistedDomain.Add(whitelistedDomainModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, whitelistedDomainModel.Channel, Operations.Delete);

            // Act
            var response = await controller.RemoveDomain(whitelistedDomainModel.Channel.ChannelName, whitelistedDomainModel.Domain);

            // Assert
            Assert.True(response is OkResult);
            Assert.False(dbContext.WhitelistedDomain.Where(p => p.ChannelName == whitelistedDomainModel.Channel.ChannelName && p.Domain == whitelistedDomainModel.Domain).Any());
        }
    }
}
