﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Model;
using ShikashiBot.TwitchAPI;
using ShikashiBot.ViewModels.Auth;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    /// <summary>
    /// Test cases for AuthController
    /// </summary>
    public class AuthControllerTest
    {
        /// <summary>
        /// Tests that when an invalid token is provided (null or empty string), the controller respons with a bad request.
        /// </summary>
        /// <param name="token">OAuth token</param>
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async void InvalidTokenResultsInBadRequest(string token)
        {
            // Arrange
            var authService = new Mock<ITwitchAuthService>();
            authService.Setup(c => c.ObtainAndStoreCredentialsAsync(token)).ReturnsAsync(null);
            AuthController controller = new AuthController(authService.Object, null);
            
            // Act
            var result = await controller.CreateToken(new LoginViewModel() { Token = token });

            // Assert
            Assert.True(result is BadRequestResult);
        }

        /// <summary>
        /// Tests that when a valid token is provided, controller respons with an entity with a generated token.
        /// </summary>
        /// <param name="token">OAuth token</param>
        [Theory, AutoMoqData]
        public async void ValidTokenResultsInResponse([Frozen] Mock<ITwitchAuthService> authService, string token)
        {
            // Arrange
            string generatedToken = "generated token Kappa";
            var authServiceTokenResult = new TwitchCredentials { PublicAccessToken = generatedToken };
            authService.Setup(c => c.ObtainAndStoreCredentialsAsync(It.Is<string>(t => t == token))).ReturnsAsync(authServiceTokenResult);
            AuthController controller = new AuthController(authService.Object, null);

            // Act
            var result = await controller.CreateToken(new LoginViewModel() { Token = token }) as CreatedResult;

            // Assert
            Assert.True(result is CreatedResult);
            Assert.True(result.Value is LoginViewModel);

            LoginViewModel resultViewModel = result.Value as LoginViewModel;

            Assert.Equal(resultViewModel.Token, generatedToken);
        }

        [Theory, AutoMoqData]
        public async void CorrectTokenIsReturned([Frozen] Mock<ITwitchAuthService> authService, TwitchCredentials credentials, string token)
        {
            // Arrange
            PersistenceContext dbContext = TestDatabaseProvider.CreateInMemoryDb();
            dbContext.TwitchCredentials.Add(credentials);
            await dbContext.SaveChangesAsync();

            authService.Setup(c => c.GetCredentialsAsync(token)).ReturnsAsync(credentials);

            var controller = new AuthController(authService.Object, null);
            SetAuthorizationHeader(controller, token);

            // Act
            var result = await controller.GetCurrentToken() as OkObjectResult;

            // Assert
            Assert.True(result is OkObjectResult);
            Assert.True(result.Value is LoginViewModel);

            LoginViewModel resultViewModel = result.Value as LoginViewModel;

            Assert.Equal(resultViewModel.Token, credentials.PublicAccessToken);
        }

        [Theory, AutoMoqData]
        public async void TokenIsDeleted([Frozen] Mock<ITwitchAuthService> authService, TwitchCredentials credentials, string token)
        {
            // Arrange
            PersistenceContext dbContext = TestDatabaseProvider.CreateInMemoryDb();
            dbContext.TwitchCredentials.Add(credentials);
            await dbContext.SaveChangesAsync();

            authService.Setup(c => c.GetCredentialsAsync(token)).ReturnsAsync(credentials);

            var controller = new AuthController(authService.Object, null);
            SetAuthorizationHeader(controller, token);

            // Act
            var result = await controller.InvalidateToken() as OkResult;

            // Assert
            Assert.True(result is OkResult);
            authService.Verify(t => t.InvalidateTokenAsync(It.Is<TwitchCredentials>(k => k.TwitchUsername == credentials.TwitchUsername)));
        }

        private void SetAuthorizationHeader(Controller controller, string headerValue)
        {
            var context = new Mock<HttpContext>();
            var request = new Mock<HttpRequest>();
            var headers = new Mock<IHeaderDictionary>();
            context.Setup(c => c.Request).Returns(request.Object);
            request.Setup(c => c.Headers).Returns(headers.Object);
            headers.Setup(c => c["Authorization"]).Returns(headerValue);

            controller.ControllerContext = new ControllerContext(new ActionContext(context.Object, new RouteData(), new ControllerActionDescriptor()));
        }
    }
}
