﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class CapsFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, CapsFilter capsFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CapsFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == capsFilterModel.Channel.ChannelName)));

            dbContext.CapsFilter.Add(capsFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, capsFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(capsFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, capsFilterModel.Enabled);
            Assert.Equal(viewModel.Limit, capsFilterModel.CapsLimit);
            Assert.Equal(viewModel.TimeoutSeconds, capsFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, capsFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, CapsFilter capsFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new CapsFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == capsFilterModel.Channel.ChannelName)));

            dbContext.CapsFilter.Add(capsFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, capsFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                Limit = 20,
                OutputMessage = "test message",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            // Act
            var result = await controller.ModifyFilter(newSettings, capsFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);

            var persistentFilter = dbContext.CapsFilter.Where(t => t.ChannelName == capsFilterModel.Channel.ChannelName && t.Id == capsFilterModel.Id).Single();

            Assert.Equal(newSettings.Limit, persistentFilter.CapsLimit);
            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
