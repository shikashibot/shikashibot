﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class DomainFilterControllerTest
    {
        [Theory,AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, DomainFilter domainFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new DomainFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == domainFilterModel.Channel.ChannelName)));

            dbContext.DomainFilter.Add(domainFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, domainFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(domainFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, domainFilterModel.Enabled);
            Assert.Equal(viewModel.RemoveType, domainFilterModel.RemoveType);
            Assert.Equal(viewModel.TimeoutSeconds, domainFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, domainFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, DomainFilter domainFilterModel)
        {
            //Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new DomainFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == domainFilterModel.Channel.ChannelName)));

            dbContext.DomainFilter.Add(domainFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, domainFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                OutputMessage = "TestMessage",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            //Act
            var result = await controller.ModifyFilter(newSettings, domainFilterModel.Channel.ChannelName);

            //Assert
            Assert.True(result is OkResult);
            var persistentFilter = dbContext.DomainFilter.Where(t => t.ChannelName == domainFilterModel.Channel.ChannelName && t.Id == domainFilterModel.Id).Single();

            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        } 
    }
}
