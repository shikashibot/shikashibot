﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class LengthFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager,LengthFilter lengthFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new LengthFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == lengthFilterModel.Channel.ChannelName)));

            dbContext.LengthFilter.Add(lengthFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, lengthFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(lengthFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, lengthFilterModel.Enabled);
            Assert.Equal(viewModel.Limit, lengthFilterModel.MaxLength);
            Assert.Equal(viewModel.TimeoutSeconds, lengthFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, lengthFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, LengthFilter lengthFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new LengthFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == lengthFilterModel.Channel.ChannelName)));

            dbContext.LengthFilter.Add(lengthFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, lengthFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                Limit = 20,
                OutputMessage = "test message",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            // Act
            var result = await controller.ModifyFilter(newSettings, lengthFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);

            var persistentFilter = dbContext.LengthFilter.Where(t => t.ChannelName == lengthFilterModel.Channel.ChannelName && t.Id == lengthFilterModel.Id).Single();

            Assert.Equal(newSettings.Limit, persistentFilter.MaxLength);
            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
