﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class EmoteFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, EmoteFilter emoteFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new EmoteFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == emoteFilterModel.Channel.ChannelName)));

            dbContext.EmoteFilter.Add(emoteFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, emoteFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(emoteFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, emoteFilterModel.Enabled);
            Assert.Equal(viewModel.Limit, emoteFilterModel.EmoteLimit);
            Assert.Equal(viewModel.TimeoutSeconds, emoteFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, emoteFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, EmoteFilter emoteFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new EmoteFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == emoteFilterModel.Channel.ChannelName)));

            dbContext.EmoteFilter.Add(emoteFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, emoteFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                Limit = 20,
                OutputMessage = "test message",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            // Act
            var result = await controller.ModifyFilter(newSettings, emoteFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);

            var persistentFilter = dbContext.EmoteFilter.Where(t => t.ChannelName == emoteFilterModel.Channel.ChannelName && t.Id == emoteFilterModel.Id).Single();

            Assert.Equal(newSettings.Limit, persistentFilter.EmoteLimit);
            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
