﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class WordFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, WordFilter wordFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new WordFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == wordFilterModel.Channel.ChannelName)));

            dbContext.WordFilter.Add(wordFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, wordFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(wordFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, wordFilterModel.Enabled);
            Assert.Equal(viewModel.RemoveType, wordFilterModel.RemoveType);
            Assert.Equal(viewModel.TimeoutSeconds, wordFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, wordFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, WordFilter wordFilterModel)
        {
            //Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new WordFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == wordFilterModel.Channel.ChannelName)));

            dbContext.WordFilter.Add(wordFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, wordFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                OutputMessage = "TestMessage",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            //Act
            var result = await controller.ModifyFilter(newSettings, wordFilterModel.Channel.ChannelName);

            //Assert
            Assert.True(result is OkResult);
            var persistentFilter = dbContext.WordFilter.Where(t => t.ChannelName == wordFilterModel.Channel.ChannelName && t.Id == wordFilterModel.Id).Single();

            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
