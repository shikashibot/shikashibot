﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class RepeatFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, RepeatFilter repeatFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new RepeatFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == repeatFilterModel.Channel.ChannelName)));

            dbContext.RepeatFilter.Add(repeatFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, repeatFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(repeatFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, repeatFilterModel.Enabled);
            Assert.Equal(viewModel.Limit, repeatFilterModel.WordLimit);
            Assert.Equal(viewModel.TimeoutSeconds, repeatFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, repeatFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, RepeatFilter repeatFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new RepeatFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == repeatFilterModel.Channel.ChannelName)));

            dbContext.RepeatFilter.Add(repeatFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, repeatFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                Limit = 20,
                OutputMessage = "test message",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            // Act
            var result = await controller.ModifyFilter(newSettings, repeatFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);

            var persistentFilter = dbContext.RepeatFilter.Where(t => t.ChannelName == repeatFilterModel.Channel.ChannelName && t.Id == repeatFilterModel.Id).Single();

            Assert.Equal(newSettings.Limit, persistentFilter.WordLimit);
            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
