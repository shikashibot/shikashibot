﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers.Filters;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers.Filters
{
    public class SymbolFilterControllerTest
    {
        [Theory, AutoMoqData]
        public async void CorrectConfigurationIsReturned([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, SymbolFilter symbolFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new SymbolFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == symbolFilterModel.Channel.ChannelName)));

            dbContext.SymbolFilter.Add(symbolFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, symbolFilterModel.Channel, Operations.Read);

            // Act
            var result = await controller.GetConfiguration(symbolFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkObjectResult);
            var objectResult = (OkObjectResult)result;

            Assert.True(objectResult.Value is GeneralFilterViewModel);
            var viewModel = (GeneralFilterViewModel)objectResult.Value;

            Assert.Equal(viewModel.Enabled, symbolFilterModel.Enabled);
            Assert.Equal(viewModel.Limit, symbolFilterModel.MaxCount);
            Assert.Equal(viewModel.TimeoutSeconds, symbolFilterModel.TimeoutSeconds);
            Assert.Equal(viewModel.OutputMessage, symbolFilterModel.OutputMessage);
        }

        [Theory, AutoMoqData]
        public async void SettingsAreStored([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, SymbolFilter symbolFilterModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new SymbolFilterController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == symbolFilterModel.Channel.ChannelName)));

            dbContext.SymbolFilter.Add(symbolFilterModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, symbolFilterModel.Channel, Operations.Update);

            GeneralFilterViewModel newSettings = new GeneralFilterViewModel
            {
                Limit = 20,
                OutputMessage = "test message",
                RemoveType = ChatMessageViolationActionType.Ban,
                Enabled = true,
                TimeoutSeconds = 200
            };

            // Act
            var result = await controller.ModifyFilter(newSettings, symbolFilterModel.Channel.ChannelName);

            // Assert
            Assert.True(result is OkResult);

            var persistentFilter = dbContext.SymbolFilter.Where(t => t.ChannelName == symbolFilterModel.Channel.ChannelName && t.Id == symbolFilterModel.Id).Single();

            Assert.Equal(newSettings.Limit, persistentFilter.MaxCount);
            Assert.Equal(newSettings.OutputMessage, persistentFilter.OutputMessage);
            Assert.Equal(newSettings.RemoveType, persistentFilter.RemoveType);
            Assert.Equal(newSettings.Enabled, persistentFilter.Enabled);
            Assert.Equal(newSettings.TimeoutSeconds, persistentFilter.TimeoutSeconds);
        }
    }
}
