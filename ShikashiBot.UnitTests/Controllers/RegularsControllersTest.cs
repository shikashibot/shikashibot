﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using ShikashiBot.Controllers;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ShikashiBot.UnitTests.Controllers
{
    public class RegularsControllersTest
    {
        [Theory, AutoMoqData]
        public async void GetRegularsShouldReturnPersistentRegulars([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Regular regularModel)
        {
            //Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new RegularsController(botManager.Object, dbContext, authorizationService.Object);
            
            dbContext.Regular.Add(regularModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, regularModel.Channel, Operations.Create);

            //Act
            var response = await controller.GetRegulars(regularModel.Channel.ChannelName);

            //Assert
            Assert.True(response is OkObjectResult);

            OkObjectResult objectResult = (OkObjectResult)response;
            Assert.NotNull(objectResult.Value);

            List<string> regulars = (List<string>)objectResult.Value;
            Assert.Equal(1, regulars.Count);
            Assert.Equal(regularModel.TwitchUsername, regulars[0]);
        }

        [Theory, AutoMoqData]
        public async void AddingRegularShouldStoreRegular([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Regular regularModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new RegularsController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == regularModel.Channel.ChannelName)));

            dbContext.Channel.Add(regularModel.Channel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, regularModel.Channel, Operations.Create);

            // Act
            var response = await controller.AddRegular(regularModel.Channel.ChannelName, new TwitchUserViewModel { Username = regularModel.TwitchUsername });

            // Assert
            Assert.True(response is CreatedResult);
            Assert.True(dbContext.Regular.Where(p => p.ChannelName == regularModel.Channel.ChannelName && p.TwitchUsername == regularModel.TwitchUsername).Any());
        }

        [Theory, AutoMoqData]
        public async void DeleteRegularShouldDeleteRegular([Frozen] Mock<IAuthorizationService> authorizationService,
            [Frozen] Mock<IShikashiBotManager> botManager, Model.Regular regularModel)
        {
            // Arrange
            var dbContext = TestDatabaseProvider.CreateInMemoryDb();
            var controller = new RegularsController(botManager.Object, dbContext, authorizationService.Object);

            botManager.Setup(c => c.GetChannel(It.Is<string>(t => t == regularModel.Channel.ChannelName)));

            dbContext.Regular.Add(regularModel);
            dbContext.SaveChanges();

            AuthorizationServiceProvider.Setup(authorizationService, regularModel.Channel, Operations.Delete);

            // Act
            var response = await controller.RemoveRegular(regularModel.Channel.ChannelName, regularModel.TwitchUsername);

            // Assert
            Assert.True(response is OkResult);
            Assert.False(dbContext.Regular.Where(p => p.ChannelName == regularModel.Channel.ChannelName && p.TwitchUsername == regularModel.TwitchUsername).Any());
        }

    }
}
