﻿using Microsoft.Extensions.Logging;

namespace ShikashiBot.Logging
{
    public interface ILoggingProvider
    {
        ILogger GetLogger<T>();
    }
}
