﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ShikashiBot.Logging
{
    public class DefaultLoggingProvider : ILoggingProvider
    {
        private ILoggerFactory loggerFactory;

        public DefaultLoggingProvider(ILoggerFactory loggerFactory, ConfigurationRoot configuration)
        {
            loggerFactory.AddConsole(configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            this.loggerFactory = loggerFactory;
        }

        public ILogger GetLogger<T>()
        {
            return loggerFactory.CreateLogger<T>();
        }
    }
}
