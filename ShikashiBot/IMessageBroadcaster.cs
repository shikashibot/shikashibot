﻿using IrcDotNet;

namespace ShikashiBot
{
    public interface IMessageBroadcaster
    {
        /// <summary>
        /// Requests that a message is broadcasted in and IRC Channel.
        /// </summary>
        /// <param name="message">Message to broadcast.</param>
        /// <param name="channel">Channel to broadcast message in.</param>
        void RequestBroadcast(string message, IrcChannel channel);
    }
}
