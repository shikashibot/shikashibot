﻿using IrcDotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShikashiBot.IRC;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Services;
using ShikashiBot.TwitchAPI;
using System.Collections;
using System.Linq;

namespace ShikashiBot
{
    /// <summary>
    /// Shikashi bot manager manages all the channels that are currently being moderated.
    /// Can represent the current chat moderation bot (main class).
    /// </summary>
    public class ShikashiBotManager : IChannelProvider, IShikashiBotManager
    {
        private ILogger logger;
        private IPersistenceProvider persistenceProvider;
        private ITwitchStreamProvider streamProvider;
        private bool apiOnlyMode;
        private Hashtable channels;

        internal ILoggerFactory LoggerFactory { get; private set; }

        public IUsageStatisticsService StatisticsService { get; private set; }
        public IMessageBroadcaster Broadcaster { get; private set; }
        public string BotUsername { get; private set; }
        
        /// <summary>
        /// The IRC facade.
        /// </summary>
        internal IIrcRelay IrcRelay { get; private set; }

        public IEmoticonProvider EmoteProvider { get; private set; }

        /// <summary>
        /// Creates a new instance of the bot manager.
        /// </summary>
        public ShikashiBotManager(IPersistenceProvider persistenceProvider, 
            ILoggerFactory loggerFactory, IIrcRelay ircRelay, IConfigurationRoot configuration, IUsageStatisticsService statisticsService,
            ITwitchStreamProvider streamProvider, IEmoticonProvider emoteProvider)
        {
            this.channels = new Hashtable();
            this.Broadcaster = new MessageBroadcaster(statisticsService, loggerFactory.CreateLogger<MessageBroadcaster>());
            this.persistenceProvider = persistenceProvider;
            this.streamProvider = streamProvider;
            this.logger = loggerFactory.CreateLogger<ShikashiBotManager>();
            this.StatisticsService = statisticsService;
            this.LoggerFactory = loggerFactory;
            this.EmoteProvider = emoteProvider;
            this.apiOnlyMode = configuration["ApiOnlyMode"] == "True";
            this.BotUsername = configuration["BotUsername"];

            logger.LogDebug($"Creating bot for channel {BotUsername}");
            this.IrcRelay = ircRelay;

            if (apiOnlyMode)
            {
                logger.LogWarning("Bot is in API only mode");
            }
        }

        /// <summary>
        /// Prepares and starts up the bot
        /// </summary>
        /// <param name="loggerFactory">Logger factory</param>
        public void Initialize()
        {
            IrcRelay.MessageReceived += IrcRelay_MessageReceived;
            IrcRelay.ChannelJoined += IrcRelay_ChannelJoined;
            IrcRelay.ModesChanged += IrcRelay_ModesChanged;

            if (!apiOnlyMode)
            {
                IrcRelay.Connect();
            }

            StartModerate();
        }


        /// <summary>
        /// Gets a channel that is currently registered.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>The channel.</returns>
        public Channel GetChannel(string channelName)
        {
            return channels[channelName] as Channel;
        }

        private async void IrcRelay_ModesChanged(string channelName)
        {
            Channel channel = GetChannel(channelName);
            await channel?.PermissionHandler.LoadPermissions();
        }

        private void IrcRelay_ChannelJoined(string channelName, IrcChannel ircChannel)
        {
            Channel channel = GetChannel(channelName);
            channel?.SetIrcChannel(ircChannel);
        }

        private void IrcRelay_MessageReceived(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            Channel channel = GetChannel(channelName);
            messageContext.Channel = channel;

            channel?.ChatHandler.OnMessageReceived(channelName, senderUsername, message, messageContext);
            channel?.StatisticsHandler.AddMessage();
            StatisticsService.AddMessageProcessed();
        }

        /// <summary>
        /// Starts moderating the channels that are registered with the bot.
        /// </summary>
        public void StartModerate()
        {
            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();

            var persistedChannels = (from t in dbContext.Channel
                                     where t.Visiting
                                     select t).ToList();

            foreach (Model.Channel channelModel in persistedChannels)
                ModerateChannel(channelModel, true);

            if (channels.Count == 0)
            {
                var channelModel = new Model.Channel()
                {
                    ChannelName = BotUsername, 
                    Visiting = true
                };

                dbContext.Channel.Add(channelModel);
                dbContext.SaveChanges();

                ModerateChannel(channelModel, true);
            }
        }

        private Channel GetOrCreateChannel(string channelName)
        {
            if (channels.ContainsKey(channelName))
            {
                return (Channel)channels[channelName];
            }
            else
            {
                PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();
                Model.Channel channelModel = dbContext.Channel.Where(p => p.ChannelName == channelName).SingleOrDefault();

                Channel channel = new Channel(channelModel, this, persistenceProvider, LoggerFactory, apiOnlyMode, streamProvider);

                if (!apiOnlyMode)
                {
                    channel.LoadResources().Wait();
                }
                
                this.channels.Add(channelName, channel);
                return channel;
            }
        }

        /// <summary>
        /// Starts moderating a channel.
        /// </summary>
        /// <param name="channelModel">Information about the channel to moderate</param>
        /// <param name="rejoin">If true, the bot is rejoining and channelModel already exists in the database. Otherwise, false.</param>
        /// <returns>Channel that is being moderated.</returns>
        public Channel ModerateChannel(Model.Channel channelModel, bool rejoin = false)
        {
            logger.LogDebug($"Attempting to join channel {channelModel.ChannelName}");
            Channel channel = GetOrCreateChannel(channelModel.ChannelName);

            if (!apiOnlyMode)
            {
                IrcRelay.Join(channelModel.ChannelName);
            }

            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();
            channel.ChannelModel.Visiting = true;


            var entries = dbContext.ChangeTracker.Entries();

            if (!rejoin)
            {
                dbContext.Channel.Add(channel.ChannelModel);
            }
            else
            {
                dbContext.Channel.Update(channel.ChannelModel);
            }

            dbContext.SaveChanges();
            
            return channel;
        }

        /// <summary>
        /// Stops moderating a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        public void StopModeratingChannel(string channelName)
        {
            Channel channel = GetChannel(channelName);
            this.channels.Remove(channelName);

            if (!apiOnlyMode)
            {
                IrcRelay.Leave(channelName);
            }

            channel.ChannelModel.Visiting = false;
            channel.ChatHandler.PhraseStatistics.Destroy();
            channel.StatisticsHandler.Stop();

            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();
            Model.Channel model = (from p in dbContext.Channel
                                   where p.ChannelName == channelName
                                   select p).SingleOrDefault();

            model.Visiting = false;
            dbContext.SaveChanges();
        }
    }
}
