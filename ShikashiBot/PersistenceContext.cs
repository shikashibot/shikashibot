﻿using Microsoft.EntityFrameworkCore;
using ShikashiBot.Model;
using ShikashiBot.Model.Commands;
using ShikashiBot.Model.Filters;

namespace ShikashiBot
{
    /// <summary>
    /// Provides persistence towards the model and the database.
    /// Each DbSet in this class represents a table in the database and is accessible through LINQ.
    /// </summary>
    public class PersistenceContext : DbContext
    {
        public DbSet<Announcement> Announcement { get; set; }
        public DbSet<BannedWord> BannedWord { get; set; }
        public DbSet<BotCommandConfiguration> BotCommandConfiguration { get; set; }
        public DbSet<Model.Channel> Channel { get; set; }
        public DbSet<ChannelCommandConfiguration> ChannelCommandConfiguration { get; set; }
        public DbSet<CustomCommand> CustomCommand { get; set; }
        public DbSet<DefaultCommandConfiguration> DefaultCommandConfiguration { get; set; }
        public DbSet<FilterConfiguration> FilterConfiguration { get; set; }
        public DbSet<Regular> Regular { get; set; }
        public DbSet<WhitelistedDomain> WhitelistedDomain { get; set; }
        public DbSet<TwitchCredentials> TwitchCredentials { get; set; }
        public DbSet<CapsFilter> CapsFilter { get; set; }
        public DbSet<DomainFilter> DomainFilter { get; set; }
        public DbSet<EmoteFilter> EmoteFilter { get; set; }
        public DbSet<LengthFilter> LengthFilter { get; set; }
        public DbSet<RepeatFilter> RepeatFilter { get; set; }
        public DbSet<SymbolFilter> SymbolFilter { get; set; }
        public DbSet<WordFilter> WordFilter { get; set; }
        public DbSet<FilterDescription> FilterDescription { get; set; }

        public DbSet<ChannelStatistics> ChannelStatistics { get; set; }

        public PersistenceContext(DbContextOptions<PersistenceContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BotCommandConfiguration>()
                .HasKey(p => new { p.ChannelName, p.CommandId });

            modelBuilder.Entity<Announcement>()
                .HasKey(p => new { p.ChannelName, p.Id });

            modelBuilder.Entity<Announcement>()
                .Property(p => p.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<BannedWord>()
                .HasKey(p => new { p.ChannelName, p.Word });
            
            modelBuilder.Entity<Model.Channel>()
                .HasKey(p => p.ChannelName);

            modelBuilder.Entity<ChannelCommandConfiguration>()
                .HasKey(p => p.CommandId);

            modelBuilder.Entity<CustomCommand>()
                .HasKey(p => new { p.ChannelName, p.CommandId });

            modelBuilder.Entity<DefaultCommandConfiguration>()
                .HasKey(p => p.CommandId);

            modelBuilder.Entity<FilterConfiguration>().Property(p => p.Id)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<Regular>()
                .HasKey(p => new { p.ChannelName, p.TwitchUsername });

            modelBuilder.Entity<WhitelistedDomain>()
                .HasKey(p => new { p.ChannelName, p.Domain });

            modelBuilder.Entity<TwitchCredentials>()
                .HasKey(p => p.TwitchUsername);

            modelBuilder.Entity<TwitchCredentials>()
                .HasAlternateKey(p => p.PublicAccessToken);

            modelBuilder.Entity<FilterDescription>()
                .HasKey(p => p.FilterId);

            modelBuilder.Entity<ChannelStatistics>()
                .HasKey(p => new { p.ChannelName, p.Timestamp });
        }
    }
}
