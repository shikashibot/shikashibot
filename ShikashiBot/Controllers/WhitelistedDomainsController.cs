﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.HttpExtensions;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for managing the whitelisted domain list for a channel.
    /// </summary>
    [Route("/api/channels/{channelName}/whitelistedDomains")]
    public class WhitelistedDomainsController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public WhitelistedDomainsController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Gets a list of all the whitelisted domains.
        /// </summary>
        /// <param name="channelName">Channel to get the whitelisted doains for.</param>
        /// <returns>A list of the whitelisted domains.</returns>
        [HttpGet]
        public async Task<IActionResult> GetWhitelistedDomains(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var domains = await (from p in dbContext.WhitelistedDomain
                                 where p.ChannelName == channelName
                                 select p.Domain).ToListAsync();

            return Ok(domains);
        }

        /// <summary>
        /// Adds a domain to the whitelist
        /// </summary>
        /// <param name="channelName">Channel to remove the domain from.</param>
        /// <param name="viewModel">Domain to remove</param>
        /// <returns>An OK created</returns>
        [HttpPut]
        public async Task<IActionResult> AddWhitelistedDomains(string channelName, [FromBody] WhitelistedDomainViewModel viewModel)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (await dbContext.WhitelistedDomain.Where(t => t.ChannelName == channelName && t.Domain == viewModel.Domain).AnyAsync())
            {
                return this.Conflict();
            }

            var model = new Model.WhitelistedDomain
            {
                ChannelName = channelName,
                Domain = viewModel.Domain
            };

            dbContext.WhitelistedDomain.Add(model);
            await dbContext.SaveChangesAsync();

            return Created("/", viewModel);
        }

        /// <summary>
        /// Removes a domain from the whitelist.
        /// </summary>
        /// <param name="channelName">Channel to manage the whitelist for</param>
        /// <param name="domain">Domain to remove</param>
        /// <returns>Not found if there is no entry, otherwise an OK</returns>
        [HttpDelete("{domain}")]
        public async Task<IActionResult> RemoveDomain(string channelName, string domain)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }

            var existingEntry = await (from p in dbContext.WhitelistedDomain
                                       where p.ChannelName == channelName && p.Domain == domain
                                       select p).SingleOrDefaultAsync();

            if (existingEntry == null)
            {
                return NotFound();
            }
            else
            {
                dbContext.WhitelistedDomain.Remove(existingEntry);
                await dbContext.SaveChangesAsync();
                return Ok();
            }
        }
    }
}
