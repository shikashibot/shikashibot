﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Commands;
using ShikashiBot.Permissions;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller used to get all commands.
    /// </summary>
    [Route("/api/channels/{channelName}/customCommands", Name = "CommandOverviewController")]
    public class CustomCommandOverviewController : Controller
    {
        private PersistenceContext persistenceContext;
        private IAuthorizationService authorizationService;

        public CustomCommandOverviewController(PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.persistenceContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Getting all custom commands.
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns>List of Custom Commands</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllCustomCommands(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var commands = persistenceContext.CustomCommand.Where(t => t.ChannelName == channelName);

            List<CustomCommandViewModel> allCommands = new List<CustomCommandViewModel>();

            foreach (CustomCommand command in commands)
            {
                CustomCommandViewModel viewModel = new CustomCommandViewModel
                {
                    Output = command.Output,
                    CommandId = command.CommandId,
                    MinRank = (UserType)command.MinRank,
                    IsActive = command.IsActive
                };

                allCommands.Add(viewModel);
            }

            return Ok(allCommands);
        }
    }
}
