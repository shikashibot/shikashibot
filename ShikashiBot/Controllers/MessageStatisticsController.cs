﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Provides enpoint to access which messages are most frequently used in the chat.
    /// </summary>
    [Route("/api/channels/{channelName}/statistics")]
    public class MessageStatisticsController : Controller
    {
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;
        private IShikashiBotManager botManager;

        public MessageStatisticsController(PersistenceContext dbContext, IAuthorizationService authorizationService, IShikashiBotManager botManager)
        {
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
            this.botManager = botManager;
        }

        /// <summary>
        /// Gets top 20 messages from the chat.
        /// </summary>
        /// <param name="channelName">Name of the channel</param>
        /// <returns></returns>
        [HttpGet("messages")]
        public async Task<IActionResult> GetMessageStatistics(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            Channel channel = botManager.GetChannel(channelName);

            if (channel == null)
            {
                return NotFound();
            }

            var statistics = channel.ChatHandler.PhraseStatistics.GetTop(10)
                .Select(t => new GeneralStatisticsViewModel
                {
                    Item = t.Phrase,
                    Score = t.Score
                });

            return Ok(statistics);
        }

        [HttpGet("emotes")]
        public async Task<IActionResult> GetEmoteStatistics(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            Channel channel = botManager.GetChannel(channelName);

            if (channel == null)
            {
                return NotFound();
            }

            var statistics = channel.ChatHandler.EmoteStatistics.GetTop(10)
                .Select(t => new GeneralStatisticsViewModel
                {
                    Item = t.Phrase,
                    Score = t.Score
                });

            return Ok(statistics);
        }
    }
}
