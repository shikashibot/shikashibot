﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.Model.Commands;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for managing command configurations.
    /// </summary>
    [Route("/api/channels/{channelName}/commands")]
    public class CommandController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public CommandController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Gets all the commands and their configuration for a channel.
        /// </summary>
        /// <param name="channelName">Channel name</param>
        /// <returns>Representation of the current command configurations</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllCommands(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var defaultCommands = await (from p in dbContext.DefaultCommandConfiguration
                                         select p).ToListAsync();

            var channelCommands = await (from p in dbContext.ChannelCommandConfiguration
                                         where p.Channel.ChannelName == channelName
                                         select p).ToListAsync();

            var views = new Dictionary<string, CommandConfigurationViewModel>();

            foreach (ChannelCommandConfiguration config in channelCommands)
            {
                string description = (from p in defaultCommands where p.CommandId == config.CommandId select p.Description).FirstOrDefault();

                views.Add(config.CommandId, new CommandConfigurationViewModel
                {
                    CommandId = config.CommandId,
                    MinRank = config.MinRank,
                    Prefix = config.Prefix,
                    Enabled = config.Enabled,
                    Description = description
                });
            }

            foreach (DefaultCommandConfiguration config in defaultCommands)
            {
                if (!views.ContainsKey(config.CommandId))
                {
                    views.Add(config.CommandId, new CommandConfigurationViewModel
                    {
                        CommandId = config.CommandId,
                        MinRank = config.MinRank,
                        Prefix = config.Prefix,
                        Description = config.Description
                    });
                }
            }

            return Ok(views.Values.ToList());
        }

        /// <summary>
        /// Modifies an existing command.
        /// </summary>
        /// <param name="channelName">Name of the channel where the command belongs</param>
        /// <param name="commandId">Id of the command to configure</param>
        /// <param name="viewModel">New command configuration</param>
        /// <returns></returns>
        [HttpPut("{commandId}")]
        public async Task<IActionResult> ModifyCommand(string channelName, string commandId, [FromBody] CommandConfigurationViewModel viewModel)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            // Check if the command is existing or not
            var template = await (from p in dbContext.DefaultCommandConfiguration
                                  where p.CommandId == commandId
                                  select p).SingleOrDefaultAsync();

            if (template == null)
            {
                return NotFound();
            }

            if (!viewModel.MinRank.HasValue)
            {
                viewModel.MinRank = template.MinRank;
            }
                
            if (string.IsNullOrEmpty(viewModel.Prefix))
            {
                viewModel.Prefix = template.Prefix;
            }

            var existingConfig = await (from p in dbContext.ChannelCommandConfiguration
                                        where p.Channel.ChannelName == channelName && p.CommandId == commandId
                                        select p).SingleOrDefaultAsync();

            if (existingConfig == null)
            {
                var config = new ChannelCommandConfiguration
                {
                    CommandId = commandId,
                    MinRank = viewModel.MinRank.Value,
                    Prefix = viewModel.Prefix,
                    Enabled = viewModel.Enabled,
                    Channel = await dbContext.Channel.Where(t => t.ChannelName == channelName).SingleOrDefaultAsync()
                };

                dbContext.ChannelCommandConfiguration.Add(config);
                await dbContext.SaveChangesAsync();
            }
            else
            {
                existingConfig.MinRank = viewModel.MinRank.Value;
                existingConfig.Prefix = viewModel.Prefix;
                existingConfig.Enabled = viewModel.Enabled;
                await dbContext.SaveChangesAsync();
            }

            botManager.GetChannel(channelName)?.ChatHandler.ReloadCommands(channelName, dbContext);

            viewModel.CommandId = commandId;
            return Ok(viewModel);
        }
    }
}
