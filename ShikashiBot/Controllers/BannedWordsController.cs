﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.HttpExtensions;
using ShikashiBot.Messaging.Filtering;
using ShikashiBot.Model;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for operations (add, get, remove) related to banned words.
    /// </summary>
    [Route("/api/channels/{channelName}/bannedWords", Name = "BannedWordsController")]
    public class BannedWordsController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public BannedWordsController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Add new bannedWord.
        /// </summary>
        /// <param name="bannedWordViewModel"></param>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>Status 201-Created if added.</returns>
        [HttpPut]
        public async Task<ActionResult> AddBannedWord([FromBody] BannedWordViewModel bannedWordViewModel, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            if (bannedWordViewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            BannedWord bannedWord = new BannedWord()
            {
                Word = bannedWordViewModel.Word,
                ChannelName = channelName
            };
            
            BannedWord existingWord = await dbContext.BannedWord.Where(t => t.ChannelName == bannedWord.ChannelName && t.Word == bannedWord.Word).FirstOrDefaultAsync();
            if (existingWord == null)
            {
                dbContext.BannedWord.Add(bannedWord);
                await dbContext.SaveChangesAsync();

                ShikashiBot.Channel channel = botManager.GetChannel(channelName);

                if (channel != null)
                {
                    WordFilterHandler wordFilterHandler = (WordFilterHandler)channel.GetMessageFilterHandler(typeof(WordFilterHandler));
                    wordFilterHandler?.ReloadBannedWords();
                }

                return Created("/", bannedWord);
            }

            return this.Accepted();
        }

        /// <summary>
        /// Get all banned words for one channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>List of banned words.</returns>
        [HttpGet]
        public async Task<ActionResult> GetAllBannedWords(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var words = await (from p in dbContext.BannedWord
                               where p.ChannelName == channelName
                               select p.Word).ToListAsync();

            return Ok(words);
        }

        /// <summary>
        /// Deletes a banned word.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="word">Word which should be deleted.</param>
        /// <returns>Status OK if word is deleted or HTTPNotFound if the word doesn't exists.</returns>
        [HttpDelete("{word}")]
        public async Task<IActionResult> DeleteBannedWord(string channelName, string word)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }

            BannedWord existingWord = await dbContext.BannedWord.Where(t => t.ChannelName == channelName && t.Word == word).FirstOrDefaultAsync();

            if (existingWord == null)
            {
                return NotFound();
            }

            dbContext.BannedWord.Remove(existingWord);
            await dbContext.SaveChangesAsync();

            ShikashiBot.Channel channel = botManager.GetChannel(channelName);
            if (channel != null)
            {
                WordFilterHandler wordFilterHandler = (WordFilterHandler)channel.GetMessageFilterHandler(typeof(WordFilterHandler));
                wordFilterHandler.ReloadBannedWords();
            }

            return Ok();
        }
    }
}

