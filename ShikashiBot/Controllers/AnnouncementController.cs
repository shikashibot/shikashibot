﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.Model;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for operations (stop, add, get) related to an announcement.
    /// </summary>
    [Route("/api/channels/{channelName}/announcements", Name = "AnnouncementController")]
    public class AnnouncementController : Controller
    {
        private IShikashiBotManager botManager;
        private IAuthorizationService authorizationService;
        private PersistenceContext dbContext;

        public AnnouncementController(IShikashiBotManager botManager, IAuthorizationService authorizationService, PersistenceContext dbContext)
        {
            this.dbContext = dbContext;
            this.botManager = botManager;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Stops an announcement from a specific channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="announcementId">Announcement id.</param>
        /// <returns>Status OK if its stoped or HTTPNotFound if the announcement doesn't exists.</returns>
        [HttpDelete("{announcementId}")]
        public async Task<IActionResult> StopAnnouncement(string channelName, int announcementId)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }

            var announcement = await (from p in dbContext.Announcement
                                      where p.ChannelName == channelName && p.Id == announcementId
                                      select p).SingleOrDefaultAsync();

            if (announcement == null)
            {
                return NotFound();
            }

            dbContext.Announcement.Remove(announcement);
            await dbContext.SaveChangesAsync();

            Channel loadedChannel = botManager.GetChannel(channelName);

            loadedChannel?.AnnouncementHandler.ReloadAnnouncement(announcementId, dbContext);
            
            return Ok();
        }

        /// <summary>
        /// Add new announcement.
        /// </summary>
        /// <param name="announcementModel"></param>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>Status Ok if added.</returns>
        [HttpPost]
        public async Task<IActionResult> AddAnnouncement([FromBody] AnnouncementViewModel viewModel, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            if (viewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var channel = await (from p in dbContext.Channel
                                 where p.ChannelName == channelName
                                 select p).SingleOrDefaultAsync();

            if (channel == null)
            {
                return NotFound();
            }

            Announcement model = new Announcement
            {
                ChannelName = channelName,
                IsActive = viewModel.IsActive,
                Message = viewModel.Message,
                SecondsInterval = viewModel.SecondsInterval
            };

            dbContext.Announcement.Add(model);
            await dbContext.SaveChangesAsync();

            Channel loadedChannel = botManager.GetChannel(channelName);

            loadedChannel?.AnnouncementHandler.AddAnnouncement(model);

            return Ok();
        }

        /// <summary>
        /// Get one announcement by ID.
        /// </summary>
        /// <param name="announcementId">Announcement id.</param>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>A representation of the requested Announcement.</returns>
        [HttpGet("{announcementId}", Name = "GetAnnouncement")]
        public async Task<IActionResult> GetAnnouncement(int announcementId, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var announcement = await (from p in dbContext.Announcement
                                      where p.Id == announcementId && p.ChannelName == channelName
                                      select p).SingleOrDefaultAsync();

            if (announcement == null)
            {
                return NotFound();
            }

            AnnouncementViewModel viewModel = new AnnouncementViewModel
            {
                Id = announcement.Id,
                IsActive = announcement.IsActive,
                Message = announcement.Message,
                SecondsInterval = announcement.SecondsInterval
            };

            return Ok(viewModel);
        }

        /// <summary>
        /// Get all announcements for one channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>List of announcements.</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllAnnouncements(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var announcements = from p in dbContext.Announcement
                                where p.ChannelName == channelName
                                select p;

            List<AnnouncementViewModel> viewModels = new List<AnnouncementViewModel>();

            foreach (Announcement model in announcements)
            {
                viewModels.Add(new AnnouncementViewModel
                {
                    Id = model.Id,
                    IsActive = model.IsActive,
                    Message = model.Message,
                    SecondsInterval = model.SecondsInterval
                });
            }

            return Ok(viewModels);
        }

        /// <summary>
        /// Update an announcement.
        /// </summary>
        /// <param name="announcementId"></param>
        /// <param name="channelName"></param>
        /// <param name="announcementModel"></param>
        /// <returns>The updated/modified announcement.</returns>
        [HttpPut("{announcementId}")]
        public async Task<IActionResult> UpdateAnnouncement(int announcementId, string channelName, [FromBody] AnnouncementViewModel viewModel)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }

            if (viewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            var announcement = await (from p in dbContext.Announcement
                                      where p.Id == announcementId && p.ChannelName == channelName
                                      select p).SingleOrDefaultAsync();

            if (announcement == null)
            {
                return NotFound();
            }

            announcement.IsActive = viewModel.IsActive;
            announcement.Message = viewModel.Message;
            announcement.SecondsInterval = viewModel.SecondsInterval;
            
            await dbContext.SaveChangesAsync();

            Channel loadedChannel = botManager.GetChannel(channelName);

            loadedChannel?.AnnouncementHandler.ReloadAnnouncement(announcementId, dbContext);

            return Ok(viewModel);
        }
    }
}
