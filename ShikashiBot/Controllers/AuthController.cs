﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.TwitchAPI;
using ShikashiBot.ViewModels.Auth;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Resource related to authentication of the user
    /// </summary>
    [Route("/api/auth", Name = "AuthController")]
    public class AuthController : Controller
    {
        private ITwitchAuthService twitchAuthorizationService;
        private IAuthorizationService authorizationService;

        public AuthController(ITwitchAuthService twitchAuthorizationService, IAuthorizationService authorizationService)
        {
            this.twitchAuthorizationService = twitchAuthorizationService;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Validates Twitch credentials for a user and provides (if valid), a token that can be used
        /// for further access on API-endpoints in this system.
        /// </summary>
        /// <param name="model">Access token from Twitch OAuth process</param>
        /// <returns>A token that can be used for accessing API endpoints in this system</returns>
        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var tokenInfo = await twitchAuthorizationService.ObtainAndStoreCredentialsAsync(model.Token);
            if (tokenInfo == null)
            {
                return BadRequest();
            }

            LoginViewModel viewModel = new LoginViewModel()
            {
                Token = tokenInfo.PublicAccessToken,
                ChannelName = tokenInfo.TwitchUsername
            };

            return Created("/", viewModel);
        }

        /// <summary>
        /// Gets information about a token currently being used.
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetCurrentToken")]
        public async Task<IActionResult> GetCurrentToken()
        {
            string authorizationToken = HttpContext.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorizationToken))
            {
                return Unauthorized();
            }

            var token = await twitchAuthorizationService.GetCredentialsAsync(authorizationToken.Replace("Bearer ", string.Empty));

            if (token == null)
            {
                return Unauthorized();
            }

            LoginViewModel viewModel = new LoginViewModel()
            {
                Token = token.PublicAccessToken,
                ChannelName = token.TwitchUsername
            };

            return Ok(viewModel);
        }

        /// <summary>
        /// Invalidates the token being used.
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> InvalidateToken()
        {
            string authorizationToken = HttpContext.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorizationToken))
            {
                return Unauthorized();
            }

            var token = await twitchAuthorizationService.GetCredentialsAsync(authorizationToken.Replace("Bearer ", string.Empty));
            if (token == null)
            {
                return Unauthorized();
            }

            await twitchAuthorizationService.InvalidateTokenAsync(token);

            return Ok();
        }
    }
}
