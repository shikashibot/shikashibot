﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for operations related to a channel.
    /// </summary>
    [Route("api/channels", Name = "ChannelController")]
    public class ChannelController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        /// <summary>
        /// Creates a new channel controller.
        /// </summary>
        /// <param name="botManager">Bot manager</param>
        public ChannelController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        [HttpDelete("{channelName}")]
        public async Task<IActionResult> LeaveChannel(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }

            Channel channel = botManager.GetChannel(channelName);
            if (channel == null)
            {
                return NotFound();
            }

            botManager.StopModeratingChannel(channelName);
            return Ok();
        }

        /// <summary>
        /// Makes the bot start moderating a channel
        /// </summary>
        /// <param name="channel">The channel to start moderate</param>
        /// <returns>A representation of the added channel resource</returns>
        [HttpPost]
        public async Task<IActionResult> EnterChannel([FromBody] ChannelViewModel viewModel)
        {
            if (!await authorizationService.AuthorizeAsync(User, viewModel, Operations.Create))
            {
                return Unauthorized();
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Model.Channel channel = await (from p in dbContext.Channel
                                           where p.ChannelName == viewModel.ChannelName
                                           select p).SingleOrDefaultAsync();
            if (channel == null)
            {
                // Should never happen
                return NotFound(); 
            }

            botManager.ModerateChannel(channel, true);
            return CreatedAtRoute("GetChannel", new { controller = "ChannelController", channelName = viewModel.ChannelName }, viewModel);
        }

        /// <summary>
        /// Gets an existing channel.
        /// </summary>
        /// <param name="channelName">Name of the channel to find.</param>
        /// <returns>A representation of the added channel.</returns>
        [HttpGet("{channelName}", Name = "GetChannel")]
        public async Task<IActionResult> GetChannel(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            Model.Channel channel = await (from p in dbContext.Channel
                                           where p.ChannelName == channelName
                                           select p).SingleOrDefaultAsync();

            if (channel == null)
            {
                return NotFound();
            }

            return Ok(new ChannelViewModel { ChannelName = channel.ChannelName, Visiting = channel.Visiting });
        }
    }
}
