﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for message length filters.
    /// </summary>
    [Route("/api/channels/{channelName}/filters/length")]
    public class LengthFilterController : FilterController<LengthFilter>
    {
        /// <summary>
        /// Creates a new length filter controller.
        /// </summary>
        /// <param name="botManager"></param>
        /// <param name="persistenceProvider"></param>
        /// <param name="authorizationService"></param>
        public LengthFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a length filter model from a general filter view model.
        /// </summary>
        /// <param name="model">The view model</param>
        /// <returns>A new length filter model</returns>
        public override LengthFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new LengthFilter
            {
                MaxLength = model.Limit,
                TimeoutSeconds = model.TimeoutSeconds,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a general view model from an existing model.
        /// </summary>
        /// <param name="model">Model to create view model from</param>
        /// <returns>A general view model of the filter</returns>
        public override object CreateViewModel(LengthFilter model)
        {
            return new GeneralFilterViewModel()
            {
                Limit = model.MaxLength,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Length",
                Enabled = model.Enabled
            };
        }
        
        /// <summary>
        /// Updates the filter model with new configuration.
        /// </summary>
        /// <param name="model">New configuration</param>
        /// <param name="filter">Model to update</param>
        public override void UpdateFilter(GeneralFilterViewModel model, LengthFilter filter)
        {
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.MaxLength = model.Limit;
            filter.Enabled = model.Enabled;
        }
    }
}
