﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.Model.Filters;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// General controller for controllers related to filter configuration. Defines basic CRUD operations.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class FilterController<T> : Controller where T : FilterConfiguration
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        /// <summary>
        /// Creates a view model from an existing model.
        /// </summary>
        /// <param name="model">Model to create a view model of</param>
        /// <returns>A view model</returns>
        public abstract object CreateViewModel(T model);

        /// <summary>
        /// Creates a model from a view model.
        /// </summary>
        /// <param name="model">View model to create the model from</param>
        /// <returns>A model</returns>
        public abstract T CreateModelFromViewModel(GeneralFilterViewModel model);

        /// <summary>
        /// Updates the filter with what has been set in the view model.
        /// </summary>
        /// <param name="model">New configuration</param>
        /// <param name="filter">Filter to update</param>
        public abstract void UpdateFilter(GeneralFilterViewModel model, T filter);
        
        /// <summary>
        /// Gets the database set for the concrete filter.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        private DbSet<T> GetDbSet()
        {
            Type persistenceContextType = typeof(PersistenceContext);
            var getMethod = persistenceContextType.GetMethods().Where(t => t.ReturnType == typeof(DbSet<T>)).FirstOrDefault();

            return (DbSet<T>)getMethod.Invoke(dbContext, null);
        }

        /// <summary>
        /// Creates a new filter controller.
        /// </summary>
        /// <param name="botManager">Bot manager</param>
        /// <param name="persistenceProvider">Persistence provider</param>
        /// <param name="authorizationService">Authorization service.</param>
        public FilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Gets existing filter config for a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel</param>
        /// <returns>A representation of the filter config</returns>
        [HttpGet]
        public async Task<IActionResult> GetConfiguration(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            var viewModel = GetFilterViewModel(channelName);

            if (viewModel == null)
            {
                return NotFound();
            }

            AppendDescription(viewModel);

            return Ok(viewModel);
        }

        public void AppendDescription(object viewModel)
        {
            if (viewModel is GeneralFilterViewModel)
            {
                GeneralFilterViewModel generalFilterViewModel = (GeneralFilterViewModel)viewModel;
                generalFilterViewModel.Description = (from p in dbContext.FilterDescription
                                                      where p.FilterId == generalFilterViewModel.Identifier
                                                      select p.Description).FirstOrDefault();
            }
        }

        /// <summary>
        /// Gets filter view model for a channel name.
        /// </summary>
        /// <param name="channelName">Channel name to get view model for.</param>
        /// <returns>View model of the filter if found, otherwise null</returns>
        public object GetFilterViewModel(string channelName)
        {
            var filters = (from p in dbContext.FilterConfiguration
                          where p.ChannelName == channelName && p is T
                          select p).ToList();

            var filter = filters.FirstOrDefault() as T;
            if (filter == null)
            {
                var defaultConfig = (from p in dbContext.FilterConfiguration
                                     where p.ChannelName == null && p is T
                                     select p).SingleOrDefault() as T;

                return CreateViewModel(defaultConfig);
            }
            else
            {
                return CreateViewModel(filter);
            }
        }

        /// <summary>
        /// Modifies an existing filter.
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="channelName">Name of the channel to modify filter for</param>
        /// <returns>An HTTP OK if filter was modified</returns>
        [HttpPut]
        public async Task<IActionResult> ModifyFilter([FromBody] GeneralFilterViewModel model, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var filter = await (from p in GetDbSet()
                                where p.ChannelName == channelName
                                select p).SingleOrDefaultAsync() as T;

            if (filter == null)
            {
                T filterModel = CreateModelFromViewModel(model);
                filterModel.ChannelName = channelName;
                filterModel.Channel = dbContext.Channel.Where(t => t.ChannelName == channelName).Single();

                GetDbSet().Add(filterModel);
            }
            else
            {
                UpdateFilter(model, filter);

                GetDbSet().Update(filter);
            }

            await dbContext.SaveChangesAsync();
            botManager.GetChannel(channelName)?.ReloadFilters();

            return Ok();
        }
    }
}
