﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for managing caps filters.
    /// </summary>
    [Route("/api/channels/{channelName}/filters/caps")]
    public class CapsFilterController : FilterController<CapsFilter>
    {
        public CapsFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        { }

        /// <summary>
        /// Creates a new caps filter form an existing view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override CapsFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new CapsFilter
            {
                CapsLimit = model.Limit,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a generalized view model from an existing caps filter model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(CapsFilter model)
        {
            return new GeneralFilterViewModel
            {
                Limit = model.CapsLimit,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Caps",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates an exiting filter (model) with a view model.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="filter"></param>
        public override void UpdateFilter(GeneralFilterViewModel model, CapsFilter filter)
        {
            filter.CapsLimit = model.Limit;
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
