﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for accessing an overview of all the filters and their configuration.
    /// </summary>
    [Route("/api/channels/{channelName}/filters")]
    public class FilterOverviewController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public FilterOverviewController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Gets all the filters that are registered for a channel.
        /// </summary>
        /// <param name="channelName">Channel to get all the filters for.</param>
        /// <returns>A list of all the filter configuration</returns>
        [HttpGet]
        public async Task<IActionResult> GetFilterOverview(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Read))
            {
                return Unauthorized();
            }

            List<object> viewModels = new List<object>();
            Type generalType = typeof(FilterController<>);

            var filterControllerTypes = generalType.Assembly.GetTypes().Where(t => t.Namespace == generalType.Namespace && t.BaseType.Name == generalType.Name);

            foreach (Type filterControllerType in filterControllerTypes)
            {
                // Instanciate a new instance of the controller to get the view model
                var controller = Activator.CreateInstance(filterControllerType, botManager, dbContext, authorizationService);
                var methodInfo = filterControllerType.GetMethod("GetFilterViewModel");

                var viewModel = methodInfo.Invoke(controller, new object[] { channelName });

                if (viewModel != null)
                {
                    filterControllerType.GetMethod("AppendDescription").Invoke(controller, new object[] { viewModel });
                    viewModels.Add(viewModel);
                }
            }

            return Ok(viewModels);
        }
    }
}
