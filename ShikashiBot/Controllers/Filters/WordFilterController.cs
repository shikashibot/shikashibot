﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    [Route("/api/channels/{channelName}/filters/word")]
    public class WordFilterController : FilterController<WordFilter>
    {
        /// <summary>
        /// Creates a new word filter controller.
        /// </summary>
        /// <param name="botManager"></param>
        /// <param name="persistenceProvider"></param>
        /// <param name="authorizationService"></param>
        public WordFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a word filter model from a general filter view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override WordFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new WordFilter
            {
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a general view model from an existing model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(WordFilter model)
        {
            return new GeneralFilterViewModel
            {
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Word",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates the filter model with new configuration.
        /// </summary>
        /// <param name="model">New configuration</param>
        /// <param name="filter">Model to update</param>
        public override void UpdateFilter(GeneralFilterViewModel model, WordFilter filter)
        {
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
