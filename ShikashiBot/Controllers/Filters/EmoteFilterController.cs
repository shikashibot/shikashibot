﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for configuring the emote filter.
    /// </summary>
    [Route("/api/channels/{channelName}/filters/emote")]
    public class EmoteFilterController : FilterController<EmoteFilter>
    {
        public EmoteFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a new emote filter model from an existing view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override EmoteFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new EmoteFilter
            {
                EmoteLimit = model.Limit,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a view model from an existing emote filter model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(EmoteFilter model)
        {
            return new GeneralFilterViewModel
            {
                Limit = model.EmoteLimit,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Emote",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates an existing filter with new configuration.
        /// </summary>
        /// <param name="model">View model with new configuration</param>
        /// <param name="filter">Filter to configure</param>
        public override void UpdateFilter(GeneralFilterViewModel model, EmoteFilter filter)
        {
            filter.EmoteLimit = model.Limit;
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
