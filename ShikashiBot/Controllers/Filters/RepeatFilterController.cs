﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for managing the repetition filter.
    /// </summary>
    [Route("/api/channels/{channelName}/filters/repeat")]
    public class RepeatFilterController : FilterController<RepeatFilter>
    {
        public RepeatFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a new model from a view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override RepeatFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new RepeatFilter
            {
                OutputMessage = model.OutputMessage,
                TimeoutSeconds = model.TimeoutSeconds,
                WordLimit = model.Limit,
                RemoveType = model.RemoveType,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a view model from an existing model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(RepeatFilter model)
        {
            return new GeneralFilterViewModel
            {
                Limit = model.WordLimit,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Repeat",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates a filter with new config.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="filter"></param>
        public override void UpdateFilter(GeneralFilterViewModel model, RepeatFilter filter)
        {
            filter.WordLimit = model.Limit;
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
