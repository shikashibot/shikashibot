﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for managing the symbol filter.
    /// </summary>
    [Route("/api/channels/{channelName}/filters/symbol")]
    public class SymbolFilterController : FilterController<SymbolFilter>
    {
        public SymbolFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a new model from an existing view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override SymbolFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new SymbolFilter
            {
                MaxCount = model.Limit,
                TimeoutSeconds = model.TimeoutSeconds,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a view model from an existing model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(SymbolFilter model)
        {
            return new GeneralFilterViewModel
            {
                Limit = model.MaxCount,
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Symbol",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates the filter with new configuration.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="filter"></param>
        public override void UpdateFilter(GeneralFilterViewModel model, SymbolFilter filter)
        {
            filter.MaxCount = model.Limit;
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
