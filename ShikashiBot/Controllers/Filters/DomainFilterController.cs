﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Model.Filters;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Controllers.Filters
{
    /// <summary>
    /// Controller for managing the domain filter
    /// </summary>
    [Route("/api/channels/{channelName}/filters/domain")]
    public class DomainFilterController : FilterController<DomainFilter>
    {
        public DomainFilterController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService) 
            : base(botManager, dbContext, authorizationService)
        {
        }

        /// <summary>
        /// Creates a domain filter model from an existing view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override DomainFilter CreateModelFromViewModel(GeneralFilterViewModel model)
        {
            return new DomainFilter
            {
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Creates a new view model from an existing model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override object CreateViewModel(DomainFilter model)
        {
            return new GeneralFilterViewModel
            {
                OutputMessage = model.OutputMessage,
                RemoveType = model.RemoveType,
                TimeoutSeconds = model.TimeoutSeconds,
                Identifier = "Domain",
                Enabled = model.Enabled
            };
        }

        /// <summary>
        /// Updates an existing filter with new configuration.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="filter"></param>
        public override void UpdateFilter(GeneralFilterViewModel model, DomainFilter filter)
        {
            filter.OutputMessage = model.OutputMessage;
            filter.RemoveType = model.RemoveType;
            filter.TimeoutSeconds = model.TimeoutSeconds;
            filter.Enabled = model.Enabled;
        }
    }
}
