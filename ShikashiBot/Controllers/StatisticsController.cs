﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace ShikashiBot.Controllers
{
    public class StatisticsController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Memory"] = ((float)Process.GetCurrentProcess().WorkingSet64) / (1024 * 1024);
            return View();
        }
    }
}
