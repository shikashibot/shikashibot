﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.HttpExtensions;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for managing the regulars for a channel.
    /// </summary>
    [Route("/api/channels/{channelName}/regulars")]
    public class RegularsController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public RegularsController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Returns all the regulars for a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>The regulars for a channel.</returns>
        [HttpGet]
        public async Task<ActionResult> GetRegulars(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            var regulars = await (from p in dbContext.Regular
                                  where p.ChannelName == channelName
                                  select p.TwitchUsername).ToListAsync();

            return Ok(regulars);
        }

        /// <summary>
        /// Adds a regular to a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="regular">The regular to add</param>
        /// <returns>HTTP ok with the added regular</returns>
        [HttpPut]
        public async Task<ActionResult> AddRegular(string channelName, [FromBody] TwitchUserViewModel regular)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            if (regular == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            if (await dbContext.Regular.Where(t => t.ChannelName == channelName && t.TwitchUsername == regular.Username).AnyAsync())
            {
                return this.Conflict();
            }

            var regularModel = new Model.Regular
            {
                ChannelName = channelName,
                TwitchUsername = regular.Username
            };

            dbContext.Regular.Add(regularModel);

            await dbContext.SaveChangesAsync();

            botManager.GetChannel(channelName)?.ReloadFilters();
            return Created("/", regular);
        }

        /// <summary>
        /// Removes a regular for a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="regular">Regular to remove</param>
        /// <returns>HTTP ok</returns>
        [HttpDelete("{regular}")]
        public async Task<ActionResult> RemoveRegular(string channelName, string regular)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }

            var regularModel = await (from p in dbContext.Regular
                                      where p.ChannelName == channelName && p.TwitchUsername == regular
                                      select p).SingleOrDefaultAsync();

            if (regularModel == null)
            {
                return NotFound();
            }

            dbContext.Regular.Remove(regularModel);

            await dbContext.SaveChangesAsync();
            botManager.GetChannel(channelName)?.ReloadFilters();

            return Ok();
        }
    }
}