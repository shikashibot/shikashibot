﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShikashiBot.HttpExtensions;
using ShikashiBot.Model.Commands;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller related to Custom Commands (add,update,delete operations).
    /// </summary>
    [Route("/api/channels/{channelName}/customCommands/{commandId}", Name = "CommandController")]
    public class CustomCommandController : Controller
    {
        private IShikashiBotManager botManager;
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public CustomCommandController(IShikashiBotManager botManager, PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.botManager = botManager;
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Add new command. 
        /// </summary>
        /// <param name="customcommandViewModel"></param>
        /// <param name="channelName"></param>
        /// <param name="commandId"></param>
        /// <returns>Status code.</returns>
        [HttpPost]
        public async Task<IActionResult> AddCommand([FromBody] CustomCommandViewModel customCommandViewModel, string channelName, string commandId)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            if (customCommandViewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var channel = await (from p in dbContext.Channel
                                 where p.ChannelName == channelName
                                 select p).SingleOrDefaultAsync();

            if (channel == null)
            {
                return NotFound();
            }

            var existingCommand = await (from p in dbContext.CustomCommand
                                         where p.CommandId == commandId && p.ChannelName == channelName
                                         select p).SingleOrDefaultAsync();

            if (existingCommand != null)
            {
                return this.Conflict();
            }

            CustomCommand customCommand = new CustomCommand()
            {
                CommandId = commandId,
                ChannelName = channelName,
                Output = customCommandViewModel.Output,
                MinRank = (int)customCommandViewModel.MinRank,
                IsActive = customCommandViewModel.IsActive
            };

            dbContext.CustomCommand.Add(customCommand);
            await dbContext.SaveChangesAsync();

            botManager.GetChannel(channelName)?.ChatHandler.ReloadCommands(channelName, dbContext);

            return Ok();
        }

        /// <summary>
        /// Delete an exsiting custom command.
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="customcommandViewModel"></param>
        /// <param name="commandId"></param>
        /// <returns>Status code.</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCommand(string channelName, string commandId)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Delete))
            {
                return Unauthorized();
            }
            
            CustomCommand customCommand = await dbContext.CustomCommand.Where(t => t.ChannelName == channelName && t.CommandId == commandId).FirstOrDefaultAsync();
            
            if (customCommand == null)
            {
                return NotFound();
            }

            dbContext.CustomCommand.Remove(customCommand);
            await dbContext.SaveChangesAsync();

            botManager.GetChannel(channelName)?.ChatHandler.ReloadCommands(channelName, dbContext);

            return Ok();
        }

        /// <summary>
        /// Update a custom command.
        /// </summary>
        /// <param name="customCommandViewModel"></param>
        /// <param name="commandId"></param>
        /// <param name="channelName"></param>
        /// <returns>Status Code.</returns>
        [HttpPut]
        public async Task<IActionResult> UpdateCommand([FromBody] CustomCommandViewModel customCommandViewModel, string commandId, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }

            if (customCommandViewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            CustomCommand command = await (from p in dbContext.CustomCommand
                                           where p.CommandId == commandId && p.ChannelName == channelName
                                           select p).SingleOrDefaultAsync();

            command.Output = customCommandViewModel.Output;
            command.MinRank = (int)customCommandViewModel.MinRank;
            command.IsActive = customCommandViewModel.IsActive;
            await dbContext.SaveChangesAsync();
            
            botManager.GetChannel(channelName)?.ChatHandler.ReloadCommands(channelName, dbContext);

            return Ok();
        }

        /// <summary>
        /// Sets the IsActive flag on a command.
        /// </summary>
        /// <param name="customCommandViewModel"></param>
        /// <param name="commandId"></param>
        /// <param name="channelName"></param>
        /// <returns>Status Code.</returns>
        [HttpPut("active")]
        public async Task<IActionResult> SetIsActive([FromBody] CustomCommandViewModel customCommandViewModel, string commandId, string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel { ChannelName = channelName }, Operations.Update))
            {
                return Unauthorized();
            }

            if (customCommandViewModel == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            CustomCommand command = await (from p in dbContext.CustomCommand
                                           where p.CommandId == commandId && p.ChannelName == channelName
                                           select p).SingleOrDefaultAsync();

            command.IsActive = customCommandViewModel.IsActive;
            await dbContext.SaveChangesAsync();

            botManager.GetChannel(channelName)?.ChatHandler.ReloadCommands(channelName, dbContext);

            return Ok();
        }
    }
}
