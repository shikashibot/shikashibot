﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShikashiBot.Policies;
using ShikashiBot.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Controllers
{
    /// <summary>
    /// Controller for obtaining statistics on a channel.
    /// </summary>
    [Route("/api/channels/{channelName}/statistics/channel")]
    public class ChannelStatisticsController : Controller
    {
        private PersistenceContext dbContext;
        private IAuthorizationService authorizationService;

        public ChannelStatisticsController(PersistenceContext dbContext, IAuthorizationService authorizationService)
        {
            this.dbContext = dbContext;
            this.authorizationService = authorizationService;
        }

        /// <summary>
        /// Gets the recent statistics for a channel 10 minutes back.
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        [HttpGet("recent")]
        public async Task<IActionResult> GetStatistics(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            var statistics = dbContext.ChannelStatistics
                .Where(t => (DateTime.Now - t.Timestamp).TotalMinutes <= 10 && t.ChannelName == channelName)
                .Select(t => new ChannelStatisticsViewModel
                {
                    ViewCount = t.ViewCount,
                    BanCount = t.BanCount,
                    MessageCount = t.MessageCount,
                    TimeoutCount = t.TimeoutCount,
                    Timestamp = t.Timestamp
                });

            return Ok(statistics);
        }

        /// <summary>
        /// Gets the weekly statistics for a channel, per each day.
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns>A statistic entry for each day in the week with the sum of messages, bans and timeouts where viewers are the max value.</returns>
        [HttpGet("weekly")]
        public async Task<IActionResult> GetWeeklyStatistics(string channelName)
        {
            if (!await authorizationService.AuthorizeAsync(User, new ChannelViewModel() { ChannelName = channelName }, Operations.Create))
            {
                return Unauthorized();
            }

            var statistics = dbContext.ChannelStatistics
                .Where(t => (DateTime.Now - t.Timestamp).TotalDays <= 7 && t.ChannelName == channelName)
                .GroupBy(t => new DateTime(t.Timestamp.Year, t.Timestamp.Month, t.Timestamp.Day, 0, 0, 0))
                .Select(t => new ChannelStatisticsViewModel
                {
                    ViewCount = t.Max(x => x.ViewCount),
                    BanCount = t.Sum(x => x.BanCount),
                    MessageCount = t.Sum(x => x.MessageCount),
                    TimeoutCount = t.Sum(x => x.TimeoutCount),
                    Timestamp = t.Select(x => x.Timestamp).FirstOrDefault()
                });

            return Ok(statistics);
        }
    }
}
