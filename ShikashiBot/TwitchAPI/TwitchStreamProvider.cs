﻿using Microsoft.Extensions.Configuration;
using ShikashiBot.Services;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// Interfaces with Twitch's API to get information about a channel.
    /// </summary>
    public class TwitchStreamProvider : ITwitchStreamProvider
    {
        private IUsageStatisticsService statisticsService;
        private bool apiOnlyMode;

        public TwitchStreamProvider(IUsageStatisticsService statisticsService, IConfigurationRoot configuration)
        {
            this.statisticsService = statisticsService;
            this.apiOnlyMode = configuration["ApiOnlyMode"] == "True";
        }

        /// <summary>
        /// Gets information about a live channel.
        /// See https://github.com/justintv/Twitch-API/blob/master/v3_resources/streams.md#get-streamschannel
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>Information about the stream, null if its not live.</returns>
        public async Task<TwitchStream> GetChannelInformation(string channelName)
        {
            if (apiOnlyMode)
            {
                return null;
            }

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var request = await client.GetAsync($"https://api.twitch.tv/kraken/streams/{channelName}");
                    statisticsService.AddTwitchAPICall();

                    TwitchStreamContainer container = await request.Content.ReadAsAsync<TwitchStreamContainer>();
                    return container.Stream;
                }
            }
            catch
            {
                // TODO: Log exception
                return null;
            }
        }
    }
}
