﻿using System.Runtime.Serialization;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// Information about a Twitch account.
    /// See https://github.com/justintv/Twitch-API/blob/master/v3_resources/users.md#get-user
    /// </summary>
    [DataContract]
    public class TwitchAccount
    {
        [DataMember(Name = "display_name")]
        public string DisplayName { get; set; }

        [DataMember(Name = "_id")]
        public int Id { get; set; }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }
        
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}
