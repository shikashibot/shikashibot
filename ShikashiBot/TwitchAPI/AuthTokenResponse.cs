﻿using System.Runtime.Serialization;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// API response from Twitch with token information.
    /// </summary>
    [DataContract]
    public class AuthTokenResponse
    {
        /// <summary>
        /// OAuth access token.
        /// </summary>
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Refresh token.
        /// </summary>
        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }
        
        /// <summary>
        /// Which scopes this token is valid for.
        /// </summary>
        [DataMember(Name = "scope")]
        public string[] Scope;
    }
}
