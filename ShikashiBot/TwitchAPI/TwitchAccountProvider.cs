﻿using ShikashiBot.Services;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// Interfaces with the Twitch API to get information about users.
    /// </summary>
    public class TwitchAccountProvider : ITwitchAccountProvider
    {
        private IUsageStatisticsService statisticsService;

        public TwitchAccountProvider(IUsageStatisticsService statisticsService)
        {
            this.statisticsService = statisticsService;
        } 

        /// <summary>
        /// Gets account information from Twitch associated with the specified oAuth token.
        /// </summary>
        /// <param name="oAuthToken">oAuth token to find account information for.</param>
        /// <returns>Account information. Null if oAuth token was invalid.</returns>
        public async Task<TwitchAccount> GetTwitchAccountAsync(string oAuthToken)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", $"OAuth {oAuthToken}");
                var request = await client.GetAsync("https://api.twitch.tv/kraken/user");
                statisticsService.AddTwitchAPICall();

                return await request.Content.ReadAsAsync<TwitchAccount>();
            }
        }
    }
}
