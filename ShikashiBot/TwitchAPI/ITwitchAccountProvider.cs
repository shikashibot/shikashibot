﻿using System.Threading.Tasks;

namespace ShikashiBot.TwitchAPI
{
    public interface ITwitchAccountProvider
    {
        /// <summary>
        /// Gets Twitch account information.
        /// </summary>
        /// <param name="oAuthToken">OAuth access token.</param>
        /// <returns>Twitch account information associated with the token.</returns>
        Task<TwitchAccount> GetTwitchAccountAsync(string oAuthToken);
    }
}
