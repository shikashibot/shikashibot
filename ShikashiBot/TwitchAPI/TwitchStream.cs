﻿using System.Runtime.Serialization;

namespace ShikashiBot.TwitchAPI
{
    [DataContract]
    public class TwitchStream
    {
        /// <summary>
        /// Amount of viewers in the stream.
        /// </summary>
        [DataMember(Name = "viewers")]
        public int Viewers { get; set; }

        /// <summary>
        /// The current title of the stream.
        /// </summary>
        [DataMember(Name = "game")]
        public string StreamTitle { get; set; }
    }

    [DataContract]
    public class TwitchStreamContainer
    {
        [DataMember(Name = "stream")]
        public TwitchStream Stream { get; set; }
    }
}
