﻿using ShikashiBot.Model;
using System.Threading.Tasks;

namespace ShikashiBot.TwitchAPI
{
    public interface ITwitchAuthService
    {
        /// <summary>
        /// Checks if the credentials are valid.
        /// </summary>
        /// <param name="credentials">Credentials to validate</param>
        /// <returns>True if valid, otherwise false</returns>
        Task<bool> ValidateCredentialsAsync(TwitchCredentials credentials);

        /// <summary>
        /// Gets existing credentials.
        /// </summary>
        /// <param name="publicAccessToken">Access token being used towards this system.</param>
        /// <returns>Credentials for the access token, null if not found.</returns>
        Task<TwitchCredentials> GetCredentialsAsync(string publicAccessToken);

        /// <summary>
        /// Obtains an access token, persists and generates a new public key that is used
        /// to authenticate the user when performing API calls towards this system.
        /// </summary>
        /// <param name="token">Twitch OAuth access token</param>
        /// <returns>Access credentials</returns>
        Task<TwitchCredentials> ObtainAndStoreCredentialsAsync(string token);

        /// <summary>
        /// Invalidates a token.
        /// </summary>
        /// <param name="token">Token to invalidate</param>
        /// <returns>Async task</returns>
        Task InvalidateTokenAsync(TwitchCredentials token);
    }
}
