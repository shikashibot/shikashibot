﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ShikashiBot.Model;
using ShikashiBot.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// Class for Twitch authentication of users.
    /// </summary>
    public class TwitchAuthService : ITwitchAuthService
    {
        private PersistenceContext dbContext;
        private ITwitchAccountProvider accountProvider;
        private IUsageStatisticsService statisticsService;
        private ILogger<TwitchAuthService> logger;

        private string twitchAPIClientSecret;
        private string twitchAPIClientId;
        private string twitchAPIRedirectUri;

        public TwitchAuthService(PersistenceContext dbContext, ITwitchAccountProvider accountProvider, IConfigurationRoot configuration, IUsageStatisticsService statisticsService, ILogger<TwitchAuthService> logger)
        {
            this.dbContext = dbContext;
            this.accountProvider = accountProvider;
            this.twitchAPIClientId = configuration["TwitchAPI:ClientId"];
            this.twitchAPIClientSecret = configuration["TwitchAPI:ClientSecret"];
            this.twitchAPIRedirectUri = configuration["TwitchAPI:RedirectUri"];
            this.statisticsService = statisticsService;
            this.logger = logger;
        }
        
        /// <summary>
        /// Gets existing credentials for a user.
        /// </summary>
        /// <param name="publicAccessToken">Public keys for the credentials.</param>
        /// <returns>Credentials associated with the keys</returns>
        public async Task<TwitchCredentials> GetCredentialsAsync(string publicAccessToken)
        {
            return await dbContext.TwitchCredentials.Where(p => p.PublicAccessToken == publicAccessToken).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Obtains an OAuth access token from Twitch using the provided token, generates and persists
        /// a public key that the user can use to access this API.
        /// </summary>
        /// <param name="token">OAuth token</param>
        /// <returns>Obtained/generated credentials</returns>
        public async Task<TwitchCredentials> ObtainAndStoreCredentialsAsync(string token)
        {
            using (HttpClient client = new HttpClient())
            {
                Dictionary<string, string> authRequestProperties = new Dictionary<string, string>();

                authRequestProperties.Add("client_id", twitchAPIClientId);
                authRequestProperties.Add("client_secret", twitchAPIClientSecret);
                authRequestProperties.Add("grant_type", "authorization_code");
                authRequestProperties.Add("redirect_uri", twitchAPIRedirectUri);
                authRequestProperties.Add("code", token);

                HttpContent tokenRequestBody = new FormUrlEncodedContent(authRequestProperties);

                var request = await client.PostAsync("https://api.twitch.tv/kraken/oauth2/token", tokenRequestBody);
                statisticsService.AddTwitchAPICall();

                if (request.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    string responseContent = await request.Content.ReadAsStringAsync();
                    logger.LogWarning($"Unable to authenticate user: {responseContent}");

                    // User didn't provide a valid oauth token
                    return null;
                }

                var response = await request.Content.ReadAsAsync<AuthTokenResponse>();
                
                TwitchAccount accountInformation = await accountProvider.GetTwitchAccountAsync(response.AccessToken);

                TwitchCredentials credentialsModel = new TwitchCredentials()
                {
                    TwitchUsername = accountInformation.Name,
                    AccessToken = response.AccessToken,
                    RefreshToken = response.RefreshToken,
                    PublicAccessToken = GetUniqueKey(30)
                };

                TwitchCredentials existingToken = dbContext.TwitchCredentials.Where(t => t.TwitchUsername == accountInformation.Name).SingleOrDefault();
                if (existingToken != null)
                {
                    dbContext.Remove(existingToken);
                    await dbContext.SaveChangesAsync();
                }

                bool channelExists = await (from p in dbContext.Channel
                                            where p.ChannelName == accountInformation.Name
                                            select p).AnyAsync();
                if (!channelExists)
                {
                    Model.Channel channel = new Model.Channel
                    {
                        ChannelName = accountInformation.Name,
                        Visiting = false
                    };

                    dbContext.Channel.Add(channel);
                }

                dbContext.TwitchCredentials.Add(credentialsModel);

                await dbContext.SaveChangesAsync();

                return credentialsModel;
            }
        }

        private string GetUniqueKey(int maxSize)
        {
            // From https://stackoverflow.com/a/1344255/1924825
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }

            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            return result.ToString();
        }

        /// <summary>
        /// Checks if the credentials are valid or not.
        /// </summary>
        /// <param name="credentials">Credentials to validate.</param>
        /// <returns>True if valid, otherwise false.</returns>
        public Task<bool> ValidateCredentialsAsync(TwitchCredentials credentials)
        {
            throw new System.NotImplementedException();
            //var t = await accountProvider.GetTwitchAccountAsync(credentials.AccessToken);
            /* using (HttpClient client = new HttpClient())
             {
                 statisticsService.AddTwitchAPICall();
                 client.DefaultRequestHeaders.Add("Authorization", $"OAuth {credentials.AccessToken}");
                 var request = client.GetAsync("https://api.twitch.tv/kraken");
                 return null;
             }*/
        }

        /// <summary>
        /// Removes an existing token from the database.
        /// </summary>
        /// <param name="publicAccessToken">Token to remove.</param>
        public async Task InvalidateTokenAsync(TwitchCredentials credentials)
        {
            dbContext.Attach(credentials);
            dbContext.TwitchCredentials.Remove(credentials);
            await dbContext.SaveChangesAsync();
        }
    }
}
