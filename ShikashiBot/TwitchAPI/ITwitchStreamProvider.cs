﻿using System.Threading.Tasks;

namespace ShikashiBot.TwitchAPI
{
    /// <summary>
    /// Provides information related to a stream.
    /// </summary>
    public interface ITwitchStreamProvider
    {
        /// <summary>
        /// Obtains information about a live channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>Null if the stream is offline, otherwise an instance holding information about the channel.</returns>
        Task<TwitchStream> GetChannelInformation(string channelName);
    }
}
