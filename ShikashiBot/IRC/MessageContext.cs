﻿using IrcDotNet;

namespace ShikashiBot
{
    /// <summary>
    /// Holds information about where a message came from.
    /// </summary>
    public class MessageContext : IMessageContext
    {
        public IrcChannel channel;

        /// <summary>
        /// Gets the channel which the message was posted in.
        /// </summary>
        /// <returns>Current channel.</returns>
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel where the message was posted in.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// Creates a new message context.
        /// </summary>
        /// <param name="channel">Current IRC channel where the message was posted in.</param>
        /// <param name="currentChannel">The Twitch channel where the message was posted in.</param>
        public MessageContext(IrcChannel channel)
        {
            this.channel = channel;
        }

        /// <summary>
        /// Sends a message in the chat.
        /// </summary>
        /// <param name="message">Message.</param>
        public void SendMessage(string message)
        {
            Channel.BroadcastMessage(message);
        }

        /// <summary>
        /// Sends a command to the chat.
        /// </summary>
        /// <param name="message"></param>
        public void SendCommand(string message)
        {
            Channel.SendCommand(message);
        }
    }
}
