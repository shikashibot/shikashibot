﻿using IrcDotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;

namespace ShikashiBot.IRC
{
    public class IrcRelay : IIrcRelay
    {
        private ILogger logger;
        private const string DefaultTwtichIrcHostname = "irc.twitch.tv";

        private TwitchIrcClient client;
        public event OnMessageReceivedDelegate MessageReceived;
        public event OnChannelJoined ChannelJoined;
        public event OnChannelModesChanged ModesChanged;

        private string hostname = DefaultTwtichIrcHostname;

        private string botUsername;
        private string botOAuthToken;

        /// <summary>
        /// Creates a new IRC relay to communicate with IRC.
        /// </summary>
        /// <param name="channelProvider">Channel provider that provides access to channels.</param>
        /// <param name="hostname">Hostname for the IRC server.</param>
        public IrcRelay(ILoggerFactory loggerFactory, IConfigurationRoot config)
        {
            this.logger = loggerFactory.CreateLogger<IrcRelay>();
            this.hostname = DefaultTwtichIrcHostname;
            this.botUsername = config["BotUsername"];
            this.botOAuthToken = config["IRCOauthToken"];
            this.client = new TwitchIrcClient();
        }

        /// <summary>
        /// Starts connecting to IRC.
        /// </summary>
        public void Connect()
        {
            logger.LogInformation(string.Format("Attempting to establish IRC connection to {0}", hostname));

            client.FloodPreventer = new IrcStandardFloodPreventer(4, 2000);
            client.Disconnected += IrcClient_Disconnected;
            client.Registered += IrcClient_Registered;

            // Wait until connection has succeeded or timed out.
            using (var registeredEvent = new ManualResetEventSlim(false))
            {
                using (var connectedEvent = new ManualResetEventSlim(false))
                {
                    client.Connected += (sender2, e2) => connectedEvent.Set();
                    client.Registered += (sender2, e2) => registeredEvent.Set();
                    client.Connect(hostname, false,
                        new IrcUserRegistrationInfo()
                        {
                            NickName = botUsername,
                            Password = botOAuthToken,
                            UserName = botUsername
                        });

                    if (!connectedEvent.Wait(10000))
                    {
                        logger.LogError("Connection timed out");
                        return;
                        // TODO: Throw exception
                    }
                }

                logger.LogInformation("Waiting for registration to complete");
                if (!registeredEvent.Wait(10000))
                {
                    logger.LogError("Could not register");
                    return;
                    // TODO: Throw exception
                }
            }

            client.LocalUser.NoticeReceived += IrcClient_LocalUser_NoticeReceived;
            client.LocalUser.MessageReceived += IrcClient_LocalUser_MessageReceived;
            client.LocalUser.JoinedChannel += IrcClient_LocalUser_JoinedChannel;
            client.LocalUser.LeftChannel += IrcClient_LocalUser_LeftChannel;
            client.RawMessageReceived += Client_RawMessageReceived;

            client.SendRawMessage("CAP REQ :twitch.tv/membership");
            //  client.SendRawMessage("CAP REQ :twitch.tv/tags");
            //  client.SendRawMessage("CAP REQ :twitch.tv/commands");

            logger.LogInformation("Now registered as '{0}'", botUsername);
        }

        private void Client_RawMessageReceived(object sender, IrcRawMessageEventArgs e)
        {
          //  logger.DebugFormat("Raw message received: {0}", e.RawContent);
        }

        /// <summary>
        /// Joins a channel.
        /// </summary>
        /// <param name="channelName">Name of the Twitch channel to join without # as prefix.</param>
        public void Join(string channelName)
        {
            logger.LogDebug("Joining channel {0}", channelName);
            string f = string.Format("join #{0}", channelName);
            client.SendRawMessage(f);
        }

        /// <summary>
        /// Leaves a Twitch channel.
        /// </summary>
        /// <param name="channelName">Name of the Twitch channel without # as prefix.</param>
        public void Leave(string channelName)
        {
            client.Channels.Leave(new string[] { string.Format("#{0}", channelName) });
        }

        private void IrcClient_Registered(object sender, EventArgs e)
        {
            logger.LogInformation("Client registered");
            //throw new NotImplementedException(); ??
            var client = (IrcClient)sender;
        }
        
        private void IrcClient_LocalUser_LeftChannel(object sender, IrcChannelEventArgs e)
        {
            var localUser = (IrcLocalUser)sender;

            e.Channel.UserJoined -= IrcClient_Channel_UserJoined;
            e.Channel.UserLeft -= IrcClient_Channel_UserLeft;
            e.Channel.MessageReceived -= IrcClient_Channel_MessageReceived;
            e.Channel.NoticeReceived -= IrcClient_Channel_NoticeReceived;

            logger.LogDebug(string.Format("You left the channel {0}.", e.Channel.Name));
        }

        private void IrcClient_LocalUser_JoinedChannel(object sender, IrcChannelEventArgs e)
        {
            var localUser = (IrcLocalUser)sender;

            e.Channel.UserJoined += IrcClient_Channel_UserJoined;
            e.Channel.UserLeft += IrcClient_Channel_UserLeft;
            e.Channel.MessageReceived += IrcClient_Channel_MessageReceived;
            e.Channel.NoticeReceived += IrcClient_Channel_NoticeReceived;
            e.Channel.ModesChanged += Channel_ModesChanged;

            logger.LogDebug(string.Format("You joined the channel {0}.", e.Channel.Name));

            ChannelJoined(e.Channel.Name.Substring(1), e.Channel);
        }

        private void Channel_ModesChanged(object sender, IrcUserEventArgs e)
        {
            var channelOrigin = (IrcChannel)sender;
            logger.LogDebug(">>>> Channel OP list was changed, updating " + channelOrigin.Name);

            ModesChanged(channelOrigin.Name.Substring(1));
        }

        private void IrcClient_Channel_NoticeReceived(object sender, IrcMessageEventArgs e)
        {
            var channel = (IrcChannel)sender;
            logger.LogDebug(string.Format("[{0}] Notice: {1}.", channel.Name, e.Text));
        }

        private void IrcClient_Channel_MessageReceived(object sender, IrcMessageEventArgs e)
        {
            var channel = (IrcChannel)sender;
            if (e.Source is IrcUser)
            {
                // Read message.
                logger.LogDebug(string.Format("[{0}]({1}): {2}.", channel.Name, e.Source.Name, e.Text));
            }
            else
            {
                logger.LogDebug(string.Format("[{0}]({1}) Message: {2}.", channel.Name, e.Source.Name, e.Text));
            }

            // Relay message to chat message handler
            this.MessageReceived(channel.Name.Substring(1), e.Source.Name, e.Text, new MessageContext(channel));
        }

        private void IrcClient_Channel_UserLeft(object sender, IrcChannelUserEventArgs e)
        {
            var channel = (IrcChannel)sender;
            logger.LogDebug(string.Format("[{0}] User {1} left the channel.", channel.Name, e.ChannelUser.User.NickName));
        }

        private void IrcClient_Channel_UserJoined(object sender, IrcChannelUserEventArgs e)
        {
            var channel = (IrcChannel)sender;
            logger.LogDebug(string.Format("[{0}] User {1} joined the channel.", channel.Name, e.ChannelUser.User.NickName));
        }

        private void IrcClient_LocalUser_MessageReceived(object sender, IrcMessageEventArgs e)
        {
            var localUser = (IrcLocalUser)sender;

            if (e.Source is IrcUser)
            {
                // Read message.
                logger.LogDebug(string.Format("({0}): {1}.", e.Source.Name, e.Text));
            }
            else
            {
                logger.LogDebug(string.Format("({0}) Message: {1}.", e.Source.Name, e.Text));
            }

        }

        private void IrcClient_LocalUser_NoticeReceived(object sender, IrcMessageEventArgs e)
        {
            var localUser = (IrcLocalUser)sender;
            logger.LogDebug(string.Format("Notice: {0}.", e.Text));
        }
        

        private void IrcClient_Disconnected(object sender, EventArgs e)
        {
            logger.LogDebug("Lost connection to IRC, attempting to reconnect");
            Connect();
        }
    }
}
