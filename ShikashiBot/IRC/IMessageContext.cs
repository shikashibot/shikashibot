﻿namespace ShikashiBot
{
    /// <summary>
    /// A message context object holds functions that can be used to interact with where the message came from.
    /// </summary>
    public interface IMessageContext
    {
        /// <summary>
        /// Sends a message to the channel which the message originates from.
        /// </summary>
        /// <param name="message">Message to send.</param>
        void SendMessage(string message);

        /// <summary>
        /// Sends a command to the channel where the message originates from.
        /// </summary>
        /// <param name="message">The command together with /</param>
        void SendCommand(string message);

        /// <summary>
        /// The channel where the message was sent from.
        /// </summary>
        /// <returns></returns>
        Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel.
        /// </summary>
        string ChannelName { get; set; }
    }
}
