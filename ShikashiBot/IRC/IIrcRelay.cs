﻿using IrcDotNet;

namespace ShikashiBot.IRC
{
    public interface IIrcRelay
    {
        /// <summary>
        /// Sets up the IRC connection.
        /// </summary>
        void Connect();

        /// <summary>
        /// Joins a specific channel.
        /// </summary>
        /// <param name="channelName">Channel to join.</param>
        void Join(string channelName);

        /// <summary>
        /// Leaves a specific channel.
        /// </summary>
        /// <param name="channelName">Channel to leave.</param>
        void Leave(string channelName);

        /// <summary>
        /// Events fired at when messages are received.
        /// </summary>
        event OnMessageReceivedDelegate MessageReceived;

        /// <summary>
        /// Events fired once a channel has been joined.
        /// </summary>
        event OnChannelJoined ChannelJoined;

        /// <summary>
        /// Events fired once the channel modes have been changed.
        /// </summary>
        event OnChannelModesChanged ModesChanged;
    }


    /// <summary>
    /// Delegate function when messages are received.
    /// </summary>
    /// <param name="channelName">Origin channel name.</param>
    /// <param name="senderUsername">User who sent the message.</param>
    /// <param name="message">Message content.</param>
    /// <param name="messageContext">Message context.</param>
    public delegate void OnMessageReceivedDelegate(string channelName, string senderUsername, string message, MessageContext messageContext);

    public delegate void OnChannelJoined(string channelName, IrcChannel channel);

    public delegate void OnChannelModesChanged(string channelName);

}
