﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using ShikashiBot.AuthenticatorMiddleware;
using ShikashiBot.IRC;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Policies;
using ShikashiBot.Services;
using ShikashiBot.TwitchAPI;
using System;

namespace ShikashiBot
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            services.AddDbContext<PersistenceContext>(options => options.UseNpgsql(Configuration["Database:ConnectionString"]));

            services.AddSingleton<IUsageStatisticsService>(new UsageStatisticsService());
            services.AddSingleton<IShikashiBotManager, ShikashiBotManager>();
            services.AddSingleton<IEmoticonProvider, EmoticonLoader>();
            services.AddSingleton<IPersistenceProvider, PersistenceProvider>();
            services.AddSingleton<ITwitchStreamProvider, TwitchStreamProvider>();
            services.AddTransient<ITwitchAuthService, TwitchAuthService>();
            services.AddTransient<ITwitchAccountProvider, TwitchAccountProvider>();
            services.AddTransient<IAuthorizationHandler, ChannelAuthorizationHandler>();
            services.AddSingleton<IIrcRelay, IrcRelay>();
            services.AddSingleton(Configuration);

            services.AddCors(o => o.AddPolicy("AllowLocalhost", builder =>
            {
                builder.WithOrigins("http://localhost", "http://localhost:5555", "https://bot.shikashi.me")
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .SetPreflightMaxAge(TimeSpan.FromSeconds(600));
            }));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IShikashiBotManager botManager, IEmoticonProvider emoteProvider)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors("AllowLocalhost");
            app.UseStaticFiles();
            app.UseTokenAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            if (Configuration["ApiOnlyMode"] != "True")
            {
                emoteProvider.LoadEmotesAsync().Wait();
            }

            botManager.Initialize();
        }
    }
}
