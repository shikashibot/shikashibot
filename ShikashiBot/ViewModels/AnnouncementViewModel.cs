﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    /// <summary>
    /// View model for announcements that are updated in the system.
    /// </summary>
    public class AnnouncementViewModel
    {
        /// <summary>
        /// Message that should be posted in the chat for the announcement.
        /// </summary>
        [StringLength(500)]
        [Required]
        public string Message { get; set; }

        /// <summary>
        /// How frequent the announcement should be posted.
        /// </summary>
        [Range(30, int.MaxValue)]
        public int SecondsInterval { get; set; }

        /// <summary>
        /// Unique ID of the announcement.
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Whether the announcement is active or not. If it is inactive, it is not posted in the chat.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }
    }
}
