﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels.Auth
{
    public class LoginViewModel
    {
        [RegularExpression(@"^[a-zA-Z0-9]*$")]
        public string Token { get; set; }

        public string ChannelName { get; set; }
    }
}
