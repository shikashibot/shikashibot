﻿using ShikashiBot.Model.Filters;
using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    /// <summary>
    /// View model for filters that are added/sent out of the system.
    /// </summary>
    public class GeneralFilterViewModel
    {
        /// <summary>
        /// Specific ID for the filter
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Action to perform once the filter is being violated.
        /// </summary>
        [Required]
        public ChatMessageViolationActionType RemoveType { get; set; }

        /// <summary>
        /// How long a user should be timed out (Depends on RemoveType).
        /// </summary>
        [Range(0, int.MaxValue)]
        public int TimeoutSeconds { get; set; }

        /// <summary>
        /// What to post in the chat if the filter is violated.
        /// </summary>
        [MaxLength(500)]
        public string OutputMessage { get; set; }

        /// <summary>
        /// Maximum limit for this filter (either max message length, emote limit, etc.)
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Limit { get; set; }

        /// <summary>
        /// Whether the filter is enabled or not.
        /// </summary>
        [Required]
        public bool Enabled { get; set; } = true;

        /// <summary>
        /// Description of the filter.
        /// </summary>
        public string Description { get; set; }
    }
}
