﻿using ShikashiBot.Permissions;
using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    public class CustomCommandViewModel
    {
        /// <summary>
        /// The output message to be posted when the message is invoked.
        /// </summary>
        [MaxLength(500)]
        public string Output { get; set; }

        /// <summary>
        /// Command prefix.
        /// </summary>
        [RegularExpression(@"^[^ ]*$")]
        public string CommandId { get; set; }

        /// <summary>
        /// Min rank for a user to invoke the command.
        /// </summary>
        [Required]
        public UserType MinRank { get; set; }

        /// <summary>
        /// Sets whether the command is active or not.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }
    }
}
