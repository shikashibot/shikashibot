﻿using System;

namespace ShikashiBot.ViewModels
{
    /// <summary>
    /// View model for the channel statistics.
    /// </summary>
    public class ChannelStatisticsViewModel
    {
        /// <summary>
        /// When this statistics is for.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// How many messages there was posted.
        /// </summary>
        public int MessageCount { get; set; }

        /// <summary>
        /// How many bans were issued.
        /// </summary>
        public int BanCount { get; set; }

        /// <summary>
        /// How many timeouts were issued.
        /// </summary>
        public int TimeoutCount { get; set; }

        /// <summary>
        /// How many viewers was in the stream.
        /// </summary>
        public int ViewCount { get; set; }
    }
}
