﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    public class ChannelViewModel
    {
        /// <summary>
        /// Name of the channel.
        /// </summary>
        [RegularExpression(@"^[a-zA-Z0-9_]*$")]
        [MaxLength(50)]
        [MinLength(1)]
        public string ChannelName { get; set; }

        /// <summary>
        /// If the bot should be in the chat for the channel and moderate or not.
        /// </summary>
        public bool Visiting { get; set; }
    }
}
