﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    public class BannedWordViewModel
    {
        /// <summary>
        /// The banned word.
        /// </summary>
        [MaxLength(200)]
        [MinLength(1)]
        [RegularExpression(@"^[^ ]*$")]
        public string Word { get; set; }
    }
}