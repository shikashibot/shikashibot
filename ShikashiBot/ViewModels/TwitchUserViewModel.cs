﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    public class TwitchUserViewModel
    {
        /// <summary>
        /// Name of a user.
        /// </summary>
        [RegularExpression(@"^[a-zA-Z0-9]*$")]
        [MaxLength(50)]
        [MinLength(1)]
        public string Username { get; set; }
    }
}
