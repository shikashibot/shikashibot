﻿
namespace ShikashiBot.ViewModels
{
    public class GeneralStatisticsViewModel
    {
        public string Item { get; set; }
        public double Score { get; set; }
    }
}
