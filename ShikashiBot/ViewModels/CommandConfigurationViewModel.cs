﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    /// <summary>
    /// View model for configuration data for commands.
    /// </summary>
    public class CommandConfigurationViewModel
    {
        /// <summary>
        /// Class name of the command.
        /// </summary>
        [RegularExpression(@"^[a-zA-Z]*$")]
        public string CommandId { get; set; }

        /// <summary>
        /// Prefix for the command which the user has to type in the chat.
        /// </summary>
        [RegularExpression(@"^[^ ]*$")]
        public string Prefix { get; set; }

        /// <summary>
        /// Minimum rank for users to execute the command in chat.
        /// </summary>
        public int? MinRank { get; set; }

        /// <summary>
        /// Flag indicating whether the command is enabled or not.
        /// </summary>
        public bool Enabled { get; set; } = true;

        /// <summary>
        /// Description of the command.
        /// </summary>
        public string Description { get; set; }
    }
}
