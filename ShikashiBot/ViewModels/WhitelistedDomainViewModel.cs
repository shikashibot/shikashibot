﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.ViewModels
{
    /// <summary>
    /// View model for whitelisted domains.
    /// </summary>
    public class WhitelistedDomainViewModel
    {
        /// <summary>
        /// Domain that is whitelisted.
        /// </summary>
        [RegularExpression(@"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$")]
        public string Domain { get; set; }
    }
}
