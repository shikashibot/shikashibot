﻿namespace ShikashiBot
{
    /// <summary>
    /// An object capable of broadcasting messages to a chat channel.
    /// </summary>
    interface IBroadcaster
    {
        /// <summary>
        /// Sends a message to the channel.
        /// </summary>
        /// <param name="message">Message to send.</param>
        void BroadcastMessage(string message);
    }
}
