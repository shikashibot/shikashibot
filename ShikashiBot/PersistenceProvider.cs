﻿
using Microsoft.EntityFrameworkCore;

namespace ShikashiBot
{
    /// <summary>
    /// Default persistence provider.
    /// </summary>
    public class PersistenceProvider : IPersistenceProvider
    {
        private DbContextOptions<PersistenceContext> dbOptions;

        public PersistenceProvider(DbContextOptions<PersistenceContext> dbOptions)
        {
            this.dbOptions = dbOptions;
        }

        /// <summary>
        /// Creates a new persistence context.
        /// </summary>
        /// <returns>A new persistence context.</returns>
        public PersistenceContext GetPersistenceContext()
        {
            return new PersistenceContext(dbOptions);
        }
    }
}
