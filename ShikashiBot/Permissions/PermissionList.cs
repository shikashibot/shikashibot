﻿using ShikashiBot.Permissions;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ShikashiBot
{
    /// <summary>
    /// Contains information about the viewers in the channel.
    /// </summary>
    [DataContract]
    class PermissionListSet
    {
        [DataMember(Name = "chatter_count")]
        public int Viewers { get; set; }

        [DataMember(Name = "chatters")]
        public PermissionList PermissionList { get; set; }
    }

    /// <summary>
    /// List of the permissions.
    /// </summary>
    [DataContract]
    class PermissionList
    {
        [DataMember(Name = "moderators")]
        public List<string> Moderators { get; set; }

        [DataMember(Name = "staff")]
        public List<string> Staff { get; set; }

        [DataMember(Name = "admins")]
        public List<string> Admins { get; set; }

        [DataMember(Name = "global_mods")]
        public List<string> GlobalMods { get; set; }

        public Dictionary<UserType, List<string>> ToList(string channelName)
        {
            Dictionary<UserType, List<string>> userList = new Dictionary<UserType, List<string>>();
            userList.Add(UserType.Moderator, Moderators);
            userList.Add(UserType.GlobalMod, GlobalMods);
            userList.Add(UserType.Broadcaster, new List<string>() { channelName });
            userList.Add(UserType.Admin, Admins);
            userList.Add(UserType.Staff, Staff);

            return userList;
        }
    }
}
