﻿namespace ShikashiBot.Permissions
{
    /// <summary>
    /// User types in the stream.
    /// </summary>
    public enum UserType : int
    {
        Developer = 6,
        Staff = 5,
        Admin = 4,
        Broadcaster = 3,
        GlobalMod = 2,
        Moderator = 1,
        Viewer = 0
    }
}
