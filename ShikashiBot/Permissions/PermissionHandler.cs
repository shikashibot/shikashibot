﻿using Microsoft.Extensions.Logging;
using ShikashiBot.Permissions;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShikashiBot
{
    /// <summary>
    /// Manages who has what permission within a channel.
    /// </summary>
    class PermissionHandler
    {
        private ILogger logger;
        private string channelName;

        private PermissionListSet permissions;

        private DateTime lastUpdate;
        private object syncRoot;
        private bool isLoadingPremissions;
        private string permissionApiEndPoint;
        private bool apiOnlyMode;

        private const string DefaultPermissionApiEndPoint = "https://tmi.twitch.tv/group/user/{0}/chatters";

        /// <summary>
        /// Creates a new permission handler.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="permissionApiEndPoint">API endpoint to get the permission list.</param>
        public PermissionHandler(string channelName, ILoggerFactory loggerFactory, bool apiOnlyMode, string permissionApiEndPoint = DefaultPermissionApiEndPoint)
        {
            this.logger = loggerFactory.CreateLogger<PermissionHandler>();
            this.channelName = channelName;
            this.syncRoot = new object();
            this.permissionApiEndPoint = permissionApiEndPoint;
            this.apiOnlyMode = apiOnlyMode;
        }

        /// <summary>
        /// Loads the permission list asynchronously.
        /// </summary>
        /// <returns></returns>
        internal async Task LoadPermissions()
        {
            try
            {
                if (apiOnlyMode)
                {
                    logger.LogWarning($"Skipped permissions for channel {channelName} due to API only-mode");
                    return;
                }

                lock (syncRoot)
                {
                    if (isLoadingPremissions)
                        return;

                    TimeSpan sinceLastLoad = DateTime.Now - lastUpdate;
                    if (sinceLastLoad.TotalSeconds < 10)
                        return;

                    isLoadingPremissions = true;
                }

                string url = string.Format(permissionApiEndPoint, channelName);
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync(url);

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        lock (syncRoot)
                        {
                            isLoadingPremissions = false;
                        }


                        logger.LogError(string.Format("Failed loading permissions for channel {0}: {1}", this.channelName, response.StatusCode));
                        return;
                    }

                    this.permissions = await response.Content.ReadAsAsync<PermissionListSet>();
                }

                lock (syncRoot)
                {
                    isLoadingPremissions = false;
                    lastUpdate = DateTime.Now;
                }

                logger.LogDebug("Loaded permissions for " + channelName);
            }
            catch (Exception e)
            {
                logger.LogCritical("Failed loading permission", e);
            }
        }

        /// <summary>
        /// Gets user type of a user in the channel. If the perm list is not loaded, the default user type is returned (viewer). 
        /// </summary>
        /// <param name="username">Username of the person to get the user type for</param>
        /// <returns>The user type for the user.</returns>
        internal UserType GetPermission(string username)
        {
            if (permissions != null && permissions.PermissionList != null)
            {
                foreach (var userCategory in permissions?.PermissionList?.ToList(channelName))
                {
                    if (userCategory.Value.Contains(username))
                        return userCategory.Key;
                }
            }

            return UserType.Viewer;
        }
    }
}
