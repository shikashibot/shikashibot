﻿using System;

namespace ShikashiBot
{
    /// <summary>
    /// Utillity class.
    /// </summary>
    class Utillity
    {
        /// <summary>
        /// Attempts to parse an URL.
        /// </summary>
        /// <param name="url">URL to parse.</param>
        /// <returns>The parsed URL, otherwise null.</returns>
        internal static Uri ParseUrl(string url)
        {
            Uri parsedUrl = null;
            if (url.Contains(".") && !url.Contains("https://") && !url.Contains("http://"))
            {
                Uri.TryCreate("http://" + url, UriKind.Absolute, out parsedUrl);
            }
            else
            {
                Uri.TryCreate(url, UriKind.Absolute, out parsedUrl);
            }

            return parsedUrl;
        }
    }
}
