﻿using ShikashiBot.Model;
using ShikashiBot.TwitchAPI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ShikashiBot
{
    /// <summary>
    /// Manages the storing of statistics related to a channel on a specified interval.
    /// </summary>
    public class ChannelStatisticsHandler
    {
        private IPersistenceProvider persistenceProvider;
        private Timer timer;
        private Channel channel;
        private const int COUNT_PERIOD = 1000 * 60 * 1;
        private Stack<ChannelStatistics> statistics;
        private ChannelStatistics currentStatistics;
        private object syncRoot;
        private ITwitchStreamProvider streamProvider;

        /// <summary>
        /// Creates a new statistic handler.
        /// </summary>
        /// <param name="persistenceProvider"></param>
        /// <param name="channel"></param>
        /// <param name="streamProvider"></param>
        public ChannelStatisticsHandler(IPersistenceProvider persistenceProvider, Channel channel, ITwitchStreamProvider streamProvider)
        {
            this.persistenceProvider = persistenceProvider;
            this.channel = channel;
            this.statistics = new Stack<ChannelStatistics>();
            this.timer = new Timer(StoreStatistics, null, COUNT_PERIOD, COUNT_PERIOD);
            this.syncRoot = new object();
            this.streamProvider = streamProvider;
            ResetStatistics();
        }

        private async void StoreStatistics(object o)
        {
            TwitchStream stream = await streamProvider.GetChannelInformation(channel.ChannelName);

            if (stream != null)
            {
                currentStatistics.ViewCount = stream.Viewers;
            }

            ChannelStatistics oldStatistics;

            lock (syncRoot)
            {
                statistics.Push(currentStatistics);
                oldStatistics = currentStatistics;
                ResetStatistics();
            }

            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();
            dbContext.ChannelStatistics.Add(oldStatistics);
            await dbContext.SaveChangesAsync();
        }

        private void ResetStatistics()
        {
            currentStatistics = new ChannelStatistics
            {
                ChannelName = channel.ChannelName,
                Timestamp = DateTime.Now
            };
        }

        /// <summary>
        /// Indicates that a message has been added and increments the counter for posted messages.
        /// </summary>
        internal void AddMessage()
        {
            lock (syncRoot)
            {
                currentStatistics.MessageCount++;
            }
        }

        /// <summary>
        /// Indicates that a ban has been issued and increases the counter for issued bans.
        /// </summary>
        internal void AddBan()
        {
            lock (syncRoot)
            {
                currentStatistics.BanCount++;
            }
        }

        /// <summary>
        /// Indicates that a timeout has been issued and increases the counter for issued timeouts.
        /// </summary>
        internal void AddTimeout()
        {
            lock (syncRoot)
            {
                currentStatistics.TimeoutCount++;
            }
        }

        /// <summary>
        /// Returns the in-memory statistics for the channel.
        /// </summary>
        /// <returns>Statistics for the channel</returns>
        public ChannelStatistics[] GetStatistics()
        {
            return statistics.ToArray();
        }

        /// <summary>
        /// Stops the gathering of statistics.
        /// </summary>
        public void Stop()
        {
            this.timer.Dispose();
        }
    }
}
