﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ShikashiBot.Model;
using ShikashiBot.TwitchAPI;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShikashiBot.AuthenticatorMiddleware
{
    /// <summary>
    /// Middleware that intercepts calls and reads a bearer token and authenticates the user based on this token.
    /// </summary>
    public class TokenAuthenticatorMiddleware
    {
        public const string BearerIdentifier = "Bearer";
        private RequestDelegate next;

        /// <summary>
        /// Creates a new authenticator middleware.
        /// </summary>
        /// <param name="next">Next middleware</param>
        /// <param name="persistenceProvider">Persistence provider</param>
        public TokenAuthenticatorMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Invokes the middleware, gets the authorization bearer token and tries to find an existing token in the database
        /// for then to set the ClaimsPrincipal for this request.
        /// </summary>
        /// <param name="context">Context</param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, ITwitchAuthService authService)
        {
            string authorizationHeader = context.Request.Headers["Authorization"];

            if (!string.IsNullOrEmpty(authorizationHeader) && authorizationHeader.StartsWith(BearerIdentifier + " "))
            {
                string authorizationKey = authorizationHeader.Substring(BearerIdentifier.Length + 1);
                TwitchCredentials token = await authService.GetCredentialsAsync(authorizationKey);

                context.User = new ClaimsPrincipal(new ShikashiBotAPIUser(token));
            }

            await next.Invoke(context);
        }
    }
}
