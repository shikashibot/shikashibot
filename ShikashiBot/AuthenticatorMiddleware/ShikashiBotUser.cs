﻿using ShikashiBot.Model;
using System.Security.Principal;

namespace ShikashiBot.AuthenticatorMiddleware
{
    /// <summary>
    /// Declares an identify for an authorized API user such that ASP.NET can understand the user.
    /// </summary>
    public class ShikashiBotAPIUser : IIdentity
    {
        private TwitchCredentials credentials;

        /// <summary>
        /// Specifies that this user is authenticated using a bearer token.
        /// </summary>
        public string AuthenticationType
        {
            get
            {
                return TokenAuthenticatorMiddleware.BearerIdentifier;
            }
        }

        /// <summary>
        /// Indicates whether the user is authenticated or not.
        /// </summary>
        public bool IsAuthenticated
        {
            get
            {
                return credentials != null;
            }
        }

        /// <summary>
        /// Name of the user that is authenticated.
        /// </summary>
        public string Name
        {
            get
            {
                return credentials?.TwitchUsername;
            }
        }

        /// <summary>
        /// Creates a new user using existing credentials.
        /// </summary>
        /// <param name="credentials">Credentials for the user</param>
        public ShikashiBotAPIUser(TwitchCredentials credentials)
        {
            this.credentials = credentials;
        }
    }
}
