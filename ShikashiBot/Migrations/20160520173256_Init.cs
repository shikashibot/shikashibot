﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShikashiBot.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Channel",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    Visiting = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Channel", x => x.ChannelName);
                });

            migrationBuilder.CreateTable(
                name: "DefaultCommandConfiguration",
                columns: table => new
                {
                    CommandId = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MinRank = table.Column<int>(nullable: false),
                    Prefix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultCommandConfiguration", x => x.CommandId);
                });

            migrationBuilder.CreateTable(
                name: "FilterDescription",
                columns: table => new
                {
                    FilterId = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilterDescription", x => x.FilterId);
                });

            migrationBuilder.CreateTable(
                name: "TwitchCredentials",
                columns: table => new
                {
                    TwitchUsername = table.Column<string>(nullable: false),
                    AccessToken = table.Column<string>(nullable: true),
                    PublicAccessToken = table.Column<string>(nullable: false),
                    RefreshToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TwitchCredentials", x => x.TwitchUsername);
                    table.UniqueConstraint("AK_TwitchCredentials_PublicAccessToken", x => x.PublicAccessToken);
                });

            migrationBuilder.CreateTable(
                name: "Announcement",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:Serial", true),
                    IsActive = table.Column<bool>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    SecondsInterval = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Announcement", x => new { x.ChannelName, x.Id });
                    table.ForeignKey(
                        name: "FK_Announcement_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BannedWord",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    Word = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BannedWord", x => new { x.ChannelName, x.Word });
                    table.ForeignKey(
                        name: "FK_BannedWord_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChannelStatistics",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    BanCount = table.Column<int>(nullable: false),
                    MessageCount = table.Column<int>(nullable: false),
                    TimeoutCount = table.Column<int>(nullable: false),
                    ViewCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChannelStatistics", x => new { x.ChannelName, x.Timestamp });
                    table.ForeignKey(
                        name: "FK_ChannelStatistics_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BotCommandConfiguration",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    CommandId = table.Column<string>(nullable: false),
                    MinRank = table.Column<int>(nullable: false),
                    Prefix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotCommandConfiguration", x => new { x.ChannelName, x.CommandId });
                    table.ForeignKey(
                        name: "FK_BotCommandConfiguration_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChannelCommandConfiguration",
                columns: table => new
                {
                    CommandId = table.Column<string>(nullable: false),
                    ChannelName = table.Column<string>(nullable: true),
                    Enabled = table.Column<bool>(nullable: false),
                    MinRank = table.Column<int>(nullable: false),
                    Prefix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChannelCommandConfiguration", x => x.CommandId);
                    table.ForeignKey(
                        name: "FK_ChannelCommandConfiguration_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomCommand",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    CommandId = table.Column<string>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    MinRank = table.Column<int>(nullable: false),
                    Output = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomCommand", x => new { x.ChannelName, x.CommandId });
                    table.ForeignKey(
                        name: "FK_CustomCommand_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FilterConfiguration",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:Serial", true),
                    ChannelName = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    OutputMessage = table.Column<string>(nullable: true),
                    RemoveType = table.Column<int>(nullable: false),
                    TimeoutSeconds = table.Column<int>(nullable: false),
                    CapsLimit = table.Column<int>(nullable: true),
                    EmoteLimit = table.Column<int>(nullable: true),
                    MaxLength = table.Column<int>(nullable: true),
                    WordLimit = table.Column<int>(nullable: true),
                    MaxCount = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilterConfiguration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FilterConfiguration_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Regular",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    TwitchUsername = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regular", x => new { x.ChannelName, x.TwitchUsername });
                    table.ForeignKey(
                        name: "FK_Regular_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WhitelistedDomain",
                columns: table => new
                {
                    ChannelName = table.Column<string>(nullable: false),
                    Domain = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WhitelistedDomain", x => new { x.ChannelName, x.Domain });
                    table.ForeignKey(
                        name: "FK_WhitelistedDomain_Channel_ChannelName",
                        column: x => x.ChannelName,
                        principalTable: "Channel",
                        principalColumn: "ChannelName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Announcement_ChannelName",
                table: "Announcement",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_BannedWord_ChannelName",
                table: "BannedWord",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_ChannelStatistics_ChannelName",
                table: "ChannelStatistics",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_BotCommandConfiguration_ChannelName",
                table: "BotCommandConfiguration",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_ChannelCommandConfiguration_ChannelName",
                table: "ChannelCommandConfiguration",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_CustomCommand_ChannelName",
                table: "CustomCommand",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_FilterConfiguration_ChannelName",
                table: "FilterConfiguration",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_Regular_ChannelName",
                table: "Regular",
                column: "ChannelName");

            migrationBuilder.CreateIndex(
                name: "IX_WhitelistedDomain_ChannelName",
                table: "WhitelistedDomain",
                column: "ChannelName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Announcement");

            migrationBuilder.DropTable(
                name: "BannedWord");

            migrationBuilder.DropTable(
                name: "ChannelStatistics");

            migrationBuilder.DropTable(
                name: "BotCommandConfiguration");

            migrationBuilder.DropTable(
                name: "ChannelCommandConfiguration");

            migrationBuilder.DropTable(
                name: "CustomCommand");

            migrationBuilder.DropTable(
                name: "DefaultCommandConfiguration");

            migrationBuilder.DropTable(
                name: "FilterConfiguration");

            migrationBuilder.DropTable(
                name: "FilterDescription");

            migrationBuilder.DropTable(
                name: "Regular");

            migrationBuilder.DropTable(
                name: "TwitchCredentials");

            migrationBuilder.DropTable(
                name: "WhitelistedDomain");

            migrationBuilder.DropTable(
                name: "Channel");
        }
    }
}
