﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ShikashiBot;

namespace ShikashiBot.Migrations
{
    [DbContext(typeof(PersistenceContext))]
    partial class PersistenceContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rc2-20901");

            modelBuilder.Entity("ShikashiBot.Model.Announcement", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsActive");

                    b.Property<string>("Message");

                    b.Property<int>("SecondsInterval");

                    b.HasKey("ChannelName", "Id");

                    b.HasIndex("ChannelName");

                    b.ToTable("Announcement");
                });

            modelBuilder.Entity("ShikashiBot.Model.BannedWord", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<string>("Word");

                    b.HasKey("ChannelName", "Word");

                    b.HasIndex("ChannelName");

                    b.ToTable("BannedWord");
                });

            modelBuilder.Entity("ShikashiBot.Model.Channel", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<bool>("Visiting");

                    b.HasKey("ChannelName");

                    b.ToTable("Channel");
                });

            modelBuilder.Entity("ShikashiBot.Model.ChannelStatistics", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<DateTime>("Timestamp");

                    b.Property<int>("BanCount");

                    b.Property<int>("MessageCount");

                    b.Property<int>("TimeoutCount");

                    b.Property<int>("ViewCount");

                    b.HasKey("ChannelName", "Timestamp");

                    b.HasIndex("ChannelName");

                    b.ToTable("ChannelStatistics");
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.BotCommandConfiguration", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<string>("CommandId");

                    b.Property<int>("MinRank");

                    b.Property<string>("Prefix");

                    b.HasKey("ChannelName", "CommandId");

                    b.HasIndex("ChannelName");

                    b.ToTable("BotCommandConfiguration");
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.ChannelCommandConfiguration", b =>
                {
                    b.Property<string>("CommandId");

                    b.Property<string>("ChannelName");

                    b.Property<bool>("Enabled");

                    b.Property<int>("MinRank");

                    b.Property<string>("Prefix");

                    b.HasKey("CommandId");

                    b.HasIndex("ChannelName");

                    b.ToTable("ChannelCommandConfiguration");
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.CustomCommand", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<string>("CommandId");

                    b.Property<bool>("IsActive");

                    b.Property<int>("MinRank");

                    b.Property<string>("Output");

                    b.HasKey("ChannelName", "CommandId");

                    b.HasIndex("ChannelName");

                    b.ToTable("CustomCommand");
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.DefaultCommandConfiguration", b =>
                {
                    b.Property<string>("CommandId");

                    b.Property<string>("Description");

                    b.Property<int>("MinRank");

                    b.Property<string>("Prefix");

                    b.HasKey("CommandId");

                    b.ToTable("DefaultCommandConfiguration");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.FilterConfiguration", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ChannelName");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<bool>("Enabled");

                    b.Property<string>("OutputMessage");

                    b.Property<int>("RemoveType");

                    b.Property<int>("TimeoutSeconds");

                    b.HasKey("Id");

                    b.HasIndex("ChannelName");

                    b.ToTable("FilterConfiguration");

                    b.HasDiscriminator<string>("Discriminator").HasValue("FilterConfiguration");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.FilterDescription", b =>
                {
                    b.Property<string>("FilterId");

                    b.Property<string>("Description");

                    b.HasKey("FilterId");

                    b.ToTable("FilterDescription");
                });

            modelBuilder.Entity("ShikashiBot.Model.Regular", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<string>("TwitchUsername");

                    b.HasKey("ChannelName", "TwitchUsername");

                    b.HasIndex("ChannelName");

                    b.ToTable("Regular");
                });

            modelBuilder.Entity("ShikashiBot.Model.TwitchCredentials", b =>
                {
                    b.Property<string>("TwitchUsername");

                    b.Property<string>("AccessToken");

                    b.Property<string>("PublicAccessToken")
                        .IsRequired();

                    b.Property<string>("RefreshToken");

                    b.HasKey("TwitchUsername");

                    b.HasAlternateKey("PublicAccessToken");

                    b.ToTable("TwitchCredentials");
                });

            modelBuilder.Entity("ShikashiBot.Model.WhitelistedDomain", b =>
                {
                    b.Property<string>("ChannelName");

                    b.Property<string>("Domain");

                    b.HasKey("ChannelName", "Domain");

                    b.HasIndex("ChannelName");

                    b.ToTable("WhitelistedDomain");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.CapsFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");

                    b.Property<int>("CapsLimit");

                    b.ToTable("CapsFilter");

                    b.HasDiscriminator().HasValue("CapsFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.DomainFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");


                    b.ToTable("DomainFilter");

                    b.HasDiscriminator().HasValue("DomainFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.EmoteFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");

                    b.Property<int>("EmoteLimit");

                    b.ToTable("EmoteFilter");

                    b.HasDiscriminator().HasValue("EmoteFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.LengthFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");

                    b.Property<int>("MaxLength");

                    b.ToTable("LengthFilter");

                    b.HasDiscriminator().HasValue("LengthFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.RepeatFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");

                    b.Property<int>("WordLimit");

                    b.ToTable("RepeatFilter");

                    b.HasDiscriminator().HasValue("RepeatFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.SymbolFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");

                    b.Property<int>("MaxCount");

                    b.ToTable("SymbolFilter");

                    b.HasDiscriminator().HasValue("SymbolFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.WordFilter", b =>
                {
                    b.HasBaseType("ShikashiBot.Model.Filters.FilterConfiguration");


                    b.ToTable("WordFilter");

                    b.HasDiscriminator().HasValue("WordFilter");
                });

            modelBuilder.Entity("ShikashiBot.Model.Announcement", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.BannedWord", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.ChannelStatistics", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.BotCommandConfiguration", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.ChannelCommandConfiguration", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName");
                });

            modelBuilder.Entity("ShikashiBot.Model.Commands.CustomCommand", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.Filters.FilterConfiguration", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName");
                });

            modelBuilder.Entity("ShikashiBot.Model.Regular", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShikashiBot.Model.WhitelistedDomain", b =>
                {
                    b.HasOne("ShikashiBot.Model.Channel")
                        .WithMany()
                        .HasForeignKey("ChannelName")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
