﻿using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Services;

namespace ShikashiBot
{
    public interface IShikashiBotManager : IChannelProvider
    {
        /// <summary>
        /// Starts moderating a channel.
        /// </summary>
        /// <param name="channelModel">Information about the channel to moderate</param>
        /// <param name="rejoin">If true, the bot is rejoining and channelModel already exists in the database. Otherwise, false.</param>
        /// <returns>Channel that is being moderated.</returns>
        Channel ModerateChannel(Model.Channel channelModel, bool rejoin = false);

        /// <summary>
        /// Stops moderating a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        void StopModeratingChannel(string channelName);

        /// <summary>
        /// Prepares and starts up the bot
        /// </summary>
        void Initialize();

        /// <summary>
        /// The emote provider for the bot.
        /// </summary>
        IEmoticonProvider EmoteProvider { get; }

        /// <summary>
        /// Name of the bot's username.
        /// </summary>
        string BotUsername { get; }

        /// <summary>
        /// Manages statistics usage of the bot itself.
        /// </summary>
        IUsageStatisticsService StatisticsService { get; }

        /// <summary>
        /// Message broadcaster for IRC.
        /// </summary>
        IMessageBroadcaster Broadcaster { get; }
    }
}
