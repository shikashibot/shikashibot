﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using ShikashiBot.ViewModels;

namespace ShikashiBot.Policies
{
    /// <summary>
    /// Authorization handler for requests that wants to access something related to channels.
    /// </summary>
    public class ChannelAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, ChannelViewModel>
    {
        /// <summary>
        /// If a requesting user is not the owner of the channel, deny the request.
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="requirement">Requirement</param>
        /// <param name="resource">Resource being accessed</param>
        protected override void Handle(AuthorizationContext context, OperationAuthorizationRequirement requirement, ChannelViewModel resource)
        {
            if (context.User.Identity.Name != resource.ChannelName)
            {
                context.Fail();
            }

            context.Succeed(requirement);
        }
    }
}
