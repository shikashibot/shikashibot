﻿namespace ShikashiBot
{
    /// <summary>
    /// An object capable of providing channels.
    /// </summary>
    public interface IChannelProvider
    {
        /// <summary>
        /// Gets a channel specified by a channel name.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>The channel if exists, otherwise null.</returns>
        Channel GetChannel(string channelName);
    }
}
