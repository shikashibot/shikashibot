﻿using System.Threading;

namespace ShikashiBot.Announcing
{
    /// <summary>
    /// Class for regularly posting an announcement in the chat. 
    /// An announcement is a specified message that can be posted at a certain inteval in chat.
    /// </summary>
    public class Announcement
    {
        /// <summary>
        /// Model for the announcement.
        /// </summary>
        public Model.Announcement Model { get; private set; }

        /// <summary>
        /// Indicating whether the announcement is being posted regularly to the chat or not.
        /// </summary>
        public bool IsActive { get; private set; }

        public Timer timer;

        /// <summary>
        /// Creates a new announcement.
        /// </summary>
        /// <param name="model">Announcement configuration/model.</param>
        public Announcement(Model.Announcement model)
        {
            this.Model = model;
        }

        /// <summary>
        /// Activates the announcement for a channel.
        /// </summary>
        /// <param name="channel">Channel which the announcement bellongs to.</param>
        internal void Activate(Channel channel)
        {
            if (Model.IsActive)
            {
                this.IsActive = true;
                this.timer = new Timer(OnMessagePost, channel, Model.SecondsInterval * 1000, Model.SecondsInterval * 1000);
            }
        }

        private void OnMessagePost(object state)
        {
            Channel channel = (Channel)state;
            channel.BroadcastMessage(Model.Message);
        }

        /// <summary>
        /// Stops the announcement from being posted on a regular basis.
        /// </summary>
        internal void Deactivate()
        {
            this.IsActive = false;
            this.timer?.Dispose();
        }
    }
}
