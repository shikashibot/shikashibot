﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShikashiBot.Announcing
{
    /// <summary>
    /// Class for managing all announcement for one channel.
    /// </summary>
    public class AnnouncementHandler
    {
        private Dictionary<int, Announcement> announcements;
        private Channel channel;

        /// <summary>
        /// Creates a new announcement handler and loads all the announcements for the channel.
        /// </summary>
        /// <param name="channelName">Name of the channel which this announcement handler belongs to.</param>
        /// <param name="persistenceProvider">Persistence provider.</param>
        public AnnouncementHandler(Channel channel, PersistenceContext dbContext)
        {
            this.announcements = new Dictionary<int, Announcement>();
            this.channel = channel;

            var annoucementConfigs = from t in dbContext.Announcement
                                     where t.Channel.ChannelName == channel.ChannelName && t.IsActive
                                     select t;

            foreach (Model.Announcement announcementModel in annoucementConfigs)
            {
                AddAnnouncement(announcementModel);
            }
        }

        /// <summary>
        /// Gets all announcements for a specific channel.
        /// </summary>
        /// <returns>List of announcements.</returns>
        public List<Announcement> GetAllAnnouncements()
        {
            return announcements.Values.ToList();
        }
        
        /// <summary>
        /// Stops the announcement and removes it from the channel.
        /// </summary>
        /// <param name="announcement">Id of announcement to remove.</param>
        public void StopAnnouncement(int announcementId)
        {
            Announcement announcement = GetAnnouncement(announcementId);
            announcement?.Deactivate();
            announcements.Remove(announcementId);
        }

        /// <summary>
        /// Reloads an announcement from the database.
        /// </summary>
        /// <param name="announcementId">Announcement to reload</param>
        public async Task ReloadAnnouncement(int announcementId, PersistenceContext dbContext)
        {
            StopAnnouncement(announcementId);
            await LoadAnnouncement(announcementId, dbContext);
        }

        /// <summary>
        /// Loads an announcement from the database.
        /// </summary>
        /// <param name="announcementId">Id of announcement to load</param>
        /// <returns></returns>
        public async Task LoadAnnouncement(int announcementId, PersistenceContext dbContext)
        {
            var model = await (from t in dbContext.Announcement
                               where t.Id == announcementId
                               select t).SingleOrDefaultAsync();

            if (model != null && model.IsActive)
            {
                AddAnnouncement(model);
            }
        }

        /// <summary>
        /// Loads an announcement from an announcement model
        /// </summary>
        /// <param name="model">Model to load from</param>
        public void AddAnnouncement(Model.Announcement model)
        {
            Announcement announcement = new Announcement(model);
            announcements.Add(model.Id, announcement);
            announcement.Activate(channel);
        }

        /// <summary>
        /// Gets an announcement.
        /// </summary>
        /// <param name="announcementId">Announcement to be returned.</param>
        /// <param name="channel">Channel which the announcement belongs to.</param>
        public Announcement GetAnnouncement(int announcementId)
        {
            Announcement announcement = null;
            announcements.TryGetValue(announcementId, out announcement);
            return announcement;
        }

    }
}
