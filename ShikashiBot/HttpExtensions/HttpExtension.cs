﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ShikashiBot.HttpExtensions
{
    /// <summary>
    /// Defines all extension methods for status codes not already defined.
    /// </summary>
    public static class HttpExtension
    {
        /// <summary>
        /// Declares that the response was accepted.
        /// </summary>
        /// <param name="controller">Controller that returns accepted</param>
        /// <returns>A status core result with HTTP Accepted</returns>
        public static ActionResult Accepted(this Controller controller)
        {
            return new StatusCodeResult(202);
        }

        /// <summary>
        /// Results in an HTTP conflict response to the client.
        /// </summary>
        /// <param name="controller">Controller that will return conflict state</param>
        /// <returns>HTTP Conflict</returns>
        public static ActionResult Conflict(this Controller controller)
        {
            return new StatusCodeResult(StatusCodes.Status409Conflict);
        }
    }
}
