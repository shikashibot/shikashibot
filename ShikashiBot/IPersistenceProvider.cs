﻿namespace ShikashiBot
{
    /// <summary>
    /// A provider that provides persistence through a database.
    /// </summary>
    public interface IPersistenceProvider
    {
        /// <summary>
        /// Provides the current persistence context.
        /// </summary>
        PersistenceContext GetPersistenceContext();
    }
}
