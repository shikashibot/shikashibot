﻿using System;

namespace ShikashiBot.Services
{
    public interface IUsageStatisticsService
    {
        DateTime StartupTime { get; }
        int APICalls { get; }
        int PostedMessages { get; }
        int ExecutedCommands { get; }
        int MessagesProcessed { get; }
        void AddTwitchAPICall();
        void AddPostedMessage();
        void AddExecutedCommand();
        void AddMessageProcessed();
    }
}
