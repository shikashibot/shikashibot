﻿using System;
using System.Threading;

namespace ShikashiBot.Services
{
    public class UsageStatisticsService : IUsageStatisticsService
    {
        private int twitchAPICalls;
        private int messagePostCount;
        private int executedCommands;
        private int messagesProcessed;

        public DateTime StartupTime { get; private set; }

        public int APICalls
        {
            get
            {
                return twitchAPICalls;
            }
        }

        public int PostedMessages
        {
            get
            {
                return messagePostCount;
            }
        }

        public int ExecutedCommands
        {
            get
            {
                return executedCommands;
            }
        }

        public int MessagesProcessed
        {
            get
            {
                return messagesProcessed;
            }
        }

        public UsageStatisticsService()
        {
            this.StartupTime = DateTime.Now;
        }

        public void AddTwitchAPICall()
        {
            Interlocked.Increment(ref twitchAPICalls);
        }

        public void AddPostedMessage()
        {
            Interlocked.Increment(ref messagePostCount);
        }

        public void AddExecutedCommand()
        {
            Interlocked.Increment(ref executedCommands);
        }

        public void AddMessageProcessed()
        {
            Interlocked.Increment(ref messagesProcessed);
        }
    }
}
