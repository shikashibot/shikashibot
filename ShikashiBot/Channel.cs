﻿using IrcDotNet;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShikashiBot.Announcing;
using ShikashiBot.Messaging;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Messaging.Filtering;
using ShikashiBot.TwitchAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ShikashiBot
{
    /// <summary>
    /// Manages a channel that is currently being visited and moderated.
    /// </summary>
    public class Channel : IBroadcaster
    {
        /// <summary>
        /// Name of the channel.
        /// </summary>
        public string ChannelName { get { return ChannelModel.ChannelName; } }

        /// <summary>
        /// Channel model.
        /// </summary>
        internal Model.Channel ChannelModel { get; private set; }

        /// <summary>
        /// The permission handler for the channel.
        /// </summary>
        internal PermissionHandler PermissionHandler { get; private set; }

        /// <summary>
        /// The chat filters enabled in the channel.
        /// </summary>
        internal List<IChatMessageFilterHandler> ChatFilters { get; private set; }

        /// <summary>
        /// Reference to the main bot manager.
        /// </summary>
        internal IShikashiBotManager BotManager { get; private set; }

        /// <summary>
        /// Channel where the messages are being broadcasted to the chat (IRC).
        /// </summary>
        private IrcChannel ircChannel;


        public AnnouncementHandler AnnouncementHandler { get; private set; }

        /// <summary>
        /// Handler for the chat messages.
        /// </summary>
        internal ChatMessageHandler ChatHandler { get; private set; }

        public ChannelStatisticsHandler StatisticsHandler { get; private set; }

        private IPersistenceProvider persistenceProvider;
        
        /// <summary>
        /// Creats a new channel that is being moderated.
        /// </summary>
        /// <param name="channelModel">Data about the channel/config</param>
        /// <param name="botManager">Reference to the bot manager</param>
        /// <param name="persistenceProvider">Persistence provider</param>
        public Channel(Model.Channel channelModel, IShikashiBotManager botManager, IPersistenceProvider persistenceProvider, ILoggerFactory loggerFactory, bool apiOnlyMode, ITwitchStreamProvider streamProvider)
        {
            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();

            this.ChannelModel = channelModel;
            this.PermissionHandler = new PermissionHandler(ChannelModel.ChannelName, loggerFactory, apiOnlyMode);
            this.ChatFilters = new List<IChatMessageFilterHandler>();
            this.BotManager = botManager;
            this.AnnouncementHandler = new AnnouncementHandler(this, dbContext);
            this.ChatHandler = new ChatMessageHandler(channelModel.ChannelName, botManager.BotUsername, dbContext, loggerFactory, BotManager.EmoteProvider);
            this.StatisticsHandler = new ChannelStatisticsHandler(persistenceProvider, this, streamProvider);
            this.persistenceProvider = persistenceProvider;

            ReloadFilters();
        }

        /// <summary>
        /// Loads any external resources that the channel may need, such as channel metadata, rights list, emotes, etc.
        /// </summary>
        /// <returns></returns>
        public async Task LoadResources()
        {
            await ChatHandler.ChannelEmoteProvider.LoadEmotes(ChannelModel.ChannelName);
            await PermissionHandler.LoadPermissions();
        }

        public void ReloadFilters()
        {
            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();

            List<IChatMessageFilterHandler> chatFilters = new List<IChatMessageFilterHandler>();

            var configurations = (from t in dbContext.FilterConfiguration.Include(c => c.Channel)
                                  where t.Channel.ChannelName == ChannelName && t.Enabled
                                  select t).ToList();

            foreach (var config in configurations)
            {
                chatFilters.Add(config.ProduceHandler(persistenceProvider, BotManager));
            }

            this.ChatFilters = chatFilters;
        }

        /// <summary>
        /// Sends a messages in the chat for the channel.
        /// </summary>
        /// <param name="message">Message to send.</param>
        public void BroadcastMessage(string message)
        {
            if (message.StartsWith(".ban "))
            {
                StatisticsHandler.AddBan();
            }
            else if (message.StartsWith(".timeout "))
            {
                StatisticsHandler.AddTimeout();
            }

            BotManager.Broadcaster.RequestBroadcast(message, ircChannel);
        }

        /// <summary>
        /// Sends a Twitch related command to the channel.
        /// </summary>
        /// <param name="command">The command together with the prefix /</param>
        public void SendCommand(string command)
        {
            ircChannel.Client.LocalUser.SendMessage(ircChannel, command);

            if (command.StartsWith(".ban "))
            {
                StatisticsHandler.AddBan();
            }
            else if (command.StartsWith(".timeout "))
            {
                StatisticsHandler.AddTimeout();
            }

            BotManager.StatisticsService.AddExecutedCommand();
        }
        
        /// <summary>
        /// Sets the IRC channel for the channel that is being used.
        /// </summary>
        /// <param name="channel"></param>
        internal void SetIrcChannel(IrcChannel channel)
        {
            this.ircChannel = channel;

            if (ChannelName != BotManager.BotUsername)
                BroadcastMessage(string.Format("HeyGuys {0}, I'm here to moderator for your Twitch channel. Just incase if you forgot, please type \" /mod ShikashiBot\" without the quotation marks Kappa", ChannelName));
        }

        /// <summary>
        /// Gets a chat message filter for a type.
        /// </summary>
        /// <param name="type">Chat filter type</param>
        /// <returns>A registered filter with the associated type, otherwise null.</returns>
        internal IChatMessageFilterHandler GetMessageFilterHandler(Type type)
        {
            return ChatFilters.Where(t => t.GetType().IsAssignableFrom(type)).FirstOrDefault();
        }
    }
}
