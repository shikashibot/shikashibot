﻿namespace ShikashiBot.Messaging.Interceptor
{
    /// <summary>
    /// A class that can intercept messages and act on behalf of them. 
    /// Unlike commands, these are enabled by default and available for 
    /// everyone in addition to that they are triggered upon every message
    /// that is posted in the chat.
    /// </summary>
    abstract class MessageInterceptor : CommandHandler
    {
        public MessageInterceptor() : base(Permissions.UserType.Viewer)
        {
        }
    }
}
