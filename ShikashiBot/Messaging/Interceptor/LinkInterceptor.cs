﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace ShikashiBot.Messaging.Interceptor
{
    /// <summary>
    /// Intercepts links and provides useful information from Youtube and Soundcloud links in the chat.
    /// </summary>
    class LinkInterceptor : MessageInterceptor
    {
        private ILogger logger;

        public LinkInterceptor(ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger<LinkInterceptor>();
        }

        /// <summary>
        /// Posts the title of youtube and soundcloud links in the chat.
        /// </summary>
        /// <param name="messageParams"></param>
        public async override void Invoke(ChatMessageParams messageParams)
        {
            Uri url = Utillity.ParseUrl(messageParams.Parameters[0].Replace("www.", string.Empty));

            if (url != null)
            {
                if (url.Host == "youtube.com" || url.Host == "youtu.be" || url.Host == "soundcloud.com")
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");

                        try
                        {
                            var request = await client.GetAsync(url);
                            string response = await request.EnsureSuccessStatusCode().Content.ReadAsStringAsync();

                            string title = Regex.Match(response, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>", RegexOptions.IgnoreCase).Groups["Title"].Value.Replace(" | Free Listening on", " -");

                            messageParams.Context.SendMessage(string.Format("/me {0} link posted by {1}", title, messageParams.SenderUsername));
                        }
                        catch (Exception e)
                        {
                            logger.LogError(string.Format("Error while intercepting message with address {0}", url), e);
                            messageParams.Context.SendMessage(string.Format("/me Link posted by {0} was unavailable at this time, please try again later ", messageParams.SenderUsername));
                        }
                    }
                }
            }
        }
    }
}
