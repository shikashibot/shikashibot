﻿namespace ShikashiBot.Messaging
{
    /// <summary>
    /// Properties for a chat message being posted that is a command.
    /// </summary>
    struct ChatMessageParams
    {
        /// <summary>
        /// Channel name.
        /// </summary>
        internal string ChannelName { get; set; }

        /// <summary>
        /// Username of the person sending the message in the chat.
        /// </summary>
        internal string SenderUsername { get; set; }

        /// <summary>
        /// Parameters of the command.
        /// </summary>
        internal string[] Parameters { get; set; }

        /// <summary>
        /// Context of the chat message.
        /// </summary>
        internal IMessageContext Context { get; set; }

        /// <summary>
        /// Creates a new chat message parameter object.
        /// </summary>
        /// <param name="channelName">Channel name.</param>
        /// <param name="senderUsername">Sender username.</param>
        /// <param name="messageParams">The parameters of the message.</param>
        /// <param name="messageContext">Message context.</param>
        public ChatMessageParams(string channelName, string senderUsername, string[] messageParams, IMessageContext messageContext)
        {
            this.ChannelName = channelName;
            this.SenderUsername = senderUsername;
            this.Parameters = messageParams;
            this.Context = messageContext;
        }
    }
}
