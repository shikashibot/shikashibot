﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Sends the bot to a new channel.
    /// </summary>
    class JoinCommand : BotChannelCommand
    {
        public JoinCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Sends a confirmation message to the chat and makes the bot manager enter the requested channel.
        /// </summary>
        /// <param name="messageParams">Parameters.</param>
        /// <param name="userType">Requesting user type.</param>
        protected override void HandleCommand(ChatMessageParams messageParams, UserType userType)
        {
            messageParams.Context.SendMessage(string.Format("Now moderating {0}", messageParams.Parameters[1]));
            messageParams.Context.Channel.BotManager.ModerateChannel(new Model.Channel() { ChannelName = messageParams.Parameters[1] });
        }
    }
}
