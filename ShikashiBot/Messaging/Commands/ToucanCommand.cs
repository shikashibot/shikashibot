﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Posts the toucan asci-art in the chat.
    /// </summary>
    class ToucanCommand : CommandHandler
    {
        public ToucanCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Le toucan has arrived.
        /// </summary>
        /// <param name="messageParams">Parameters.</param>
        public override void Invoke(ChatMessageParams messageParams)
        {
            messageParams.Context.SendMessage("▄▄▄▀▀▀▄▄███▄ ░░░░░▄▀▀░░░░░░░▐░▀██▌ ░░░▄▀░░░░▄▄███░▌▀▀░▀█ ░░▄█░░▄▀▀▒▒▒▒▒▄▐░░░░█▌ ░▐█▀▄▀▄▄▄▄▀▀▀▀▌░░░░░▐█▄ ░▌▄▄▀▀░░░░░░░░▌░░░░▄███████▄ ░░░░░░░░░░░░░▐░░░░▐███████████▄ ░░░░░le░░░░░░░▐░░░░▐█████████████▄ ░░░░toucan░░░░░░▀▄░░░▐██████████████▄ ░░░░░░has░░░░░░░░▀▄▄████████████████▄ ░░░░░arrived░░░░░░░░░░░░█▀██████");
        }
    }
}
