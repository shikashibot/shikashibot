﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Abstract class for commands related to the bot's own channel, these are referred to as bot commands.
    /// The purpose of this class is to validate whether a user can perform the command or not towards the bot.
    /// </summary>
    abstract class BotChannelCommand : CommandHandler
    {
        /// <summary>
        /// Creates a new bot command.
        /// </summary>
        /// <param name="minRank">Min rank the user has to have in order to execute the command.</param>
        public BotChannelCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Checks if the user has sufficient rights to execute the command then executes the command if possible.
        /// </summary>
        /// <param name="messageParams"></param>
        public override void Invoke(ChatMessageParams messageParams)
        {
            UserType requestingUser = messageParams.Context.Channel.PermissionHandler.GetPermission(messageParams.SenderUsername);
            string targetChannel;

            if (messageParams.Parameters.Length == 1)
            {
                targetChannel = messageParams.SenderUsername;
                messageParams.Parameters = new string[] { messageParams.Parameters[0], targetChannel };
            }
            else
                targetChannel = messageParams.Parameters[1];

            if (targetChannel != messageParams.SenderUsername && requestingUser < UserType.Broadcaster)
            {
                messageParams.Context.SendMessage("I'm sorry John, I'm afraid I can't let you do that Kappa");
            }
            else
            {
                HandleCommand(messageParams, requestingUser);
            }
        }

        /// <summary>
        /// Handles execution of the command.
        /// </summary>
        /// <param name="messageParams">Parameters for the message.</param>
        /// <param name="userType">Type of the user performing the command.</param>
        protected abstract void HandleCommand(ChatMessageParams messageParams, UserType userType);
    }
}
