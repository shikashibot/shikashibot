﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Posts information about the bot itself in the chat.
    /// </summary>
    class AboutCommand : CommandHandler
    {
        public AboutCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Posts the about message in chat.
        /// </summary>
        /// <param name="messageParams">Params</param>
        public override void Invoke(ChatMessageParams messageParams)
        {
            messageParams.Context.SendMessage("HeyGuys " + messageParams.SenderUsername + ", you can check out who made this bot by having a look at https://bitbucket.org/shikashibot");
        }
    }
}
