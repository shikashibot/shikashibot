﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Makes the bot leave the channel.
    /// </summary>
    class LeaveCommand : BotChannelCommand
    {
        public LeaveCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Sends a confirmation message to the user and makes the bot leave the channel.
        /// </summary>
        /// <param name="messageParams">Parameters.</param>
        /// <param name="userType">Type of the user.</param>
        protected override void HandleCommand(ChatMessageParams messageParams, UserType userType)
        {
            messageParams.Context.SendMessage(string.Format("Stopped moderating the channel {0}", messageParams.Parameters[1]));
            messageParams.Context.Channel.BotManager.StopModeratingChannel(messageParams.Parameters[1]);
        }
    }
}
