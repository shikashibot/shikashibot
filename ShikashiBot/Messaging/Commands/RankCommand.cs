﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Shows rank information for a user.
    /// </summary>
    class RankCommand : CommandHandler
    {
        public RankCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Post the current rank for the user sending the command.
        /// </summary>
        /// <param name="messageParams">Parameters.</param>
        public override void Invoke(ChatMessageParams messageParams)
        {
            UserType type = messageParams.Context.Channel.PermissionHandler.GetPermission(messageParams.SenderUsername);
            messageParams.Context.SendMessage(messageParams.SenderUsername + " is a " + type.ToString());
        }
    }
}
