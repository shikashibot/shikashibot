﻿using ShikashiBot.Messaging.Filtering;
using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging.Commands
{
    /// <summary>
    /// Adds the user to a whitelist so that the user can send URLs if posting URLs is not allowed.
    /// </summary>
    class PermitCommand : CommandHandler
    {
        public PermitCommand(UserType minRank) : base(minRank)
        { }

        /// <summary>
        /// Adds the user to the whitelist and sends a confirmation message.
        /// </summary>
        /// <param name="messageParams">Parameters.</param>
        public override void Invoke(ChatMessageParams messageParams)
        {
            string username = messageParams.Parameters[1];

            URLFilterHandler urlFilter = messageParams.Context.Channel.GetMessageFilterHandler(typeof(URLFilterHandler)) as URLFilterHandler;
            urlFilter?.PermitUser(username);

            messageParams.Context.Channel.BroadcastMessage(string.Format("/me {0} has been permitted for {1} seconds", username, 30));
        }
    }
}
