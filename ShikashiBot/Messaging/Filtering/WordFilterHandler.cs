﻿using ShikashiBot.Model;
using ShikashiBot.Model.Filters;
using System.Collections.Generic;
using System.Linq;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Filter handler for banned words.
    /// </summary>
    public class WordFilterHandler : ChatMessageFilterHandler<WordFilter>
    {
        public HashSet<string> bannedWords { get; set; }
        private IPersistenceProvider persistenceProvider;
        private Model.Channel channel;

        /// <summary>
        /// Creates a new word filter handler.
        /// </summary>
        /// <param name="filter">Configuration</param>
        /// <param name="persistenceProvider">Persistence provider</param>
        public WordFilterHandler(WordFilter filter, IPersistenceProvider persistenceProvider) : base(filter)
        {
            this.bannedWords = new HashSet<string>();
            this.persistenceProvider = persistenceProvider;
            this.channel = filter.Channel;

            ReloadBannedWords();
        }

        /// <summary>
        /// Loads the banned words from the database and stores them in this object.
        /// </summary>
        public void ReloadBannedWords()
        {
            this.bannedWords.Clear();
            this.bannedWords.TrimExcess();

            PersistenceContext dbContext = persistenceProvider.GetPersistenceContext();
            
            var bannedWords = from t in dbContext.BannedWord
                                where t.Channel.ChannelName == channel.ChannelName
                                select t;

            foreach (BannedWord bannedWord in bannedWords)
            {
                this.bannedWords.Add(bannedWord.Word.ToLower());
            }
        }

        /// <summary>
        /// Checks if the message violates the filter by looking for emotes that are banned.
        /// </summary>
        /// <param name="channelName">Nane of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            string[] words = message.ToLower().Split(' ');

            foreach (string word in words)
            {
                if (bannedWords.Contains(word))
                    return true;
            }

            return false;
        }
    }
}