﻿using ShikashiBot.Model;
using ShikashiBot.Model.Filters;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Filter for messages with URLs.
    /// </summary>
    public class URLFilterHandler : ChatMessageFilterHandler<DomainFilter>
    {
        private HashSet<string> whitelistedDomains;
        private HashSet<string> regulars;
        private Dictionary<string, DateTime> permits;

        /// <summary>
        /// Creates a new URL filter handler.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="persistenceProvider"></param>
        public URLFilterHandler(DomainFilter filter, PersistenceContext dbContext) : base(filter)
        {
            this.whitelistedDomains = new HashSet<string>();
            this.regulars = new HashSet<string>();
            this.permits = new Dictionary<string, DateTime>();
            
            var whitelistedDomains = from t in dbContext.WhitelistedDomain
                                        where t.Channel.ChannelName == filter.Channel.ChannelName
                                        select t;

            foreach (WhitelistedDomain whitelistedDomain in whitelistedDomains)
            {
                this.whitelistedDomains.Add(whitelistedDomain.Domain);
            }

            var regulars = from t in dbContext.Regular
                            where t.Channel.ChannelName == filter.Channel.ChannelName
                            select t;

            foreach (Regular regular in regulars)
            {
                this.regulars.Add(regular.TwitchUsername);
            }
        }

        /// <summary>
        /// Permits a user for posting links.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="length">How many seconds the user should be allowed to post the message</param>
        internal void PermitUser(string username, int length = 30)
        {
            permits.Remove(username);
            permits.Add(username, DateTime.Now.AddSeconds(length));
        }

        /// <summary>
        /// Checks if the message violates the filter by looking at if the URL in the message is whitelisted or not.
        /// A user can also be permitted, meaning that the user is permitted to post links.
        /// </summary>
        /// <param name="channelName">Nane of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            if (regulars.Contains(senderUsername))
                return false;

            if (permits.ContainsKey(senderUsername))
            {
                DateTime expirationTime = permits[senderUsername];
                if (expirationTime < DateTime.Now)
                    permits.Remove(senderUsername);
                else
                    return false;
            }

            string[] words = message.ToLower().Split(' ');

            foreach (string word in words)
            {
                Uri url = Utillity.ParseUrl(word);

                if (url != null)
                {
                    if (!AllowedAddress(url))
                        return true;
                }
            }

            return false;
        }

        private bool AllowedAddress(Uri url)
        {
            foreach (string domain in whitelistedDomains)
            {
                if (url.Host.Replace("www.", string.Empty) == domain)
                    return true;
            }

            return false;
        }
    }
}
