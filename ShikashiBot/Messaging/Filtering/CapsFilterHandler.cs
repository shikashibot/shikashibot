﻿using ShikashiBot.Model.Filters;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Creates a new filter for limit of capitalized letters in a message.
    /// </summary>
    public class CapsFilterHandler : ChatMessageFilterHandler<CapsFilter>
    {
        /// <summary>
        /// Initializes a new caps filter handler.
        /// </summary>
        /// <param name="config">Configuration for the filter</param>
        public CapsFilterHandler(CapsFilter config) : base(config)
        {

        }

        /// <summary>
        /// Checks if the message violates the filter by counting the capitalized letters in the message.
        /// </summary>
        /// <param name="channelName">Name of the channel</param>
        /// <param name="senderUsername">Username of the person sending the message</param>
        /// <param name="message">Actual message being sent</param>
        /// <param name="messageContext">Context of the message</param>
        /// <returns></returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            string[] words = message.Split(' ');
            int capsCount = 0;

            foreach (string word in words)
            {
                if (IsCapitalized(word))
                    capsCount += word.Length;
            }

            return capsCount > Configuration.CapsLimit;
        }

        private bool IsCapitalized(string word)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (char.IsLower(word[i]))
                    return false;
            }

            return true;
        }
    }
}
