﻿using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Model.Filters;
using System.Linq;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Emote filter handler.
    /// </summary>
    public class EmoticonsFilterHandler : ChatMessageFilterHandler<EmoteFilter>
    {
        private IEmoticonProvider emoticonProvider;

        /// <summary>
        /// Creates a new emote filter handler.
        /// </summary>
        /// <param name="filter">Configuraiton for the filter.</param>
        /// <param name="emoticonProvider">Emote provider.</param>
        public EmoticonsFilterHandler(EmoteFilter filter, IEmoticonProvider emoticonProvider) : base(filter)
        {
            this.emoticonProvider = emoticonProvider;
        }

        /// <summary>
        /// Checks if the message violates the filter by setting a limit for how many emotes per message are allowed.
        /// </summary>
        /// <param name="channelName">Name of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            Word mostFrequentlyUsed = message.Split(' ')
                .Where(x => emoticonProvider.IsEmoticon(x))
                .GroupBy(x => x.ToLower())
                .Select(x => new Word { Content = x.Key, Occurrences = x.Count() })
                .OrderBy(x => x.Occurrences)
                .FirstOrDefault();

            return mostFrequentlyUsed?.Occurrences > Configuration.EmoteLimit;
        }
    }
}
