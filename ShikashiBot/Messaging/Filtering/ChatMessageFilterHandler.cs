﻿using ShikashiBot.Model.Filters;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Generic class for filters.
    /// </summary>
    /// <typeparam name="T">Type of the configuration for the filter.</typeparam>
    public abstract class ChatMessageFilterHandler<T> : IChatMessageFilterHandler where T : FilterConfiguration
    {
        /// <summary>
        /// Filter configuration.
        /// </summary>
        public T Configuration { get; protected set; }

        /// <summary>
        /// Checks if the message violates the filter.
        /// </summary>
        /// <param name="channelName">Name of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Name of the sender.</param>
        /// <param name="message">Actual message being sent.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if the message violates the filter, otherwise false.</returns>
        protected abstract bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext);

        /// <summary>
        /// Initializes a new instance of the filter handler.
        /// </summary>
        /// <param name="config">Configuration data.</param>
        protected ChatMessageFilterHandler(T config)
        {
            this.Configuration = config;
        }

        /// <summary>
        /// Validates the message towards the filter then the appropriate action on the message is being performed.
        /// </summary>
        /// <param name="channelName">Name of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Name of the sender.</param>
        /// <param name="message">Actual message being sent.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if the message violates the filter, otherwise false.</returns>
        public bool HandleMessage(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            if (MessageViolatesFilter(channelName, senderUsername, message, messageContext))
            {
                if (Configuration.RemoveType == ChatMessageViolationActionType.Ban)
                {
                    // Ban the fucker
                    messageContext.SendMessage(string.Format(".ban {0}", senderUsername));
                }
                else if (Configuration.RemoveType == ChatMessageViolationActionType.Timeout)
                {
                    // Timeout 
                    messageContext.SendMessage(string.Format(".timeout {0} {1}", senderUsername, Configuration.TimeoutSeconds));
                }
                
                if (!string.IsNullOrEmpty(Configuration.OutputMessage))
                    messageContext.SendMessage(string.Format(Configuration.OutputMessage, senderUsername, Configuration.TimeoutSeconds));
                return true;
            }

            return false;
        }
    }
}
