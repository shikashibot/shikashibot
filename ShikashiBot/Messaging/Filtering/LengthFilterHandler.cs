﻿using ShikashiBot.Model.Filters;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Message filter for max length on messages.
    /// </summary>
    public class LengthFilterHandler : ChatMessageFilterHandler<LengthFilter>
    {
        /// <summary>
        /// Creates a new length filter handler.
        /// </summary>
        /// <param name="filter">Configuration</param>
        public LengthFilterHandler(LengthFilter filter) : base(filter)
        {
        }

        /// <summary>
        /// Checks if the message violates the filter by looking at the message length.
        /// </summary>
        /// <param name="channelName">Nane of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            return message.Length > Configuration.MaxLength;
        }
    }
}
