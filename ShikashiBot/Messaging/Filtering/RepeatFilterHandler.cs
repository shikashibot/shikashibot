﻿using ShikashiBot.Model.Filters;
using System.Linq;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Filter for word repetition.
    /// </summary>
    public class RepeatFilterHandler : ChatMessageFilterHandler<RepeatFilter>
    {
        /// <summary>
        /// Creates a new repeat filter handler.
        /// </summary>
        /// <param name="filter"></param>
        public RepeatFilterHandler(RepeatFilter filter) : base(filter)
        {
        }

        /// <summary>
        /// Checks if the message violates the filter by looking at how many times each word has occurred.
        /// </summary>
        /// <param name="channelName">Nane of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            Word mostFrequentlyUsed = message.Split(' ')
                .GroupBy(x => x.ToLower())
                .Select(x => new Word { Content = x.Key, Occurrences = x.Count() })
                .OrderBy(x => x.Occurrences)
                .FirstOrDefault();

            return mostFrequentlyUsed?.Occurrences > Configuration.WordLimit;
        }
    }

    internal class Word
    {
        internal string Content { get; set; }
        internal int Occurrences { get; set; }
    }
}
