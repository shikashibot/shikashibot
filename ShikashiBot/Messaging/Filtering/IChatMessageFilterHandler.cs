﻿namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Interface for all filters that are capable of filtering messages.
    /// </summary>
    public interface IChatMessageFilterHandler
    {
        bool HandleMessage(string channelName, string senderUsername, string message, IMessageContext messageContext);
    }
}
