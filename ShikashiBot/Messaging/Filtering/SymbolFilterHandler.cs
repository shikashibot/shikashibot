﻿using ShikashiBot.Model.Filters;
using System.Linq;

namespace ShikashiBot.Messaging.Filtering
{
    /// <summary>
    /// Filter handler for special symbol limit.
    /// </summary>
    public class SymbolFilterHandler : ChatMessageFilterHandler<SymbolFilter>
    {
        /// <summary>
        /// Creates a new symbol filter handler.
        /// </summary>
        /// <param name="filter"></param>
        public SymbolFilterHandler(SymbolFilter filter) : base(filter)
        {
        }

        /// <summary>
        /// Checks if the message violates the filter by counting the special symbols in the message.
        /// </summary>
        /// <param name="channelName">Nane of the channel where the message is being posted.</param>
        /// <param name="senderUsername">Username of the sender.</param>
        /// <param name="message">Actual message content.</param>
        /// <param name="messageContext">Message context.</param>
        /// <returns>True if violates, otherwise false.</returns>
        protected override bool MessageViolatesFilter(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            return message.Where(x => !char.IsLetterOrDigit(x)).Count() > Configuration.MaxCount;
        }
    }
}
