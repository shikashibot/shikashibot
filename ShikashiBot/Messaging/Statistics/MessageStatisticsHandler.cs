﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ShikashiBot.Messaging.Statistics
{
    /// <summary>
    /// Manages statistics over how frequent a word/phrase/emote (element) is used.
    /// </summary>
    public class MessageStatisticsHandler
    {
        private Dictionary<string, MessageEntity> elements;
        private object syncRoot;
        private Timer drainer;
        private const double POINTS_CAP_WEIGHT = 9000000;

        /// <summary>
        /// Prepares a new statistics handler.
        /// </summary>
        public MessageStatisticsHandler()
        {
            this.elements = new Dictionary<string, MessageEntity>();
            this.syncRoot = new object();
            this.drainer = new Timer(AddTimedWeight, null, 0, 10000);
        }

        /// <summary>
        /// Adds a weight to each item depending on their age, removes items with score lower than 10.
        /// </summary>
        /// <param name="o"></param>
        private void AddTimedWeight(object o)
        {
            lock (syncRoot)
            {
                foreach (var pair in elements)
                {
                    DecrementTime(pair.Value);
                }

                elements.Where(t => t.Value.Score < 10)
                    .Select(t => t.Key)
                    .ToList()
                    .ForEach(q => elements.Remove(q));
            }
        }

        private void DecrementTime(MessageEntity entity)
        {
            double secondsSinceLastTime = (DateTime.Now - entity.LastPosted).TotalSeconds + 2;
            double decreaseFactor = 1 - (secondsSinceLastTime / 600);

            entity.Score = entity.Score * decreaseFactor;
        }

        /// <summary>
        /// Adds a new message to the statistics.
        /// </summary>
        /// <param name="message"></param>
        public void AddMessage(string message)
        {
            lock (syncRoot)
            {
                MessageEntity entity = null;
                if (elements.TryGetValue(message, out entity))
                {
                    double secondsSinceLastTime = (DateTime.Now - entity.LastPosted).TotalSeconds + 2;

                    if (secondsSinceLastTime < 2) // Todo: add 2 instead to avoid this if
                        secondsSinceLastTime = 2;

                    if (entity.Score < 600)
                    {
                        entity.Score = 600;
                    }

                    double increaseFactor = ((1 / secondsSinceLastTime) * 0.1) + 1;
                    double scoreCapFactor = 1 - ((entity.Score * increaseFactor) / POINTS_CAP_WEIGHT);

                    entity.Score = (entity.Score * scoreCapFactor * increaseFactor);

                    entity.LastPosted = DateTime.Now;
                }
                else
                {
                    entity = new MessageEntity
                    {
                        Score = 600,
                        Phrase = message,
                        LastPosted = DateTime.Now
                    };

                    elements.Add(message, entity);
                }
            }
        }

        /// <summary>
        /// Gets the most frequently used elements from the statistics.
        /// </summary>
        /// <param name="count">Max count of elements to return</param>
        /// <returns>Most frequently used elements</returns>
        public List<MessageEntity> GetTop(int count)
        {
            lock (syncRoot)
            {
                return elements.OrderByDescending(t => t.Value.Score)
                    .Select(t => t.Value)
                    .Take(count)
                    .ToList();
            }
        }

        /// <summary>
        /// Stops the statistics handler from adding weights to items.
        /// </summary>
        public void Destroy()
        {
            lock (syncRoot)
            {
                drainer.Dispose();
                elements.Clear();
            }
        }
    }
}
