﻿using System;

namespace ShikashiBot.Messaging.Statistics
{
    /// <summary>
    /// Holds information about a unique message/item.
    /// </summary>
    public class MessageEntity
    {
        /// <summary>
        /// Defines how frequent this item is being used. This value is reduced over time.
        /// </summary>
        public double Score { get; set; }

        /// <summary>
        /// The actual text content of the item.
        /// </summary>
        public string Phrase { get; set; }

        /// <summary>
        /// Last time there was any occurrences of this item.
        /// </summary>
        public DateTime LastPosted { get; set; }
    }
}
