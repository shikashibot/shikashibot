﻿using ShikashiBot.Permissions;

namespace ShikashiBot.Messaging
{
    /// <summary>
    /// Abstract class for all classes that handles commands.
    /// </summary>
    abstract class CommandHandler
    {
        private UserType requiredUserType;

        /// <summary>
        /// Creates a new command handler.
        /// </summary>
        /// <param name="requiredUserType">Min rank for having the command invoked.</param>
        public CommandHandler(UserType requiredUserType)
        {
            this.requiredUserType = requiredUserType;
        }

        /// <summary>
        /// Attempts to invoke the chat handler on a message.
        /// The invoke is rejected if the user has insufficient rights.
        /// </summary>
        /// <param name="messageParams">The message being posted.</param>
        /// <returns>True if the command was invoked, otherwise false.</returns>
        public bool TryInvoke(ChatMessageParams messageParams)
        {
            UserType userType = messageParams.Context.Channel.PermissionHandler.GetPermission(messageParams.SenderUsername);
            if (userType < requiredUserType)
                return false;

            Invoke(messageParams);
            return true;
        }

        /// <summary>
        /// Directly invokes the message handler.
        /// </summary>
        /// <param name="messageParams">The message being posted.</param>
        public abstract void Invoke(ChatMessageParams messageParams);
    }
}
