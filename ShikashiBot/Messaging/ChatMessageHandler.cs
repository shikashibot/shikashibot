﻿using Microsoft.Extensions.Logging;
using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Messaging.Filtering;
using ShikashiBot.Messaging.Interceptor;
using ShikashiBot.Messaging.Statistics;
using ShikashiBot.Model.Commands;
using ShikashiBot.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ShikashiBot.Messaging
{
    /// <summary>
    /// Handler for all chat messages.
    /// Each channel has its own chat handler that receives the messages and runs them through commands, filters, etc.
    /// </summary>
    class ChatMessageHandler
    {
        private ILogger logger;

        private Dictionary<string, CommandHandler> generalCommands;
        private Dictionary<string, CommandHandler> botCommands;
        private Dictionary<string, CustomCommand> customCommands;
        private List<MessageInterceptor> interceptors;
        private string botUsername;
        private IEmoticonProvider globalEmoteProvider;

        private const string DefaultNamespace = "ShikashiBot.Messaging.Commands.";
        public MessageStatisticsHandler PhraseStatistics { get; private set; }
        public MessageStatisticsHandler EmoteStatistics { get; private set; }

        public ChannelEmoteProvider ChannelEmoteProvider { get; private set; }

        /// <summary>
        /// Creates a new chat handler.
        /// </summary>
        /// <param name="channelName">Name of the channel</param>
        /// <param name="persistenceProvider">Persistence provider</param>
        public ChatMessageHandler(string channelName, string botUsername, PersistenceContext dbContext, ILoggerFactory loggerFactory, IEmoticonProvider emoteProvider)
        {
            this.botUsername = botUsername;
            this.logger = loggerFactory.CreateLogger<ChatMessageHandler>();
            this.generalCommands = new Dictionary<string, CommandHandler>();
            this.botCommands = new Dictionary<string, CommandHandler>();
            this.interceptors = new List<MessageInterceptor>();
            this.customCommands = new Dictionary<string, CustomCommand>();
            this.PhraseStatistics = new MessageStatisticsHandler();
            this.EmoteStatistics = new MessageStatisticsHandler();
            this.ChannelEmoteProvider = new ChannelEmoteProvider();
            this.globalEmoteProvider = emoteProvider;

            this.interceptors.Add(new LinkInterceptor(loggerFactory));
            ReloadCommands(channelName, dbContext);
        }

        public void ReloadCommands(string channelName, PersistenceContext dbContext)
        {
            Dictionary<string, CommandHandler> generalCommands = new Dictionary<string, CommandHandler>();
            Dictionary<string, CommandHandler> botCommands = new Dictionary<string, CommandHandler>();
            Dictionary<string, CustomCommand> customCommands = new Dictionary<string, CustomCommand>();

            var channelCommandConfig = from t in dbContext.ChannelCommandConfiguration
                                       where t.Channel.ChannelName == channelName
                                       select t;

            foreach (ChannelCommandConfiguration command in channelCommandConfig.Where(t => t.Enabled))
            {
                generalCommands.Add(command.Prefix, CreateCommandHandler(command.CommandId, command.MinRank));
                logger.LogDebug(string.Format("Added custom command {0} with prefix {1}", command.CommandId, command.Prefix));
            }

            var defaultCommandConfig = from t in dbContext.DefaultCommandConfiguration
                                       select t;

            foreach (DefaultCommandConfiguration command in defaultCommandConfig)
            {
                if (!generalCommands.ContainsKey(command.Prefix))
                {
                    generalCommands.Add(command.Prefix, CreateCommandHandler(command.CommandId, command.MinRank));
                }
                logger.LogDebug(string.Format("Added default command {0} with prefix {1}", command.CommandId, command.Prefix));
            }

            foreach (ChannelCommandConfiguration command in channelCommandConfig.Where(t => !t.Enabled))
            {
                generalCommands.Remove(command.Prefix);
                logger.LogDebug(string.Format("Disabled command {0} with prefix {1}", command.CommandId, command.Prefix));
            }

            if (channelName == botUsername)
            {
                var botCommandConfigs = from t in dbContext.BotCommandConfiguration
                                        select t;

                foreach (BotCommandConfiguration command in botCommandConfigs)
                {
                    botCommands.Add(command.Prefix, CreateCommandHandler(command.CommandId, command.MinRank));
                    logger.LogDebug(string.Format("Added bot command {0} with prefix {1}", command.CommandId, command.Prefix));
                }
            }

            var customCommandConfigs = from t in dbContext.CustomCommand
                                       where t.Channel.ChannelName == channelName && t.IsActive
                                       select t;

            foreach (CustomCommand command in customCommandConfigs)
            {
                customCommands.Add(command.CommandId, command);
            }

            this.generalCommands = generalCommands;
            this.botCommands = botCommands;
            this.customCommands = customCommands;
        }

        private CommandHandler CreateCommandHandler(string commandId, int minRank)
        {
            Type type = Type.GetType(DefaultNamespace + commandId);
            if (type == null)
            {
                logger.LogWarning($"{commandId} is not recognized as a chat command");
                return null;
            }

            ConstructorInfo constructur = type.GetConstructor(new Type[] { typeof(UserType) });

            return (CommandHandler)constructur.Invoke(new object[] { (UserType)minRank });
        }

        /// <summary>
        /// Called once a message is a posted in the chat.
        /// </summary>
        /// <param name="channelName">Name of the channel.</param>
        /// <param name="senderUsername">Username of the person posting the message.</param>
        /// <param name="message">The actual message.</param>
        /// <param name="messageContext">The message context.</param>
        internal void OnMessageReceived(string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            logger.LogDebug(string.Format("{0} said in {1}: {2}", senderUsername, channelName, message));

            if (senderUsername == botUsername)
            {
                return;
            }

            UserType botRank = messageContext.Channel.PermissionHandler.GetPermission(botUsername);

            if (message.StartsWith("!"))
            {
                string[] commandParams = message.Split(' ');
                HandleCommand(commandParams, generalCommands, channelName, senderUsername, message, messageContext);

                if (channelName == botUsername)
                    HandleCommand(commandParams, botCommands, channelName, senderUsername, message, messageContext);

                CustomCommand customCommand;
                UserType userType = messageContext.Channel.PermissionHandler.GetPermission(senderUsername);

                if (customCommands.TryGetValue(commandParams[0].Substring(1), out customCommand) && userType >= (UserType)customCommand.MinRank)
                {
                    string outputMessage = customCommand.Output
                        .Replace("{username}", senderUsername)
                        .Replace("{channelName}", channelName);

                    try
                    {
                        messageContext.SendMessage(string.Format(outputMessage, commandParams));
                    }
                    catch (FormatException ex)
                    {
                        logger.LogWarning("Exception occured while compsing custom chat message", ex);
                    }
                }
            }
            else //if (messageContext.GetChannel().PermissionHandler.GetPermission(senderUsername) < botRank)
            {
                foreach (IChatMessageFilterHandler filter in messageContext.Channel.ChatFilters)
                {
                    filter.HandleMessage(channelName, senderUsername, message, messageContext);
                }

                if (!globalEmoteProvider.IsEmoticon(message) && !ChannelEmoteProvider.IsEmote(message))
                {
                    PhraseStatistics.AddMessage(message);
                }

                var words = message.Split(' ').Distinct();
                foreach (string word in words)
                {
                    if (globalEmoteProvider.IsEmoticon(word) || ChannelEmoteProvider.IsEmote(word))
                    {
                        EmoteStatistics.AddMessage(word);
                    }
                }
            }

            foreach (MessageInterceptor interceptor in interceptors)
            {
                interceptor.TryInvoke(new ChatMessageParams(channelName, senderUsername, message.Split(' '), messageContext));
            }
        }

        private void HandleCommand(string[] commandParams, Dictionary<string, CommandHandler> commandHandlers, string channelName, string senderUsername, string message, IMessageContext messageContext)
        {
            CommandHandler commandHandler;

            if (commandHandlers.TryGetValue(commandParams[0].Substring(1), out commandHandler))
            {
                commandHandler.TryInvoke(new ChatMessageParams(channelName, senderUsername, commandParams, messageContext));
            }
        }
    }
}
