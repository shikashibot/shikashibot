﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Default emoticon loader that keeps track of the normal emotes available on Twitch.
    /// </summary>
    public class EmoticonLoader : IEmoticonProvider
    {
        private ILogger logger;

        private Hashtable emoticonsByCode;
        
        /// <summary>
        /// Creates a new emoticon loader.
        /// </summary>
        /// <param name="emoticonApiEndPoint">URI to the endpoint to load emotes from.</param>
        public EmoticonLoader(ILoggerFactory loggerFactory, IConfigurationRoot configuration)
        {
            this.logger = loggerFactory.CreateLogger<EmoticonLoader>();
            this.emoticonsByCode = new Hashtable();
        }

        /// <summary>
        /// Loads global Twitch emotes.
        /// </summary>
        /// <param name="client">Http client that will be used to get the emotes.</param>
        /// <param name="endPoint">API endpoint to get the emotes from</param>
        /// <returns></returns>
        public async Task LoadTwitchEmotes(HttpClient client, string endPoint = "https://api.twitch.tv/kraken/chat/emoticon_images")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                logger.LogError($"Unable to load Twitch emoticons (Response was HTTP ({response.StatusCode})");
                return;
            }

            EmoticonSet emoticonSet = await response.Content.ReadAsAsync<EmoticonSet>();

            foreach (Emoticon emoticon in emoticonSet.Emoticons)
            {
                RegisterEmote(emoticon);
            }
        }

        /// <summary>
        /// Loads global Better Twitch TV emotes.
        /// </summary>
        /// <param name="client">Http client that will be used to get the emotes.</param>
        /// <param name="endPoint">API endpoint to get the emotes from</param>
        /// <returns></returns>
        public async Task LoadBTTVEmotes(HttpClient client, string endPoint = "https://api.betterttv.net/2/emotes")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                logger.LogError($"Unable to load BTTV emoticons (Response was HTTP ({response.StatusCode})");
                return;
            }

            BTTVEmotecontainer emoticonSet = await response.Content.ReadAsAsync<BTTVEmotecontainer>();
            foreach (BTTVEmote emoticon in emoticonSet.Emoticons)
            {
                RegisterEmote(emoticon);
            }
        }

        /// <summary>
        /// Loads FrankerFaceZ global emotes.
        /// </summary>
        /// <param name="client">Http client that will be used to get the emotes.</param>
        /// <param name="endPoint">API endpoint to get the emotes from</param>
        /// <returns></returns>
        public async Task LoadFrankerFacezEmotes(HttpClient client, string endPoint = "https://api.frankerfacez.com/v1/set/global")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                logger.LogError($"Unable to load Frankerfacez emoticons (Response was HTTP ({response.StatusCode})");
                return;
            }
                
            dynamic emoteSet = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
            dynamic defaultSets = emoteSet.default_sets;
            dynamic sets = emoteSet.sets;
                
            foreach (int set in defaultSets)
            {
                dynamic emotes = sets[$"{set}"].emoticons;

                foreach (dynamic emoticon in emotes)
                {
                    FrankerFacezEmote emote = new FrankerFacezEmote
                    {
                        ID = emoticon.id,
                        Name = emoticon.name,
                        Owner = emoticon.owner.name
                    };

                    if (!emoticonsByCode.ContainsKey(emoticon.name))
                        emoticonsByCode.Add(emoticon.name, emote);
                }
            }
        }

        /// <summary>
        /// Loads global emotes from different emote providers.
        /// </summary>
        /// <returns></returns>
        public async Task LoadEmotesAsync()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    logger.LogInformation("Attempting to load emoticons from api.twitch.tv");
                    await LoadTwitchEmotes(client);
                    logger.LogInformation("Loading Frankerfacez emotes");
                    await LoadFrankerFacezEmotes(client);
                    logger.LogInformation("Loading BTTV emotes");
                    await LoadBTTVEmotes(client);
                }

                timer.Stop();
                logger.LogInformation($"Succesfully loaded global emotes ({timer.ElapsedMilliseconds} ms)");
            }
            catch (Exception e)
            {
                logger.LogError("Unable to load emote list", e);
                throw e;
            }
        }

        private void RegisterEmote(IEmote emote)
        {
            if (!emoticonsByCode.ContainsKey(emote.EmoteKey))
                emoticonsByCode.Add(emote.EmoteKey, emote);
        }
        
        /// <summary>
        /// Gets an emoticon by the emote code.
        /// </summary>
        /// <param name="code">Emote code, eg. Kappa</param>
        /// <returns>the emoticon, null if not found.</returns>
        public IEmote GetEmoticonByCode(string code)
        {
            return emoticonsByCode[code] as IEmote;
        }

        /// <summary>
        /// Checks if thbe word is an emoticon.
        /// </summary>
        /// <param name="word">A word</param>
        /// <returns>True if it is recognized as an emotiucon, otherwise false.</returns>
        public bool IsEmoticon(string word)
        {
            return emoticonsByCode.ContainsKey(word);
        }

        [DataContract]
        private class EmoticonSet
        {
            [DataMember(Name ="emoticons")]
            public List<Emoticon> Emoticons { get; set; }
        }

        [DataContract]
        private class BTTVEmotecontainer
        {
            [DataMember(Name = "emotes")]
            public List<BTTVEmote> Emoticons { get; set; }
        }
    }
}
