﻿using System.Runtime.Serialization;

namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Better Twitch TV Emote
    /// </summary>
    [DataContract]
    public class BTTVEmote : IEmote
    {
        [DataMember(Name = "id")]
        public string ID;

        [DataMember(Name = "code")]
        public string Code;

        [DataMember(Name = "channel")]
        public string Channel;

        public string EmoteKey
        {
            get
            {
                return Code;
            }
        }
    }
}
