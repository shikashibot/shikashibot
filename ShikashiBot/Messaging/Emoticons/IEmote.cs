﻿namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Indicates that a class is an emote that can be used in the chat.
    /// </summary>
    public interface IEmote
    {
        /// <summary>
        /// What text a user has to type to get said emote.
        /// </summary>
        string EmoteKey { get; }
    }
}
