﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Provides emotes specific for one channel.
    /// </summary>
    public class ChannelEmoteProvider
    {
        private Hashtable emotes;
        private List<TwitchEmote> twitchEmotes;

        public ChannelEmoteProvider()
        {
            this.emotes = new Hashtable();
            this.twitchEmotes = new List<TwitchEmote>();
        }

        /// <summary>
        /// Loads all the emotes for a specific channel.
        /// </summary>
        /// <param name="channelName">Name of the channel to load emotes for</param>
        /// <returns></returns>
        public async Task LoadEmotes(string channelName)
        {
            using (HttpClient client = new HttpClient())
            {
                await LoadFrankerFacezEmotes(channelName, client);
                await LoadTwitchEmotes(channelName, client);
                await LoadBTTVEmotes(channelName, client);
            }
        }

        /// <summary>
        /// Loads FrankerFaceZ emotes for a channel.
        /// </summary>
        /// <param name="channelName">Name of the channel to get emotes for</param>
        /// <param name="client">Client making the request</param>
        /// <param name="endPoint">API endpoint for getting the channel name</param>
        /// <returns></returns>
        public async Task LoadFrankerFacezEmotes(string channelName, HttpClient client, string endPoint = "https://api.frankerfacez.com/v1/room/{channelName}")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint.Replace("{channelName}", channelName));

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dynamic emoteRoom = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
                
                dynamic emoticons = emoteRoom.sets[$"{emoteRoom.room.set}"].emoticons;

                foreach (dynamic emoticon in emoticons)
                {
                    FrankerFacezEmote emote = new FrankerFacezEmote
                    {
                        ID = emoticon.id,
                        Name = emoticon.name,
                        Owner = emoticon.owner.name
                    };

                    if (!emotes.ContainsKey(emote.EmoteKey) && (bool)emoticon["public"])
                        emotes.Add(emote.EmoteKey, emote);
                }
            }
        }

        /// <summary>
        /// Loads Better Twitch TV emotes for a specific channel.
        /// </summary>
        /// <param name="channelName">Name of the channel to get emotes for</param>
        /// <param name="client">Client making the request</param>
        /// <param name="endPoint">API endpoint for getting the channel name</param>
        /// <returns></returns>
        public async Task LoadBTTVEmotes(string channelName, HttpClient client, string endPoint = "https://api.betterttv.net/2/channels/{channelName}")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint.Replace("{channelName}", channelName));

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dynamic channelEmotes = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());

                foreach (dynamic emoticon in channelEmotes.emotes)
                {
                    BTTVEmote emote = new BTTVEmote
                    {
                        Channel = emoticon.channel,
                        Code = emoticon.code,
                        ID = emoticon.id
                    };

                    if (!emotes.ContainsKey(emote.Code))
                    {
                        emotes.Add(emote.Code, emote);
                    }
                }
            }
        }

        /// <summary>
        /// Loads vanilla Twitch emotes from Twitch.
        /// </summary>
        /// <param name="channelName">Name of the channel to get emotes for</param>
        /// <param name="client">Client making the request</param>
        /// <param name="endPoint">API endpoint for getting the channel name</param>
        /// <returns></returns>
        public async Task LoadTwitchEmotes(string channelName, HttpClient client, string endPoint = "https://api.twitch.tv/kraken/chat/{channelName}/emoticons")
        {
            HttpResponseMessage response = await client.GetAsync(endPoint.Replace("{channelName}", channelName));

            dynamic channelEmotes = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());

            foreach (dynamic emoticon in channelEmotes.emoticons)
            {
                if (emoticon.state == "active")
                {
                    TwitchEmote emote = new TwitchEmote
                    {
                        IsSubEmote = emoticon.subscriber_only,
                        Regex = emoticon.regex,
                        Expression = new Regex($"^{emoticon.regex}$")
                    };

                    twitchEmotes.Add(emote);
                }
            }
        }

        /// <summary>
        /// Checks if a text is an emote.
        /// </summary>
        /// <param name="text">Text to check if is an emote</param>
        /// <returns>True if it is an emote, otherwise false</returns>
        public bool IsEmote(string text)
        {
            bool hasEmote = emotes.ContainsKey(text);

            if (hasEmote)
            {
                return true;
            }

            foreach (TwitchEmote emote in twitchEmotes)
            {
                Match match = emote.Expression.Match(text);
                if (match.Success)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
