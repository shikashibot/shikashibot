﻿using System.Threading.Tasks;

namespace ShikashiBot.Messaging.Emoticons
{
    public interface IEmoticonProvider
    {
        /// <summary>
        /// Gets an emoticon by the emote code.
        /// </summary>
        /// <param name="code">Emote code, eg. Kappa</param>
        /// <returns>the emoticon, null if not found.</returns>
        IEmote GetEmoticonByCode(string emoticon);

        /// <summary>
        /// Loads the emotes for the emote loader.
        /// </summary>
        /// <returns></returns>
        Task LoadEmotesAsync();

        /// <summary>
        /// Checks if thbe word is an emoticon.
        /// </summary>
        /// <param name="word">A word</param>
        /// <returns>True if it is recognized as an emotiucon, otherwise false.</returns>
        bool IsEmoticon(string word);
    }
}
