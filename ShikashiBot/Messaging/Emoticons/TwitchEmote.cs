﻿using System.Text.RegularExpressions;

namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Vanilla Twitch emote.
    /// </summary>
    public class TwitchEmote : IEmote
    {
        public string Regex { get; set; }

        public bool IsSubEmote { get; set; }

        public Regex Expression { get; set; }

        public string EmoteKey
        {
            get
            {
                return Regex;
            }
        }
    }
}
