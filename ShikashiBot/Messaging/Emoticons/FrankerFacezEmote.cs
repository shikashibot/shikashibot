﻿namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// FrankerFaceZ emote.
    /// </summary>
    public class FrankerFacezEmote : IEmote
    {
        public int ID { get; set; }
        public string Owner { get; set; }
        public string Name { get; set; }

        public string EmoteKey
        {
            get
            {
                return Name;
            }
        }
    }
}
