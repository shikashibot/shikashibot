﻿using System.Runtime.Serialization;

namespace ShikashiBot.Messaging.Emoticons
{
    /// <summary>
    /// Information about an emoticon that can be posted in chat.
    /// </summary>
    [DataContract]
    public class Emoticon : IEmote
    {
        /// <summary>
        /// Unique ID of the emoticon.
        /// </summary>
        [DataMember(Name = "id")]
        public int? ID { get; set; }

        /// <summary>
        /// The code for the emoticon, eg. Kappa
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Which channel the emote bellongs to.
        /// </summary>
        [DataMember(Name = "channel")]
        public string Channel { get; set; }

        /// <summary>
        /// The set which the emoticon belongs to. 
        /// One emoticon set = sub emotes thats available in a channel.
        /// </summary>
        [DataMember(Name = "emoticon_set")]
        public int? EmoticonSet { get; set; }

        public string EmoteKey
        {
            get
            {
                return Code;
            }
        }
    }
}
