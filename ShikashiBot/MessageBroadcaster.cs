﻿using IrcDotNet;
using Microsoft.Extensions.Logging;
using ShikashiBot.Services;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ShikashiBot
{
    /// <summary>
    /// A class for load balancing the message that are being sent to the Twitch.
    /// This is to comply with a rate limit that is specified in the API TOS.
    /// Note that this only applies to normal messages that are posted in the chat
    /// and not command that are sent, e.g. /timeout.
    /// </summary>
    class MessageBroadcaster : IMessageBroadcaster
    {
        private const int MaxMessagesPer30Seconds = 20;

        private class BroadcastRequest
        {
            internal string Message { get; set; }
            internal IrcChannel Channel { get; set; }

            internal void Send()
            {
                Channel?.Client.LocalUser.SendMessage(Channel, Message);
            }
        }

        private Queue<BroadcastRequest> requests;

        private Timer broadcastTimer;

        private IUsageStatisticsService usageStatisticsService;

        private const int MaxRequestRate = (int)(30000f / (float)MaxMessagesPer30Seconds);

        private ILogger logger;

        /// <summary>
        /// Creates a new message broadcaster.
        /// </summary>
        public MessageBroadcaster(IUsageStatisticsService usageStatisticsService, ILogger logger)
        {
            this.usageStatisticsService = usageStatisticsService;
            this.requests = new Queue<BroadcastRequest>();
            this.broadcastTimer = new Timer(OnMessageBroadcast, null, 0, MaxRequestRate);
            this.logger = logger;
        }

        /// <summary>
        /// Requests that a message gets broadcasted to the chat.
        /// </summary>
        /// <param name="message">Message to broadcast.</param>
        /// <param name="channel">Channel to broadcast to.</param>
        public void RequestBroadcast(string message, IrcChannel channel)
        {
            this.requests.Enqueue(new BroadcastRequest() { Message = message, Channel = channel });
        }

        private void OnMessageBroadcast(object state)
        {
            try
            {
                if (requests.Count > 0)
                {
                    BroadcastRequest request = requests.Dequeue();
                    if (request != null)
                    {
                        request.Send();
                        usageStatisticsService.AddPostedMessage();
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogCritical("Error while broadcasting message", e);
            }
        }
    }
}
