﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.Model
{
    /// <summary>
    /// Record for a channel that is being moderated by the bot.
    /// </summary>
    public class Channel
    {
        /// <summary>
        /// Name of the channel.
        /// </summary>
        [Key]
        public string ChannelName { get; set; }

        /// <summary>
        /// If the bot should be in the chat for the channel and moderate or not.
        /// </summary>
        public bool Visiting { get; set; }
    }
}
