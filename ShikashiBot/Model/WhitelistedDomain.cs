﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model
{
    /// <summary>
    /// Whitelisted domains are domains that users are able to post in chat although the filter is enabled.
    /// </summary>
    public class WhitelistedDomain
    {
        /// <summary>
        /// Channel which the whitelisted domain belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which the whitelisted domain belongs to.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// The actual domain that is whitelisted.
        /// </summary>
        public virtual string Domain { get; set; }
    }
}
