﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model
{
    /// <summary>
    /// A regular is a person that can be whitelisted from banned words/url filter as it is 
    /// "trusted" in some way in the stream.
    /// </summary>
    public class Regular
    {
        /// <summary>
        /// Channel which the regular belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which the regular belongs to.
        /// </summary>
        public string ChannelName { get; set; }
        
        /// <summary>
        /// Twitch username of the regular.
        /// </summary>
        public string TwitchUsername { get; set; }
    }
}
