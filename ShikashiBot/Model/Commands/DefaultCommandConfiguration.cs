﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.Model.Commands
{
    /// <summary>
    /// Default command configuration global for all channels.
    /// This config is applies if the channel does not have any configuration
    /// associated with itself on the specific command.
    /// </summary>
    public class DefaultCommandConfiguration
    {
        /// <summary>
        /// Class name of the command.
        /// </summary>
        [Key]
        public string CommandId { get; set; }

        /// <summary>
        /// Prefix of the command
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// The minimum rank for the message to be used by others. See UserType for more info.
        /// </summary>
        public int MinRank { get; set; }

        /// <summary>
        /// A description of what the command does.
        /// </summary>
        public string Description { get; set; }
    }
}
