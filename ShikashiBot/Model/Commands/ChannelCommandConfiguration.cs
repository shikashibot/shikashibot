﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.Model.Commands
{
    /// <summary>
    /// Config for commands specific to channels.
    /// </summary>
    public class ChannelCommandConfiguration
    {
        /// <summary>
        /// Class name of the command.
        /// </summary>
        [Key]
        public string CommandId { get; set; }

        /// <summary>
        /// Channel which the config belongs to.
        /// </summary>
        [Key]
        public Channel Channel { get; set; }
        
        /// <summary>
        /// Prefix of the command.
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// The minimum rank for the message to be used by others. See UserType for more info.
        /// </summary>
        public int MinRank { get; set; }

        /// <summary>
        /// Whether this command is enabled or not.
        /// </summary>
        public bool Enabled { get; set; }
    }
}
