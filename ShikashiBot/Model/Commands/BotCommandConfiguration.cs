﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model.Commands
{
    /// <summary>
    /// Configuration for command specific to the bot channel.
    /// </summary>
    public class BotCommandConfiguration
    {
        /// <summary>
        /// Class name of the command.
        /// </summary>
        public string CommandId { get; set; }

        /// <summary>
        /// Channel which the config bellongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which the config belongs to.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// Prefix of the command.
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// The minimum rank for the message to be used by others. See UserType for more info.
        /// </summary>
        public int MinRank { get; set; }
    }
}
