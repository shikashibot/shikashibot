﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model.Commands
{
    /// <summary>
    /// Config for custom commands, usually a user-defined output to a user-defined command.
    /// </summary>
    public class CustomCommand
    {
        /// <summary>
        /// Class name of the command.
        /// </summary>
        public string CommandId { get; set; }

        /// <summary>
        /// The channel which the command belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which it belongs to.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// The output message to be posted when the message is invoked.
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// Min rank for a user to invoke the command.
        /// </summary>
        public int MinRank { get; set; }

        /// <summary>
        /// Sets whether the command is active or not.
        /// </summary>
        public bool IsActive { get; set; }
    }
}
