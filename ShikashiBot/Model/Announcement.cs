﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model
{
    /// <summary>
    /// An announcement is a message that is continuusly posted in the chat.
    /// </summary>
    public class Announcement
    {
        /// <summary>
        /// Unique ID of the announcement.
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Channel which the announcement belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which it belongs to.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// Message that should be posted in the chat for the announcement.
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// How frequent the announcement should be posted.
        /// </summary>
        public int SecondsInterval { get; set; }
        
        /// <summary>
        /// Whether the announcement is active or not. If it is inactive, it is not posted in the chat.
        /// </summary>
        public bool IsActive { get; set; }
    }
}
