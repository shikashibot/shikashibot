﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model
{
    public class ChannelStatistics
    {
        /// <summary>
        /// Channel which the statistics belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which the statistics belongs to.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// When this statistics is for.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// How many messages there was posted.
        /// </summary>
        public int MessageCount { get; set; }

        /// <summary>
        /// How many bans were issued.
        /// </summary>
        public int BanCount { get; set; }

        /// <summary>
        /// How many timeouts were issued.
        /// </summary>
        public int TimeoutCount { get; set; }

        /// <summary>
        /// How many viewers was in the stream.
        /// </summary>
        public int ViewCount { get; set; }
    }
}
