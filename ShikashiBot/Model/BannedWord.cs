﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ShikashiBot.Model
{
    /// <summary>
    /// Banned word from a channel.
    /// </summary>
    public class BannedWord
    {
        /// <summary>
        /// Channel which the banned word belongs to.
        /// </summary>
        [ForeignKey("ChannelName")]
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel which the banned word belongs to.
        /// </summary>
        public string ChannelName { get; set; }
        
        /// <summary>
        /// The banned word.
        /// </summary>
        public string Word { get; set; }
    }
}
