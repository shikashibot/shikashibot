﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Symbol filter configuration.
    /// </summary>
    public class SymbolFilter : FilterConfiguration
    {
        /// <summary>
        /// Max allowed special symbols.
        /// </summary>
        public int MaxCount { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A handler for this filter</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new SymbolFilterHandler(this);
        }
    }
}
