﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Command config for caps filter.
    /// </summary>
    public class CapsFilter : FilterConfiguration
    {
        /// <summary>
        /// Max limit on capitalized letters per message.
        /// </summary>
        public int CapsLimit { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A new caps filter handler</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new CapsFilterHandler(this);
        }
    }
}
