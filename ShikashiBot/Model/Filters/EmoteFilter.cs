﻿using ShikashiBot.Messaging.Emoticons;
using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Filter configuration for the emote filter.
    /// </summary>
    public class EmoteFilter : FilterConfiguration
    {
        /// <summary>
        /// Limit for how many emotes per message.
        /// </summary>
        public int EmoteLimit { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>An emote filter handler</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new EmoticonsFilterHandler(this, botManager.EmoteProvider);
        }
    }
}
