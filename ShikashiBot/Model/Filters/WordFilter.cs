﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Configuration for banned words.
    /// </summary>
    public class WordFilter : FilterConfiguration
    {
        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A handler for this filter</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new WordFilterHandler(this, persistenceProvider);
        }
    }
}
