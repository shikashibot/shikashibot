﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Filter configuration for message repetition in messages.
    /// </summary>
    public class RepeatFilter : FilterConfiguration
    {
        /// <summary>
        /// Max limit on message repetitions.
        /// </summary>
        public int WordLimit { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A handler for this filter</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new RepeatFilterHandler(this);
        }
    }
}
