﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Filter configuration for max length on messages.
    /// </summary>
    public class LengthFilter : FilterConfiguration
    {
        /// <summary>
        /// Max length of the messages.
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A handler for this filter</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new LengthFilterHandler(this);
        }
    }
}
