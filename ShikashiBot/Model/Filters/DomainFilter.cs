﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Filter configuration for the domain filter.
    /// </summary>
    public class DomainFilter : FilterConfiguration
    {
        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A domain filter handler</returns>
        public override IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager)
        {
            return new URLFilterHandler(this, persistenceProvider.GetPersistenceContext());
        }
    }
}
