﻿using ShikashiBot.Messaging.Filtering;

namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Class for all filter configurations.
    /// </summary>
    public abstract class FilterConfiguration
    {
        /// <summary>
        /// Id that you shall not use because this is to avoid an issue with generics and ef.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Channel which the configuration belongs to.
        /// </summary>
        public Channel Channel { get; set; }

        /// <summary>
        /// Name of the channel.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// Action to perform once the filter is being violated.
        /// </summary>
        public ChatMessageViolationActionType RemoveType { get; set; }

        /// <summary>
        /// How long a user should be timed out (Depends on RemoveType).
        /// </summary>
        public int TimeoutSeconds { get; set; }

        /// <summary>
        /// What to post in the chat if the filter is violated.
        /// </summary>
        public string OutputMessage { get; set; }

        /// <summary>
        /// Produces a chat message handler with the logic for the filter.
        /// </summary>
        /// <param name="persistenceProvider">Persistence provider.</param>
        /// <returns>A handler for this filter</returns>
        public abstract IChatMessageFilterHandler ProduceHandler(IPersistenceProvider persistenceProvider, IShikashiBotManager botManager); 

        /// <summary>
        /// Whether the filter is enabled or not.
        /// </summary>
        public bool Enabled { get; set; }
    }

    public enum ChatMessageViolationActionType : int
    {
        Ignore = 0,
        Timeout = 1,
        Ban = 2
    }
}
