﻿namespace ShikashiBot.Model.Filters
{
    /// <summary>
    /// Defines description for each filter available in the system.
    /// </summary>
    public class FilterDescription
    {
        /// <summary>
        /// A unique ID of the filter (usually the class name of the filter)
        /// </summary>
        public string FilterId { get; set; }

        /// <summary>
        /// A user-friendly text as the description of the filter.
        /// </summary>
        public string Description { get; set; }
    }
}
