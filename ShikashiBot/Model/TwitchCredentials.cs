﻿using System.ComponentModel.DataAnnotations;

namespace ShikashiBot.Model
{
    /// <summary>
    /// Credentials for a user.
    /// </summary>
    public class TwitchCredentials
    {
        /// <summary>
        /// Name of the user on Twitch.
        /// </summary>
        [Key]
        public string TwitchUsername { get; set; }

        /// <summary>
        /// OAuth token being used towards Twitch servers.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Refresh token.
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Access token that the client uses to access this service.
        /// </summary>
        public string PublicAccessToken { get; set; }
    }
}
