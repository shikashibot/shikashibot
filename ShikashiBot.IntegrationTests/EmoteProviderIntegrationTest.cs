﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Logging;
using Moq;
using ShikashiBot.Messaging.Emoticons;
using System.Net;
using System.Net.Http;
using Xunit;

namespace ShikashiBot.IntegrationTests
{
    public class EmoteProviderIntegrationTest
    {
        private TestServer testServer;

        public EmoteProviderIntegrationTest()
        {
            this.testServer = new TestServer(new WebHostBuilder().Configure(app => app.UseStaticFiles()));
        }
        
        [Fact]
        public async void FrankerFaceZEmotesAreDetected()
        {
            // Arrange
            var provider = new ChannelEmoteProvider();
            using (var client = testServer.CreateClient())
            {
                await provider.LoadFrankerFacezEmotes("testUser", client, "/emotes/frankerfacez/channel.json");
            }

            // Act / Assert
            Assert.True(provider.IsEmote("MasterSword"));
            Assert.False(provider.IsEmote("blaaaa"));
        }
        
        [Fact]
        public async void BTTVEmotesAreDetected()
        {
            // Arrange
            var provider = new ChannelEmoteProvider();
            using (var client = testServer.CreateClient())
            {
                await provider.LoadBTTVEmotes("forsenlol", client, "/emotes/bttv/channel.json");
            }

            // Act / Assert
            Assert.True(provider.IsEmote("pajaPls"));
            Assert.False(provider.IsEmote("blaaaa"));
        }

        [Fact]
        public async void SubEmotesAreIdentified()
        {
            // Arrange
            var provider = new ChannelEmoteProvider();
            using (var client = testServer.CreateClient())
            {
                await provider.LoadTwitchEmotes("forsenlol", client, "/emotes/twitch/channel.json");
            }

            // Act / Assert
            Assert.True(provider.IsEmote("forsenE"));
            Assert.False(provider.IsEmote("blaaaa"));
        }
        
        [Fact]
        public void TwitchRegexEmotesAreIdentified()
        {
            // Arrange
            var provider = new ChannelEmoteProvider();
            using (var client = testServer.CreateClient())
            {
                provider.LoadTwitchEmotes("forsenlol", client, "/emotes/twitch/channel.json").Wait();
            }

            // Act / Assert
            Assert.True(provider.IsEmote(":-)"));
        }

        [Fact]
        public async void GlobalEmotesAreIdentified()
        {
            // Arrange
            var loggerFactory = new Mock<ILoggerFactory>();
            var logger = new Mock<ILogger>();
            loggerFactory.Setup(c => c.CreateLogger(It.IsAny<string>())).Returns(logger.Object);

            var provider = new EmoticonLoader(loggerFactory.Object, null);
            using (var client = testServer.CreateClient())
            {
                await provider.LoadBTTVEmotes(client, "/emotes/bttv/global.json");
                await provider.LoadFrankerFacezEmotes(client, "/emotes/frankerfacez/global.json");
                await provider.LoadTwitchEmotes(client, "/emotes/twitch/global.json");
            }

            // Act / Assert
            Assert.True(provider.IsEmoticon("Kappa")); // Twitch emote
            Assert.True(provider.IsEmoticon("FishMoley")); // BTTV emote
            Assert.True(provider.IsEmoticon("Kappa")); // FrankerFaceZ emote
        }
    }
}
