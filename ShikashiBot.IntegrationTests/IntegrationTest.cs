﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using ShikashiBot.IntegrationTests.Extensions;
using System.IO;
using System.Net;
using Xunit;

namespace ShikashiBot.IntegrationTests
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class IntegrationTest
    {
        private TestServer server;

        public IntegrationTest()
        {
            this.server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
        }

        [Fact]
        public async void EndPointsRequiresAuthorization()
        {
            using (var client = server.CreateClient().AcceptJson())
            {
                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/caps"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/caps", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/domain"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/domain", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/emote"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/emote", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/length"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/length", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/repeat"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/repeat", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/symbol"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/symbol", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters/word"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/filters/word", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/filters"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.DeleteAsync("/api/channels/{channelName}/announcements/1"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.GetAsync("/api/channels/{channelName}/announcements/1"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/announcements/1", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PostAsync("/api/channels/{channelName}/announcements", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.GetAsync("/api/channels/{channelName}/announcements"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/auth"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/auth"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/bannedWords"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/bannedWords", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/channels/{channelName}/bannedWords/word"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.PostAsync("/api/channels", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/channels/channelName"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.GetAsync("/api/channels/channelName"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/statistics/channel/recent"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.GetAsync("/api/channels/{channelName}/statistics/channel/weekly"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/commands"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/commands/commandId", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.PostAsync("/api/channels/{channelName}/customCommands/{commandId}", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/channels/{channelName}/customCommands/{commandId}"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/customCommands/{commandId}", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/customCommands/{commandId}/active", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/customCommands"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/statistics/messages"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.GetAsync("/api/channels/{channelName}/statistics/emotes"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/regulars"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/regulars", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/channels/{channelName}/regulars/regular"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));

                Assert.True((await client.GetAsync("/api/channels/{channelName}/whitelistedDomains"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.PutAsync("/api/channels/{channelName}/whitelistedDomains", null))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
                Assert.True((await client.DeleteAsync("/api/channels/{channelName}/whitelistedDomains/domain"))
                    .HasStatusCode(HttpStatusCode.Unauthorized));
            }
        }
    }
}
