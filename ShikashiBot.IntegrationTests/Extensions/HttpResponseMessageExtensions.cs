﻿using System.Net;
using System.Net.Http;

namespace ShikashiBot.IntegrationTests.Extensions
{
    public static class HttpResponseMessageExtensions
    {
        public static bool HasStatusCode(this HttpResponseMessage response, HttpStatusCode code)
        {
            return response.StatusCode == code;
        }
    }
}
