/*
Navicat PGSQL Data Transfer

Source Server         : Local [PG]
Source Server Version : 90501
Source Host           : localhost:5432
Source Database       : shikashibot
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90501
File Encoding         : 65001

Date: 2016-05-22 11:43:06
*/


-- ----------------------------
-- Sequence structure for "public"."Announcement_Id_seq"
-- ----------------------------
DROP SEQUENCE "public"."Announcement_Id_seq";
CREATE SEQUENCE "public"."Announcement_Id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."Announcement_Id_seq1"
-- ----------------------------
DROP SEQUENCE "public"."Announcement_Id_seq1";
CREATE SEQUENCE "public"."Announcement_Id_seq1"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."FilterConfiguration_Id_seq"
-- ----------------------------
DROP SEQUENCE "public"."FilterConfiguration_Id_seq";
CREATE SEQUENCE "public"."FilterConfiguration_Id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."__EFMigrationsHistory"
-- ----------------------------
DROP TABLE "public"."__EFMigrationsHistory";
CREATE TABLE "public"."__EFMigrationsHistory" (
"MigrationId" text NOT NULL,
"ProductVersion" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of __EFMigrationsHistory
-- ----------------------------
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160420121037_init', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160421100900_EnableDisableCommands', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160425140528_EnableDisableFilters', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160425143828_WhitelistedDomainPkFix', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160427113039_CommandDescription', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160427114950_FilterDescription', '7.0.0-rc1-16348');
INSERT INTO "public"."__EFMigrationsHistory" VALUES ('20160504125131_Statistics', '7.0.0-rc1-16348');

-- ----------------------------
-- Table structure for "public"."Announcement"
-- ----------------------------
DROP TABLE "public"."Announcement";
CREATE TABLE "public"."Announcement" (
"ChannelName" text NOT NULL,
"Id" int4 DEFAULT nextval('"Announcement_Id_seq1"'::regclass) NOT NULL,
"IsActive" bool NOT NULL,
"Message" text,
"SecondsInterval" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Announcement
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."BannedWord"
-- ----------------------------
DROP TABLE "public"."BannedWord";
CREATE TABLE "public"."BannedWord" (
"ChannelName" text NOT NULL,
"Word" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of BannedWord
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."BotCommandConfiguration"
-- ----------------------------
DROP TABLE "public"."BotCommandConfiguration";
CREATE TABLE "public"."BotCommandConfiguration" (
"ChannelName" text NOT NULL,
"CommandId" text NOT NULL,
"MinRank" int4 NOT NULL,
"Prefix" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of BotCommandConfiguration
-- ----------------------------
INSERT INTO "public"."BotCommandConfiguration" VALUES ('shikashibot', 'JoinCommand', '1', 'join');
INSERT INTO "public"."BotCommandConfiguration" VALUES ('shikashibot', 'LeaveCommand', '1', 'leave');

-- ----------------------------
-- Table structure for "public"."Channel"
-- ----------------------------
DROP TABLE "public"."Channel";
CREATE TABLE "public"."Channel" (
"ChannelName" text NOT NULL,
"Visiting" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Channel
-- ----------------------------
INSERT INTO "public"."Channel" VALUES ('shikashibot', 't');

-- ----------------------------
-- Table structure for "public"."ChannelCommandConfiguration"
-- ----------------------------
DROP TABLE "public"."ChannelCommandConfiguration";
CREATE TABLE "public"."ChannelCommandConfiguration" (
"CommandId" text NOT NULL,
"ChannelName" text,
"MinRank" int4 NOT NULL,
"Prefix" text,
"Enabled" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ChannelCommandConfiguration
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."ChannelStatistics"
-- ----------------------------
DROP TABLE "public"."ChannelStatistics";
CREATE TABLE "public"."ChannelStatistics" (
"ChannelName" text NOT NULL,
"Timestamp" timestamp(6) NOT NULL,
"BanCount" int4 NOT NULL,
"MessageCount" int4 NOT NULL,
"TimeoutCount" int4 NOT NULL,
"ViewCount" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ChannelStatistics
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."CustomCommand"
-- ----------------------------
DROP TABLE "public"."CustomCommand";
CREATE TABLE "public"."CustomCommand" (
"ChannelName" text NOT NULL,
"CommandId" text NOT NULL,
"IsActive" bool NOT NULL,
"MinRank" int4 NOT NULL,
"Output" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of CustomCommand
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."DefaultCommandConfiguration"
-- ----------------------------
DROP TABLE "public"."DefaultCommandConfiguration";
CREATE TABLE "public"."DefaultCommandConfiguration" (
"CommandId" text NOT NULL,
"MinRank" int4 NOT NULL,
"Prefix" text,
"Description" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of DefaultCommandConfiguration
-- ----------------------------
INSERT INTO "public"."DefaultCommandConfiguration" VALUES ('AboutCommand', '1', 'about', 'The about command');
INSERT INTO "public"."DefaultCommandConfiguration" VALUES ('PermitCommand', '3', 'permit', 'The permit command');
INSERT INTO "public"."DefaultCommandConfiguration" VALUES ('RankCommand', '1', 'rank', 'The rank ');
INSERT INTO "public"."DefaultCommandConfiguration" VALUES ('ToucanCommand', '1', 'toucan', 'le toucan has arrived');

-- ----------------------------
-- Table structure for "public"."FilterConfiguration"
-- ----------------------------
DROP TABLE "public"."FilterConfiguration";
CREATE TABLE "public"."FilterConfiguration" (
"Id" int4 DEFAULT nextval('"FilterConfiguration_Id_seq"'::regclass) NOT NULL,
"ChannelName" text,
"Discriminator" text NOT NULL,
"OutputMessage" text,
"RemoveType" int4 NOT NULL,
"TimeoutSeconds" int4 NOT NULL,
"CapsLimit" int4,
"EmoteLimit" int4,
"MaxLength" int4,
"WordLimit" int4,
"MaxCount" int4,
"Enabled" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of FilterConfiguration
-- ----------------------------
INSERT INTO "public"."FilterConfiguration" VALUES ('1', null, 'LengthFilter', null, '0', '600', null, null, '5', null, null, 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('2', null, 'WordFilter', null, '0', '600', null, null, null, null, null, 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('4', null, 'EmoteFilter', null, '0', '600', null, '1', null, null, null, 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('5', null, 'RepeatFilter', null, '0', '600', null, null, null, '0', null, 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('6', null, 'SymbolFilter', null, '0', '600', null, null, '20', null, '20', 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('7', null, 'DomainFilter', null, '0', '600', null, null, null, '20', null, 'f');
INSERT INTO "public"."FilterConfiguration" VALUES ('8', null, 'CapsFilter', null, '0', '600', '10', null, '10', null, null, 'f');

-- ----------------------------
-- Table structure for "public"."FilterDescription"
-- ----------------------------
DROP TABLE "public"."FilterDescription";
CREATE TABLE "public"."FilterDescription" (
"FilterId" text NOT NULL,
"Description" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of FilterDescription
-- ----------------------------
INSERT INTO "public"."FilterDescription" VALUES ('Caps', 'Limit the amount of capitalized characters allowed in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Domain', 'Do not allow any domains in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Emote', 'Do not allow any emotes in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Length', 'Limit the maximum length of messages posted in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Repeat', 'Limit maximum repetitions in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Symbol', 'Do not allow any special characters in the chat.');
INSERT INTO "public"."FilterDescription" VALUES ('Word', 'Ban certain words from being said in the chat.');

-- ----------------------------
-- Table structure for "public"."Regular"
-- ----------------------------
DROP TABLE "public"."Regular";
CREATE TABLE "public"."Regular" (
"ChannelName" text NOT NULL,
"TwitchUsername" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Regular
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."TwitchCredentials"
-- ----------------------------
DROP TABLE "public"."TwitchCredentials";
CREATE TABLE "public"."TwitchCredentials" (
"TwitchUsername" text NOT NULL,
"AccessToken" text,
"PublicAccessToken" text,
"RefreshToken" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of TwitchCredentials
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."WhitelistedDomain"
-- ----------------------------
DROP TABLE "public"."WhitelistedDomain";
CREATE TABLE "public"."WhitelistedDomain" (
"ChannelName" text NOT NULL,
"Domain" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of WhitelistedDomain
-- ----------------------------

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."Announcement_Id_seq1" OWNED BY "Announcement"."Id";

-- ----------------------------
-- Primary Key structure for table "public"."__EFMigrationsHistory"
-- ----------------------------
ALTER TABLE "public"."__EFMigrationsHistory" ADD PRIMARY KEY ("MigrationId");

-- ----------------------------
-- Primary Key structure for table "public"."Announcement"
-- ----------------------------
ALTER TABLE "public"."Announcement" ADD PRIMARY KEY ("ChannelName", "Id");

-- ----------------------------
-- Primary Key structure for table "public"."BannedWord"
-- ----------------------------
ALTER TABLE "public"."BannedWord" ADD PRIMARY KEY ("ChannelName", "Word");

-- ----------------------------
-- Primary Key structure for table "public"."BotCommandConfiguration"
-- ----------------------------
ALTER TABLE "public"."BotCommandConfiguration" ADD PRIMARY KEY ("ChannelName", "CommandId");

-- ----------------------------
-- Primary Key structure for table "public"."Channel"
-- ----------------------------
ALTER TABLE "public"."Channel" ADD PRIMARY KEY ("ChannelName");

-- ----------------------------
-- Primary Key structure for table "public"."ChannelCommandConfiguration"
-- ----------------------------
ALTER TABLE "public"."ChannelCommandConfiguration" ADD PRIMARY KEY ("CommandId");

-- ----------------------------
-- Primary Key structure for table "public"."ChannelStatistics"
-- ----------------------------
ALTER TABLE "public"."ChannelStatistics" ADD PRIMARY KEY ("ChannelName", "Timestamp");

-- ----------------------------
-- Primary Key structure for table "public"."CustomCommand"
-- ----------------------------
ALTER TABLE "public"."CustomCommand" ADD PRIMARY KEY ("ChannelName", "CommandId");

-- ----------------------------
-- Primary Key structure for table "public"."DefaultCommandConfiguration"
-- ----------------------------
ALTER TABLE "public"."DefaultCommandConfiguration" ADD PRIMARY KEY ("CommandId");

-- ----------------------------
-- Primary Key structure for table "public"."FilterDescription"
-- ----------------------------
ALTER TABLE "public"."FilterDescription" ADD PRIMARY KEY ("FilterId");

-- ----------------------------
-- Primary Key structure for table "public"."Regular"
-- ----------------------------
ALTER TABLE "public"."Regular" ADD PRIMARY KEY ("ChannelName", "TwitchUsername");

-- ----------------------------
-- Uniques structure for table "public"."TwitchCredentials"
-- ----------------------------
ALTER TABLE "public"."TwitchCredentials" ADD UNIQUE ("PublicAccessToken");

-- ----------------------------
-- Primary Key structure for table "public"."TwitchCredentials"
-- ----------------------------
ALTER TABLE "public"."TwitchCredentials" ADD PRIMARY KEY ("TwitchUsername");

-- ----------------------------
-- Primary Key structure for table "public"."WhitelistedDomain"
-- ----------------------------
ALTER TABLE "public"."WhitelistedDomain" ADD PRIMARY KEY ("ChannelName", "Domain");

-- ----------------------------
-- Foreign Key structure for table "public"."Announcement"
-- ----------------------------
ALTER TABLE "public"."Announcement" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."BannedWord"
-- ----------------------------
ALTER TABLE "public"."BannedWord" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."BotCommandConfiguration"
-- ----------------------------
ALTER TABLE "public"."BotCommandConfiguration" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ChannelCommandConfiguration"
-- ----------------------------
ALTER TABLE "public"."ChannelCommandConfiguration" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."ChannelStatistics"
-- ----------------------------
ALTER TABLE "public"."ChannelStatistics" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."CustomCommand"
-- ----------------------------
ALTER TABLE "public"."CustomCommand" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."FilterConfiguration"
-- ----------------------------
ALTER TABLE "public"."FilterConfiguration" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."Regular"
-- ----------------------------
ALTER TABLE "public"."Regular" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."WhitelistedDomain"
-- ----------------------------
ALTER TABLE "public"."WhitelistedDomain" ADD FOREIGN KEY ("ChannelName") REFERENCES "public"."Channel" ("ChannelName") ON DELETE NO ACTION ON UPDATE NO ACTION;
