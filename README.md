# README #

This repo contains the backend code for the Shikashi Twitch Bot. It is written in C# and is currently targeting .NET Framework 4.6.1 (.NET Core will be in the future once the IRC.NET supports .NET core). 

### How do I get set up? ###

* Make sure you have a [PostgreSQL server](http://www.postgresql.org/download/) set up
* Clone this repo
* Inside the project there is a file called appsettings.json.example, make a copy of this file in the same location and remove the .example extension
* Inside the configuration file, update the database name/username/password in addition to the other values (see bottom of readme)
* You will have to create the database for your bot. This can be done by importing the sample SQL file from this repo, or running the following command inside the ShikashiBot folder:

```
#!cmd

dotnet ef database update
```
This command will also check if you have set up the database correctly.


* Start the project by compiling and running using Visual Studio or running the following command in a command line prompt:
```
#!cmd

dotnet run
```
 

### Configuration (appsettings.json) ###

* BotUsername: Username of your bot
* IRCOauthToken: OAuth token that the bot uses to interact with the chat, you can obtain one of these tokens at [this](https://twitchapps.com/tmi/) website.
* Twitch API: Configuration for the Twitch API interaction, you will have to register an application to get these keys under "Developer Applications" on the [Twitch settings page](https://www.twitch.tv/settings/connections).
* Database connection string: Configuration for the database connection.
* ApiOnlyMode: Disables any interaction between the bot and Twitch. This feature is used for development purposes only.